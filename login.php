<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('lib/config.php');
$config['pagename'] = 'Home';
$config['title']    = $config['name'] . ' - ' . $config['pagename'];
?>
<!DOCTYPE html>
<html>

<head>
    <?php include('inc/head.phtml') ?>
    <!-- Bootstrap 4 CSS CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css" />
    <!-- Fontawesome CSS CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />
    <style>
        @import url("https://fonts.googleapis.com/css?family=Maven+Pro:400,500,600,700,800,900&display=swap");

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: "Maven Pro", sans-serif;
        }

        .wrapper {
            height: 100vh;
        }

        .myColor {
            background-image: linear-gradient(to right, #f83600 50%, #f9d423 150%);
        }

        .myShadow {
            box-shadow: 0 10px 10px rgba(0, 0, 0, 0.5);
        }

        .myBtn {
            border-radius: 50px;
            font-weight: bold;
            font-size: 20px;
            background-image: linear-gradient(to right, #0acffe 0%, #495aff 100%);
            border: none;
        }

        .myBtn:hover {
            background-image: linear-gradient(to right, #495aff 0%, #0acffe 100%);
        }

        .myHr {
            height: 2px;
            border-radius: 100px;
        }

        .myLinkBtn {
            border-radius: 100px;
            width: 50%;
            border: 2px solid #fff;
        }

        @media (max-width: 720px) {
            .wrapper {
                margin: 2px;
            }
        }
    </style>
</head>

<body class="bg-info">
    <div class="container">
        <!-- Login Form Start -->
        <div class="row justify-content-center wrapper" id="login-box">
            <div class="col-lg-10 my-auto myShadow">
                <div class="row">
                    <div class="col-lg-7 bg-white p-4">
                        <img class="img-fluid mx-auto d-block" src="<?= $config['host']; ?>/assets/img/logo/logo-white-long-tr-2.png" width="300" height="200">
                        <h4 class="text-center font-weight-bold text-primary">Masuk Untuk Melanjutkan</h4>
                        <hr class="my-3" />
                        <form id="Login-Form" method="POST" class="px-3" action="<?= $config['host']; ?>/api/v1/login">
                            <div class="input-group input-group-lg form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text rounded-0"><i class="far fa-envelope fa-lg fa-fw"></i></span>
                                </div>
                                <input type="email" id="email" name="email" class="form-control rounded-0" placeholder="Alamat Email" required />
                            </div>
                            <div class="input-group input-group-lg form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text rounded-0"><i class="fas fa-key fa-lg fa-fw"></i></span>
                                </div>
                                <input type="password" id="password" name="password" class="form-control rounded-0" minlength="5" placeholder="Kata Sandi" required autocomplete="off" />
                            </div>
                            <div class="form-group clearfix">
                                <div class="custom-control custom-checkbox float-left">
                                    <input type="checkbox" class="custom-control-input" id="customCheck" name="rem" />
                                    <label class="custom-control-label" for="customCheck">Ingat saya</label>
                                </div>
                                <div class="forgot float-right">
                                    <a href="#" id="forgot-link">Lupa password?</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" id="login-btn" value="Masuk" class="btn btn-primary btn-lg btn-block myBtn" />
                            </div>
                            <a href="home.html"><b style="text-decoration: underline;">
                                    <-- Back</b></a>
                        </form>
                    </div>
                    <div class="col-lg-5 d-flex flex-column justify-content-center myColor p-4">
                        <h2 class="text-center font-weight-bold text-white">Hello Bahariers!</h2>
                        <hr class="my-3 bg-light myHr" />
                        <p class="text-center font-weight-bolder text-light lead">Daftar dan nikmati pengalaman baru makan warteg bersama kami!</p>
                        <button class="btn btn-outline-light btn-lg align-self-center font-weight-bolder mt-4 myLinkBtn" id="register-link">Daftar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Login Form End -->

        <!-- Registration Form Start -->
        <div class="row justify-content-center wrapper" id="register-box" style="display: none;">
            <div class="col-lg-10 my-auto myShadow">
                <div class="row">
                    <div class="col-lg-5 d-flex flex-column justify-content-center myColor p-4">
                        <h2 class="text-center font-weight-bold text-white">Selamat Datang!</h2>
                        <hr class="my-4 bg-light myHr" />
                        <p class="text-center font-weight-bolder text-light lead">Masukan informasi anda untuk selalu terhubung dengan kami.</p>
                        <button class="btn btn-outline-light btn-lg font-weight-bolder mt-4 align-self-center myLinkBtn" id="login-link">Masuk</button>
                    </div>
                    <div class="col-lg-7 bg-white p-4">
                        <img class="img-fluid mx-auto d-block" src="<?= $config['host']; ?>/assets/img/logo/logo-white-long-tr-2.png" width="300" height="200">
                        <h4 class="text-center font-weight-bold text-primary">Daftar Akun</h4>
                        <hr class="my-3" />
                        <form id="Register-Form" method="POST" class="px-3" action="<?= $config['host']; ?>/api/v1/register">
                            <div class="input-group input-group-lg form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text rounded-0"><i class="far fa-user fa-lg fa-fw"></i></span>
                                </div>
                                <input type="text" id="full_name" name="full_name" class="form-control rounded-0" placeholder="Nama Lengkap" required />
                            </div>
                            <div class="input-group input-group-lg form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text rounded-0"><i class="far fa-envelope fa-lg fa-fw"></i></span>
                                </div>
                                <input type="email" id="email" name="email" class="form-control rounded-0" placeholder="Alamat Email" required />
                            </div>
                            <div class="input-group input-group-lg form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text rounded-0"><i class="fas fa-phone fa-lg fa-fw"></i></span>
                                </div>
                                <input type="text" id="phone" name="phone" class="form-control rounded-0" placeholder="Nomer Handphone" required />
                            </div>
                            <div class="input-group input-group-lg form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text rounded-0"><i class="fas fa-key fa-lg fa-fw"></i></span>
                                </div>
                                <input type="password" id="password" name="password" class="form-control rounded-0" minlength="5" placeholder="Kata Sandi" required />
                            </div>
                            <div class="input-group input-group-lg form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text rounded-0"><i class="fas fa-key fa-lg fa-fw"></i></span>
                                </div>
                                <input type="password" id="conf_password" name="conf_password" class="form-control rounded-0" minlength="5" placeholder="Konfirmasi Kata Sandi" required />
                            </div>
                            <div class="form-group">
                                <div id="passError" class="text-danger font-weight-bolder"></div>
                            </div>
                            <div class="form-group">
                                <input type="submit" id="register-btn" value="Daftar" class="btn btn-primary btn-lg btn-block myBtn" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Registration Form End -->

        <!-- Forgot Password Form Start -->
        <div class="row justify-content-center wrapper" id="forgot-box" style="display: none;">
            <div class="col-lg-10 my-auto myShadow">
                <div class="row">
                    <div class="col-lg-7 bg-white p-4">
                        <h2 class="text-center font-weight-bold text-primary">Lupa Kata Sandi?</h2>
                        <hr class="my-3" />
                        <p class="lead text-center text-secondary">Untuk mengatur ulang kata sandi Anda, masukkan adddress email terdaftar dan kami akan mengirimkan instruksi pengaturan ulang kata sandi pada email Anda!</p>
                        <form action="#" method="post" class="px-3" id="forgot-form">
                            <div id="forgotAlert"></div>
                            <div class="input-group input-group-lg form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text rounded-0"><i class="far fa-envelope fa-lg"></i></span>
                                </div>
                                <input type="email" id="femail" name="email" class="form-control rounded-0" placeholder="E-Mail" required />
                            </div>
                            <div class="form-group">
                                <input type="submit" id="forgot-btn" value="Reset Password" class="btn btn-primary btn-lg btn-block myBtn" />
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-5 d-flex flex-column justify-content-center myColor p-4">
                        <h1 class="text-center font-weight-bold text-white">Reset Password!</h1>
                        <hr class="my-4 bg-light myHr" />
                        <button class="btn btn-outline-light btn-lg font-weight-bolder myLinkBtn align-self-center" id="back-link">Back</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Forgot Password Form End -->

    </div>

    <?php include('inc/foot.phtml'); ?>
    <script>
        $(function() {
            $("#register-link").click(function() {
                $("#login-box").hide();
                $("#register-box").show();
            });
            $("#login-link").click(function() {
                $("#login-box").show();
                $("#register-box").hide();
            });
            $("#forgot-link").click(function() {
                $("#login-box").hide();
                $("#forgot-box").show();
            });
            $("#back-link").click(function() {
                $("#login-box").show();
                $("#forgot-box").hide();
            });
        });

        // JS login
        $("form#Login-Form").submit(function() {
            var pdata = $(this).serialize();
            var purl = $(this).attr('action');
            $.ajax({
                url: purl,
                data: pdata,
                timeout: false,
                type: 'POST',
                dataType: 'JSON',
                success: function(hasil) {
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                    if (hasil.status) {
                        swal("Success!", hasil.content, "success");
                        window.location = '<?= $config['host']; ?>';
                    } else
                        swal("Failed!", hasil.content, "error");
                },
                error: function(a, b, c) {
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                },
                beforeSend: function() {
                    $("input").attr("disabled", "disabled");
                    $("button").attr("disabled", "disabled");
                }
            });
            return false
        });

        // JS register
        $("form#Register-Form").submit(function() {
            var pdata = $(this).serialize();
            var purl = $(this).attr('action');
            $.ajax({
                url: purl,
                data: pdata,
                timeout: false,
                type: 'POST',
                dataType: 'JSON',
                success: function(hasil) {
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                    if (hasil.status) {
                        swal("Success!", hasil.content, "success");
                    } else
                        swal("Failed!", hasil.content, "error");
                },
                error: function(a, b, c) {
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                },
                beforeSend: function() {
                    $("input").attr("disabled", "disabled");
                    $("button").attr("disabled", "disabled");
                }
            });
            return false
        });
    </script>
</body>

</html>