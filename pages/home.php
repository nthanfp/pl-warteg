<!doctype html>
<html lang="en">

<head>
    <title>Hello, world!</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Fontaswesome CSS CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins&display=swap">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <!-- Bottom Navbar -->
    <nav class="pt-2 navbar navbar-dark bg-danger navbar-expand d-md-none d-lg-none d-xl-none fixed-bottom">
        <ul class="navbar-nav nav-justified w-100">
            <li class="nav-item menu-item">
                <a href="#" class="nav-link text-center">
                    <i class="fas fa-home fa-lg"></i>
                    <span class="small d-block">Home</span>
                </a>
            </li>
            <li class="nav-item menu-item">
                <a href="#" class="nav-link text-center">
                    <i class="fas fa-shopping-bag fa-lg"></i>
                    <span class="small d-block">Pesanan</span>
                </a>
            </li>
            <li class="nav-item menu-item">
                <a href="#" class="nav-link text-center">
                    <i class="fas fa-ticket-alt fa-lg"></i>
                    <span class="small d-block">Kupon</span>
                </a>
            </li>
            <li class="nav-item menu-item">
                <a href="#" class="nav-link text-center">
                    <i class="fas fa-user fa-lg"></i>
                    <span class="small d-block">Profil</span>
                </a>
            </li>
        </ul>
    </nav>
    <!-- End: Bottom Navbar -->

    <!-- Box Promotion -->
    <div class="promotion-box pb-4">
    </div>
    <!-- End: Box Promotion -->

    <!-- Home Menu -->
    <div class="container pt-5">
        <div class="row mx-4 px-4 pt-4">

            <div class="d-flex justify-content-center col-6 py-2">
                <div class="menu-home">
                    Dine In
                </div>
            </div>
            <div class="d-flex justify-content-center col-6 py-2">
                <div class="menu-home">
                    Takeaway
                </div>
            </div>
            <div class="d-flex justify-content-center col-6 py-2">
                <div class="menu-home">
                    Delivery
                </div>
            </div>
            <div class="d-flex justify-content-center col-6 py-2">
                <div class="menu-home">
                    Lihat Menu
                </div>
            </div>

        </div>
    </div>
    <!-- End Home Menu -->

    <!-- Optional JavaScript; choose one of the two! -->
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>

</html>