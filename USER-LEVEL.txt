------------- User Level Guide --------------
----------------- PL Warteg -----------------

List all roles:
1. Developer (88)
2. Pemilik (81)
3. Manager (82)
4. Kasir (87)
5. Koki (83)
6. Pelayan (84)
--------------------------

List all feature:
- Dashboard                         | Developer (86), Pemilik (81), Manager (82), Kasir (87), Koki (83), Pelayan (84)
- Master Data
    - Data Kategori Menu            | Developer (86), Pemilik (81), Manager (82), Koki (83)
    - Data Menu                     | Developer (86), Pemilik (81), Manager (82), Koki (83)
    - Data Varian                   | Developer (86), Pemilik (81), Manager (82), Koki (83)
    - Data Bahan Baku               | Developer (86), Pemilik (81), Manager (82), Koki (83)
    - Data Pelanggan                | Developer (86), Pemilik (81), Manager (82)
    - Data Pegawai                  | Developer (86), Pemilik (81), Manager (82)
- Pesanan
    - Buat Pesanan                  | Developer (86), Pemilik (81), Manager (82), Kasir (87)
    - Kelola Pesanan                | Developer (86), Pemilik (81), Manager (82), Kasir (87)
- Pencatatan
    - Bahan Baku Masuk              | Developer (86), Pemilik (81), Manager (82), Koki (83), Pelayan (84)
    - Bahan Baku Keluar             | Developer (86), Pemilik (81), Manager (82), Koki (83), Pelayan (84)
- Database
    - Optimize Database             | Developer (86), Pemilik (81), Manager (82)
    - Backup Database               | Developer (86), Pemilik (81), Manager (82)
- Setting
    - Website Config                | Developer (86), Pemilik (81), Manager (82), Koki (83), Pelayan (84)