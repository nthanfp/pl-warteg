<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('lib/config.php');
$config['pagename'] = 'Kupon Saya';
$config['title']    = $config['name'] . ' - ' . $config['pagename'];
if (isset($_SESSION['cst_status']) && ($_SESSION['cst_status'] == 'login')) {
    $id_cust    = $_SESSION['cst_id'];
    $query      = mysqli_query($conn, "SELECT * FROM `wrtg_customer` WHERE `id_customer`='$id_cust'");
    $cust       = mysqli_fetch_assoc($query);
} else {
    header('Location: ' . $config['host'] . '/login');
    exit;
}
?>
<!DOCTYPE html>
<html>

<head>
    <?php include('inc/head.phtml') ?>
    <style>
        .coupon .kanan {
            border-left: 1px dashed #ddd;
            width: 40% !important;
            position: relative;
        }

        .coupon .kanan .info::after,
        .coupon .kanan .info::before {
            content: '';
            position: absolute;
            width: 20px;
            height: 20px;
            background: #E6E6E6;
            border-radius: 100%;
        }

        .coupon .kanan .info::before {
            top: -10px;
            left: -10px;
        }

        .coupon .kanan .info::after {
            bottom: -10px;
            left: -10px;
        }

        .coupon .time {
            font-size: 1.6rem;
        }
    </style>
</head>

<body>

    <?php include('inc/sidebar.phtml'); ?>

    <div class="w3-main" style="margin-left: 340px; margin-right: 40px">

        <?php include('inc/headbox.phtml'); ?>

        <!-- Content -->
        <div class="w3-container" style="margin-top: 10px; margin-bottom: 80px;">
            <div class="row">
                <div>
                    <h1 class="fs-2" style="color: #2980b9;"><b>Kupon</b></h1>
                    <hr style="width:50px; border: 5px solid #2980b9; " class="w3-round">
                </div>
                <div class="col-lg-4">
                    <div class="card mb-3">
                        <div class="card-header">Klaim Kode Kupon</div>
                        <div class="card-body">
                            <form id="Claim-Voucher" method="POST" action="<?= $config['host']; ?>/api/v1/voucher?method=CLAIM">
                                <div class="mb-2">
                                    <label class="small mb-1">Kode Kupon</label>
                                    <input type="text" class="form-control" id="voc_code" name="voc_code" placeholder="Kode Kupon">
                                </div>
                                <div class="mb-3">
                                    <button type="submit" class="btn btn-primary w-100">Klaim Kupon</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="alert alert-warning">Silahkan gunakan voucher yang anda miliki, karena voucher di sistem ini bersifat <strong>pemakaian terbatas</strong>!</div>

                    <div id="DataVoucher">

                    </div>

                </div>
            </div>
        </div>
        <!-- End Content -->
    </div>

    <?php include('inc/foot.phtml'); ?>
    <script type="text/javascript">
        function loadVoucher() {
            let url = '<?= $config['host']; ?>/api/v1/voucher?method=GET_MY';
            $.ajax({
                type: "POST",
                url: url,
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.data == null) {
                        $('#DataVoucher').html('<div class="alert alert-danger" role="alert">Belum ada kupon yang kamu miliki :(</div>');
                    } else if (((response.data).length < 1)) {
                        $('#DataVoucher').html('<div class="alert alert-danger" role="alert">Belum ada kupon yang kamu miliki :(</div>');
                    } else {
                        $.each(response.data, function(i, val) {
                            onClick = "useVoucher('" + val.id_own + "', '" + val.voc_code + "')";
                            htmlData = '<div class="card mb-3 border border-primary">' +
                                '<div class="card-body">' +
                                '<div class="row">' +
                                '<div class="col-sm-12 col-md-3 mb-2 d-none d-md-block"><img class="img-fluid mx-auto" src="<?= $config['host']; ?>/' + val.voc_image + '"></div>' +
                                '<div class="col-sm-12 col-md-9 mb-2">' +
                                '<h3 class="lead">' + val.voc_name + '</h3>' +
                                '<p class="text-muted text-justify mb-0" style="text-align: justify;">' + val.voc_description + '</p>' +
                                '</div>' +
                                '<div class="col-sm-12 mb-2">' +
                                '<button disabled class="btn btn-primary w-100 btn-block" onclick="' + onClick + '"><i class="far fa-check-circle"></i> Gunakan</button>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            $('#DataVoucher').append(htmlData);
                        });
                    }

                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                    $('#DataVoucher').html('');
                }
            });
        }

        $(document).ready(function() {
            loadVoucher();

            $("form#Claim-Voucher").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        if (hasil.status) {
                            swal("Success!", hasil.content, "success");
                            loadVoucher();
                        } else
                            swal("Failed!", hasil.content, "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });
        });
    </script>
</body>

</html>