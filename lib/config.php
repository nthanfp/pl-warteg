<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
date_default_timezone_set('Asia/Jakarta');

// Database configuration
if ($_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR']) {
    $sql_details['user']    = 'acpnmodm_warteg';
    $sql_details['pass']    = 'Hahalol123!';
    $sql_details['db']      = 'acpnmodm_warteg';
    $sql_details['host']    = 'acpn-mod.my.id';
} else {
    $sql_details['user']    = 'acpnmodm_warteg';
    $sql_details['pass']    = 'Hahalol123!';
    $sql_details['db']      = 'acpnmodm_warteg';
    $sql_details['host']    = 'localhost';
}

$conn = mysqli_connect($sql_details['host'], $sql_details['user'], $sql_details['pass'], $sql_details['db']);
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
}

// Configuration from DB
$config = array();
$query_config = mysqli_query($conn, "SELECT * FROM `wrtg_config` WHERE `status`='Y'") or die(mysqli_error($conn));
while ($cnfg = mysqli_fetch_assoc($query_config)) {
    $name   = $cnfg['name'];
    $value  = $cnfg['value'];
    $config[$name] = $value;
}


// Override config
$time                   = time();
$config['admin_name']   = 'WRTG Admin';
$config['host']         = ($_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR']) ? 'http://localhost/pl-warteg' : 'https://warteg.acpn-mod.my.id';
$config['host_admin']   = $config['host'] . '/admin-page';
$config['error']        = 0; // 0 = False and 1 = True
$config['mt']           = 0; // 0 = False and 1 = True

// Including
include('function.class.php');
include('ssp.class.php');

// Error debugging
if ($config['error'] == 0) {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(0);
} else if ($config['error'] == 1) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

// Maintenance redirect
if ($config['mt'] == 1) {
    if (!strpos($_SERVER['SCRIPT_FILENAME'], 'maintenance.php')) {
        header("Location: " . $config['host'] . "/maintenance");
        exit();
    }
}
