<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('lib/config.php');
$config['pagename'] = 'Keranjang';
$config['title']    = $config['name'] . ' - ' . $config['pagename'];
if (isset($_SESSION['cst_status']) && ($_SESSION['cst_status'] == 'login')) {
    $id_cust    = $_SESSION['cst_id'];
    $query      = mysqli_query($conn, "SELECT * FROM `wrtg_customer` WHERE `id_customer`='$id_cust'");
    $cust       = mysqli_fetch_assoc($query);
} else {
    header('Location: ' . $config['host'] . '/login');
    exit;
}
?>
<!DOCTYPE html>
<html>

<head>
    <?php include('inc/head.phtml') ?>
</head>

<body>

    <?php include('inc/sidebar.phtml'); ?>

    <div class="w3-main" style="margin-left: 340px; margin-right: 40px">

        <?php include('inc/headbox.phtml'); ?>

        <!-- Content -->
        <div class="w3-container" style="margin-top: 10px" id="showcase">

            <h1 class="fs-2" style="color: #2980b9;"><b>Keranjang</b></h1>
            <hr style="width: 50px; border: 5px solid #2980b9; " class="w3-round" />

            <!-- Keranjang -->
            <section>
                <div class="container">
                    <div class="row d-flex justify-content-center align-items-center h-100">

                        <div id="DataCart" class="card px-3 pt-3 pb-2 my-2" style="background-color: #8e9ea9;">
                        </div>

                        <div class="card my-2" id="DataCartVoucher">
                            <div class="card-body p-4">
                                <label for="voc_code" class="form-label">Kode Kupon</label>
                                <div class="input-group">
                                    <input type="text" id="voc_code" name="voc_code" class="form-control" placeholder="Masukan kode kupon yang anda punya" readonly>
                                    <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#ModalVoucher">Pilih Kupon</button>
                                </div>
                            </div>
                        </div>

                        <div class="card my-2" id="DataCartTotal">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <p class="mb-2">Total Bayar</p>
                                    <p class="mb-2" id="ch_subtotal"></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="mb-2">Diskon</p>
                                    <p class="mb-2 text-danger" id="ch_discount"></p>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <p class="mb-2">Pajak</p>
                                    <p class="mb-2" id="ch_tax"></p>
                                </div>

                                <div class="d-flex justify-content-between mb-2">
                                    <p class="mb-2">Total (Termasuk Pajak)</p>
                                    <p class="mb-2" id="ch_total"></p>
                                </div>

                                <button type="button" class="btn btn-primary btn-block" data-bs-toggle="modal" data-bs-target="#exampleModal">Lanjutkan</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Form Data Pesanan</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form method="post">
                                <div class="modal-body">

                                    <div class="mb-3">
                                        <label for="ord_name" class="form-label">Masukan Nama Pemesan</label>
                                        <input type="text" class="form-control" id="ord_name" placeholder="Masukan Nama Pemesan" name="ord_name" required>
                                    </div>

                                    <div class="mb-3">
                                        <label for="ord_type" class="form-label">Tipe Pesanan</label>
                                        <select class="form-select" id="ord_type" onchange="showForm()">
                                            <option value="" selected="">-- Pilih Tipe Pesanan --</option>
                                            <option value="Dinein">Dine In / Makan Di Tempat</option>
                                            <option value="Takeaway">Take Away / Bungkus</option>
                                            <option value="Delivery">Delivery / Kirim</option>
                                        </select>
                                    </div>

                                    <div class="mb-3" id="table-number-form" style="display:none;">
                                        <label for="ord_table_number" class="form-label">Masukan No Meja</label>
                                        <input type="number" class="form-control" id="ord_table_number" placeholder="Masukan No Meja" name="ord_table_number" required>
                                    </div>

                                    <div class="mb-3" id="address-form" style="display:none;">
                                        <label for="ord_address" class="form-label">Masukan Alamat Lengkap</label>
                                        <textarea rows="3" class="form-control" id="ord_address" placeholder="Masukan Alamat Lengkap" name="ord_address" required><?= $cust['cst_full_address']; ?>, <?= $cust['cst_addr_state']; ?>, <?= $cust['cst_addr_city']; ?>, <?= $cust['cst_addr_district']; ?>, <?= $cust['cst_addr_village']; ?></textarea>
                                    </div>

                                    <div class="mb-3">
                                        <label for="ord_payment" class="form-label">Pilih Metode Pembayaran</label>
                                        <select onchange="changePayment()" class="form-select" aria-label="Category" id="ord_payment" name="kategori" required>
                                            <option value="" selected="">-- Metode Pembayaran --</option>
                                            <?php
                                            $query = mysqli_query($conn, "SELECT * FROM `wrtg_payment` WHERE `pay_status`='Y'");
                                            while ($row = mysqli_fetch_assoc($query)) {
                                                echo '<option value="' . $row['pay_code'] . '">' . $row['pay_name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="mb-3">
                                        <label for="ord_notes" class="form-label">Masukan Keterangan</label>
                                        <textarea class="form-control" id="ord_notes" rows="3" placeholder="Catatan/Keterangan"></textarea>
                                    </div>

                                    <div class="mb-3">
                                        <h4><b>Pembayaran</b></h4>
                                        <div class="d-flex justify-content-between">
                                            <p class="mb-2">Total Bayar</p>
                                            <p class="mb-2" id="ch_subtotal_2"></p>
                                        </div>

                                        <div class="d-flex justify-content-between">
                                            <p class="mb-2">Diskon</p>
                                            <p class="mb-2 text-danger" id="ch_discount_2"></p>
                                        </div>

                                        <div class="d-flex justify-content-between">
                                            <p class="mb-2">Pajak</p>
                                            <p class="mb-2" id="ch_tax_2"></p>
                                        </div>

                                        <div class="d-flex justify-content-between mb-4">
                                            <p class="mb-2">Total (Termasuk Pajak)</p>
                                            <p class="mb-2" id="ch_total_2"></p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" onclick="checkoutNow()">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal Voucher -->
                <div class="modal fade" id="ModalVoucher" tabindex="-1" aria-labelledby="ModalVoucher" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ModalVoucher">Voucher Saya</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body" id="DataVoucherList">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal Voucher -->

            </section>
            <!-- Keranjang: End -->

        </div>
        <!-- End Content -->
    </div>

    <?php include('inc/foot.phtml'); ?>
    <script type="text/javascript">
        function showForm() {
            var deliveryMethod = $('#ord_type').val();
            var tableNumberForm = $('#table-number-form');
            var addressForm = $('#address-form');

            if (deliveryMethod === "Dinein") {
                tableNumberForm.css('display', 'block');
                addressForm.css('display', 'none');
            } else if (deliveryMethod === "Delivery") {
                tableNumberForm.css('display', 'none');
                addressForm.css('display', 'block');
            } else {
                tableNumberForm.css('display', 'none');
                addressForm.css('display', 'none');
            }
        }

        function changePayment() {
            var payment = $('#ord_payment').val();
            var deliveryMethod = $('#ord_type').val();
            if ((payment === 'COD') && (deliveryMethod === 'Delivery')) {
                $('#ord_payment').val('');
                alert('Pesanan delivery tidak bisa menggunakan metode pembayaran COD, silahkan pilih metode pembayaran lainnya');
            }
            alert(payment);
        }

        function checkoutNow() {
            let ord_name = $('#ord_name').val();
            let ord_type = $('#ord_type').val();
            let ord_table_number = $('#ord_table_number').val();
            let ord_address = $('#ord_address').val();
            let ord_payment = $('#ord_payment').val();
            let ord_notes = $('#ord_notes').val();

            let url = '<?= $config['host']; ?>/api/v1/cart?method=CHECKOUT';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    ord_name,
                    ord_type,
                    ord_table_number,
                    ord_address,
                    ord_payment,
                    ord_notes
                },
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        if (response.redirect) {
                            window.location = response.redirect;
                        }
                        swal("Berhasil", response.content, "success");
                    } else {
                        swal("Error", response.content, "error");
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function apiAddCart(id_menu, qty) {
            let url = '<?= $config['host']; ?>/api/v1/cart?method=ADD_CART';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id_menu,
                    qty
                },
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        // swal("Berhasil", response.content, "success");
                    } else {
                        // swal("Error", response.content, "error");
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function loadCart() {
            let url = '<?= $config['host']; ?>/api/v1/cart?method=GET_CART';
            $.ajax({
                type: "POST",
                url: url,
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    var myCart = response.cart;
                    if (myCart.data == null) {
                        $('#DataCart').html('<div class="alert alert-danger" role="alert">Keranjang anda masih kosong :(</div>');
                        $('#DataCartVoucher').hide();
                        $('#DataCartTotal').hide();
                    } else if (((myCart.data).length < 1)) {
                        $('#DataCart').html('<div class="alert alert-danger" role="alert">Keranjang anda masih kosong :(</div>');
                        $('#DataCartVoucher').hide();
                        $('#DataCartTotal').hide();
                    } else {
                        $.each(myCart.data, function(i, val) {
                            htmlData = '<div class="card rounded-3 mb-2">' +
                                '<div class="card-body p-4">' +
                                '<div class="row d-flex justify-content-between align-items-center">' +
                                '<div class="col-12 col-sm-12 col-md-2">' +
                                '<img src="<?= $config['host']; ?>/' + val.menu_images + '" class="img-fluid rounded-3" alt="' + val.menu_name + '">' +
                                '</div>' +
                                '<div class="col-12 col-sm-12 col-md-3">' +
                                '<p class="lead fw-normal mb-2 mt-2">' + val.menu_name + '</p>' +
                                '</div>' +
                                '<div class="col-12 col-sm-12 col-md-3 d-flex">' +
                                '<button class="btn btn-link px-2" onclick="stepDownCart(' + val.id_menu + ')"><i class="fas fa-minus"></i></button>' +
                                '<input onchange="changeQtyCart(' + val.id_menu + ')" id="' + val.id_menu + '" min="0" name="quantity" value="' + val.qty + '" type="number" class="form-control form-control-sm">' +
                                '<button class="btn btn-link px-2" onclick="stepUpCart(' + val.id_menu + ')"><i class="fas fa-plus"></i></button>' +
                                '</div>' +
                                '<div class="col-8 col-sm-8 col-md-3 col-lg-2 col-xl-2 offset-lg-1">' +
                                '<h5 class="mb-0">' + formatRupiah(val.menu_price, 'Rp. ') + '</h5>' +
                                '</div>' +
                                '<div class="col-4 col-sm-4 col-md-1 col-lg-1 col-xl-1 text-end">' +
                                '<h5 class="mb-0"><a href="#!" class="text-danger" onclick="deleteCart(' + val.id_menu + ');"><i class="fas fa-trash fa-lg"></i></a></h5>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            $('#DataCart').append(htmlData);
                        });

                        if (myCart.voucher) {
                            if (myCart.voucher.voc_code) {
                                $('#voc_code').val(myCart.voucher.voc_code);
                            } else {
                                $('#voc_code').val('Tidak ada kupon terpakai');
                            }
                        }

                        $('#ch_subtotal').html(formatRupiah(myCart.amount_sub_total.toString()), 'Rp ');
                        $('#ch_discount').html('- ' + formatRupiah(myCart.amount_discount.toString()), 'Rp ');
                        $('#ch_tax').html(formatRupiah(myCart.amount_tax.toString()), 'Rp ');
                        $('#ch_total').html(formatRupiah(myCart.amount_total.toString()), 'Rp ');

                        $('#ch_subtotal_2').html(formatRupiah(myCart.amount_sub_total.toString()), 'Rp ');
                        $('#ch_discount_2').html('- ' + formatRupiah(myCart.amount_discount.toString()), 'Rp ');
                        $('#ch_tax_2').html(formatRupiah(myCart.amount_tax.toString()), 'Rp ');
                        $('#ch_total_2').html(formatRupiah(myCart.amount_total.toString()), 'Rp ');
                    }

                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                    $('#DataCart').html('');
                }
            });
        }

        function deleteCart(id_menu) {
            let url = '<?= $config['host']; ?>/api/v1/cart?method=DELETE_CART';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id_menu
                },
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        loadCart();
                    } else {

                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function stepUpCart(id) {
            qty = parseInt($('#' + id).val());
            $('#' + id).val(qty + 1);
            apiAddCart(id, qty + 1);
            loadCart();
        }

        function stepDownCart(id) {
            qty = parseInt($('#' + id).val());
            if (qty < 2) {
                $('#' + id).val(1);
                apiAddCart(id, 1);
                loadCart();
                console.log('OK 1')
            } else {
                $('#' + id).val(qty - 1);
                apiAddCart(id, qty - 1);
                loadCart();
                console.log('OK 2')
            }
        }

        function changeQtyCart(id) {
            qty = parseInt($('#' + id).val());
            apiAddCart(id, qty);
            loadCart();
        }

        function loadVoucher() {
            let url = '<?= $config['host']; ?>/api/v1/voucher?method=GET_MY';
            $.ajax({
                type: "POST",
                url: url,
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.data == null) {
                        $('#DataVoucherList').html('<div class="alert alert-danger" role="alert">Anda tidak memiliki kupon :(</div>');
                    } else if (((response.data).length < 1)) {
                        $('#DataVoucherList').html('<div class="alert alert-danger" role="alert">Anda tidak memiliki kupon :(</div>');
                    } else {
                        $.each(response.data, function(i, val) {
                            if (val.selected === true) {
                                onClick = "cancelVoucher()";
                                htmlData = '<div class="card mb-3 border border-primary">' +
                                    '<div class="card-body">' +
                                    '<div class="row">' +
                                    '<div class="col-sm-12 col-md-3 mb-2 d-none d-md-block"><img class="img-fluid mx-auto" src="<?= $config['host']; ?>/' + val.voc_image + '" /></div>' +
                                    '<div class="col-sm-12 col-md-9 mb-2">' +
                                    '<h3 class="lead">' + val.voc_name + '</h3>' +
                                    '<p class="text-muted text-justify mb-0" style="text-align: justify;">' + val.voc_description + '</p>' +
                                    '</div>' +
                                    '<div class="col-sm-12 mb-2">' +
                                    '<button class="btn btn-danger w-100 btn-block" onclick="' + onClick + '"><i class="far fa-times-circle"></i> Batalkan</button>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                            } else {
                                onClick = "useVoucher('" + val.id_own + "', '" + val.voc_code + "')";
                                htmlData = '<div class="card mb-3 border border-primary">' +
                                    '<div class="card-body">' +
                                    '<div class="row">' +
                                    '<div class="col-sm-12 col-md-3 mb-2 d-none d-md-block"><img class="img-fluid mx-auto" src="<?= $config['host']; ?>/' + val.voc_image + '" /></div>' +
                                    '<div class="col-sm-12 col-md-9 mb-2">' +
                                    '<h3 class="lead">' + val.voc_name + '</h3>' +
                                    '<p class="text-muted text-justify mb-0" style="text-align: justify;">' + val.voc_description + '</p>' +
                                    '</div>' +
                                    '<div class="col-sm-12 mb-2">' +
                                    '<button class="btn btn-primary w-100 btn-block" onclick="' + onClick + '"><i class="far fa-check-circle"></i> Gunakan</button>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                            }
                            $('#DataVoucherList').append(htmlData);
                        });
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                    $('#DataVoucherList').html('');
                }
            });
        }

        function useVoucher(id_own, voc_code) {
            $.ajax({
                type: "POST",
                url: '<?= $config['host']; ?>/api/v1/cart?method=USE_VOUCHER',
                data: {
                    id_own,
                    voc_code
                },
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        // swal("Berhasil", response.content, "success");
                        loadCart();
                        loadVoucher();
                    } else {
                        swal("Error", response.content, "error");
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function cancelVoucher() {
            $.ajax({
                type: "POST",
                url: '<?= $config['host']; ?>/api/v1/voucher?method=CANCEL',
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        // swal("Berhasil", response.content, "success");
                        loadCart();
                        loadVoucher();
                    } else {
                        // swal("Error", response.content, "error");
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        $(document).ready(function() {
            loadCart();
            loadVoucher();
        });
    </script>
</body>

</html>