<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('lib/config.php');
$config['pagename'] = 'Home';
$config['title']    = $config['name'] . ' - ' . $config['pagename'];
?>
<!DOCTYPE html>
<html>

<head>
    <?php include('inc/head.phtml') ?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?= $config['host']; ?>/assets/external/css/menu.css" />

    <style type="text/css">
        .hot_section .carousel-wrap {
            margin: 0 auto;
            padding: 0 5%;
            position: relative;
        }

        .hot_section .owl-nav>div {
            margin-top: -26px;
            position: absolute;
            top: 50%;
            color: #cdcbcd;
        }

        .hot_section .owl-carousel .owl-nav .owl-prev,
        .hot_section .owl-carousel .owl-nav .owl-next {
            width: 50px;
            height: 50px;
            background-color: #51b5ef;
            background-size: 9px;
            background-position: center;
            background-repeat: no-repeat;
            position: absolute;
            top: 50%;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            outline: none;
        }

        .hot_section .owl-carousel .owl-nav .owl-prev:hover,
        .hot_section .owl-carousel .owl-nav .owl-next:hover {
            background-color: #252525;
        }

        .hot_section .owl-carousel .owl-nav .owl-prev {
            background-image: url(../images/left-angle.png);
            left: -10%;
        }

        .hot_section .owl-carousel .owl-nav .owl-next {
            right: -10%;
            background-image: url(../images/right-angle.png);
        }

        .hot_section .owl-carousel .owl-dots.disabled,
        .hot_section .owl-carousel .owl-nav.disabled {
            display: block;
        }
    </style>

    <style type="text/css">
    </style>
</head>

<body>

    <?php include('inc/sidebar.phtml'); ?>

    <div class="w3-main" style="margin-left: 340px; margin-right: 40px">

        <?php include('inc/headbox.phtml'); ?>

        <!-- Content -->
        <div class="w3-container" style="margin-top: 10px; margin-bottom: 30px;">
            <div class="row">

                <!-- Banner -->
                <div class="col-lg-6 mb-4">
                    <div>
                        <h1 class="fs-2" style="color: #2980b9;"><b>Promosi</b></h1>
                        <hr style="width:50px; border: 5px solid #2980b9; " class="w3-round">
                    </div>
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2)">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?= $config['host']; ?>/assets/img/slider-banner/1.png" class="d-block w-100" alt="Warteg Bahari">
                            </div>
                            <div class="carousel-item">
                                <img src="<?= $config['host']; ?>/assets/img/slider-banner/2.png" class="d-block w-100" alt="Warteg Bahari">
                            </div>
                            <div class="carousel-item">
                                <img src="<?= $config['host']; ?>/assets/img/slider-banner/3.png" class="d-block w-100" alt="Warteg Bahari">
                            </div>
                            <div class="carousel-item">
                                <img src="<?= $config['host']; ?>/assets/img/slider-banner/4.png" class="d-block w-100" alt="Warteg Bahari">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
                <!-- End: Banner -->

                <!-- Testimoni -->
                <div class="col-lg-6 mb-4">
                    <div>
                        <h1 class="fs-2" style="color: #2980b9;"><b>Testimoni</b></h1>
                        <hr style="width:50px; border: 5px solid #2980b9; " class="w3-round">
                    </div>
                    <div class="mx-2" id="Review-Data">
                        <div class="carousel-wrapper">
                            <div class="owl-carousel-testimoni owl-carousel owl-theme" style="z-index: 0;">
                                <?php
                                $query  = "SELECT * FROM `wrtg_order_review` ORDER BY `created_at` DESC LIMIT 5";
                                $query  = mysqli_query($conn, $query);
                                while ($review = mysqli_fetch_assoc($query)) {
                                    $htmlStar = '';
                                    for ($i = 0; $i < 5; $i++) {
                                        if ($i < $review['rev_rating']) {
                                            $htmlStar .= '<i class="fas fa-star" style="color: #FD7E14;"></i>';
                                        } else {
                                            $htmlStar .= '<i class="far fa-star"></i>';
                                        }
                                    }
                                ?>
                                    <div class="item mx-2">
                                        <figure style="margin: 8px;">
                                            <blockquote class="blockquote">
                                                <p id="dt_rev_message"><?= $review['rev_message']; ?></p>
                                            </blockquote>
                                            <figcaption class="blockquote-footer">
                                                <span id="dt_rev_stars"><?= $htmlStar; ?></span> <strong><span id="dt_rev_rating"><?= $review['rev_rating']; ?></span>.0</strong> dari <strong>5.0</strong>
                                            </figcaption>
                                        </figure>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End: Testimoni -->

                <!-- Menu Spesial -->
                <div class="col-lg-12 mb-4">
                    <div>
                        <h1 class="fs-2" style="color: #2980b9;"><b>Paket Spesial</b></h1>
                        <hr style="width:50px; border: 5px solid #2980b9; " class="w3-round">
                        <p>Kami dengan senang hati menginformasikan bahwa restoran kami menawarkan berbagai pilihan paket makanan yang menggugah selera.</p>
                    </div>
                    <div class="carousel-wrapper">
                        <div class="owl-carousel-menu owl-carousel owl-theme" style="z-index: 0;">
                            <?php
                            $query  = "SELECT * FROM `wrtg_menu` WHERE `id_category` IN('113') ORDER BY `menu_name` ASC";
                            $query  = mysqli_query($conn, $query);
                            while ($menu = mysqli_fetch_assoc($query)) {
                                $id_menu = $menu['id_menu'];
                                $sold = mysqli_query($conn, "SELECT `id_menu`, COUNT(id_menu) AS `menu_sold` FROM  `wrtg_order_detail` WHERE `id_menu`='$id_menu'") or die(mysqli_error($conn));
                                $sold = mysqli_fetch_assoc($sold);
                                $space = '';
                                for ($i = 0; $i < (65 - strlen($menu['menu_description'])); $i++) {
                                    $space .= '-';
                                }
                            ?>
                                <div class="item mx-2">
                                    <div class="card" style="min-height: 490px;">
                                        <img src="<?= $config['host']; ?>/<?= $menu['menu_images']; ?>" class="card-img-top" alt="<?= $menu['menu_name']; ?>">
                                        <div class="card-body">
                                            <h5 class="card-title"><?= $menu['menu_name']; ?></h5>
                                            <p class="card-text"><?= $menu['menu_description']; ?> <?= str_replace('-', "&nbsp;", $space); ?></p>
                                        </div>
                                        <div class="mb-5 mx-4 d-flex justify-content-between">
                                            <h3><?= rupiah($menu['menu_price']); ?></h3>
                                            <button class="btn form-control btn-primary" onclick="openCart('<?= $menu['id_menu']; ?>');" id_menu="<?= $menu['id_menu']; ?>"><i class="fas fa-cart-plus"></i> Beli</button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div id="Modal-List-1">
                        <?php
                        $query  = "SELECT * FROM `wrtg_menu` WHERE `id_category` IN('113') ORDER BY `menu_name` ASC";
                        $query  = mysqli_query($conn, $query);
                        while ($menu = mysqli_fetch_assoc($query)) {
                            $id_menu = $menu['id_menu'];
                            $sold = mysqli_query($conn, "SELECT `id_menu`, COUNT(id_menu) AS `menu_sold` FROM  `wrtg_order_detail` WHERE `id_menu`='$id_menu'") or die(mysqli_error($conn));
                            $sold = mysqli_fetch_assoc($sold);
                            $space = '';
                            for ($i = 0; $i < (50 - strlen($menu['menu_description'])); $i++) {
                                $space .= '-';
                            }
                        ?>
                            <div class="modal fade" id="cartModal-<?= $menu['id_menu']; ?>" tabindex="-1" aria-labelledby="cartModal-<?= $menu['id_menu']; ?>-Label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title text-secondary" id="cartModal-<?= $menu['id_menu']; ?>-Label"><?= $menu['menu_name']; ?></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <th>Menu Terjual</th>
                                                        <th width="10">:</th>
                                                        <td><?= $sold['menu_sold']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Stok Menu</th>
                                                        <th width="10">:</th>
                                                        <td><?= $menu['menu_stock']; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="d-flex">
                                                <button class="btn btn-link px-2" onclick="stepDownCart('<?= $menu['id_menu']; ?>')"><i class="fas fa-minus"></i></button>
                                                <input onchange="changeQtyCart('<?= $menu['id_menu']; ?>')" id="<?= $menu['id_menu']; ?>" min="0" name="quantity" value="1" type="number" class="form-control form-control-sm">
                                                <button class="btn btn-link px-2" onclick="stepUpCart('<?= $menu['id_menu']; ?>')"><i class="fas fa-plus"></i></button>
                                                <button class="btn form-control btn-primary" onclick="addToCartV2('<?= $menu['id_menu']; ?>');" id_menu=""><i class="fas fa-cart-plus"></i> Beli</button>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- End: Menu Spesial -->

                <!-- Menu Spesial -->
                <div class="col-lg-12 mb-4">
                    <div>
                        <h1 class="fs-2" style="color: #2980b9;"><b>Menu Populer</b></h1>
                        <hr style="width:50px; border: 5px solid #2980b9; " class="w3-round">
                        <p>Kami dengan senang hati menginformasikan bahwa restoran kami menawarkan berbagai pilihan paket makanan yang menggugah selera.</p>
                    </div>
                    <div class="carousel-wrapper">
                        <div class="owl-carousel-menu owl-carousel owl-theme" style="z-index: 0;">
                            <?php
                            $query  = "SELECT *, SUM(qty) AS `menu_sold`
                            FROM `wrtg_menu`
                            INNER JOIN `wrtg_order_detail`
                                USING(`id_menu`)
                            GROUP BY `id_menu`
                            ORDER BY `menu_sold` DESC LIMIT 10";
                            $query  = mysqli_query($conn, $query);
                            while ($menu = mysqli_fetch_assoc($query)) {
                                $id_menu = $menu['id_menu'];
                                $sold = mysqli_query($conn, "SELECT `id_menu`, COUNT(id_menu) AS `menu_sold` FROM  `wrtg_order_detail` WHERE `id_menu`='$id_menu'") or die(mysqli_error($conn));
                                $sold = mysqli_fetch_assoc($sold);
                                $space = '';
                                for ($i = 0; $i < (65 - strlen($menu['menu_description'])); $i++) {
                                    $space .= '-';
                                }
                            ?>
                                <div class="item mx-2">
                                    <div class="card" style="min-height: 490px;">
                                        <img src="<?= $config['host']; ?>/<?= $menu['menu_images']; ?>" class="card-img-top" alt="<?= $menu['menu_name']; ?>">
                                        <div class="card-body">
                                            <h5 class="card-title"><?= $menu['menu_name']; ?></h5>
                                            <p class="card-text"><?= $menu['menu_description']; ?> <?= str_replace('-', "&nbsp;", $space); ?></p>
                                        </div>
                                        <div class="mb-2 mx-4 d-flex justify-content-left">
                                            <i>Terjual: <?= $menu['menu_sold']; ?></i>
                                        </div>
                                        <div class="mb-5 mx-4 d-flex justify-content-between">
                                            <h3><?= rupiah($menu['menu_price']); ?></h3>
                                            <button class="btn form-control btn-primary" onclick="openCart('<?= $menu['id_menu']; ?>');" id_menu="<?= $menu['id_menu']; ?>"><i class="fas fa-cart-plus"></i> Beli</button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div id="Modal-List-1">
                        <?php
                        $query  = "SELECT * FROM `wrtg_menu` WHERE `id_category` IN('113') ORDER BY `menu_name` ASC";
                        $query  = mysqli_query($conn, $query);
                        while ($menu = mysqli_fetch_assoc($query)) {
                            $id_menu = $menu['id_menu'];
                            $sold = mysqli_query($conn, "SELECT `id_menu`, COUNT(id_menu) AS `menu_sold` FROM  `wrtg_order_detail` WHERE `id_menu`='$id_menu'") or die(mysqli_error($conn));
                            $sold = mysqli_fetch_assoc($sold);
                            $space = '';
                            for ($i = 0; $i < (50 - strlen($menu['menu_description'])); $i++) {
                                $space .= '-';
                            }
                        ?>
                            <div class="modal fade" id="cartModal-<?= $menu['id_menu']; ?>" tabindex="-1" aria-labelledby="cartModal-<?= $menu['id_menu']; ?>-Label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title text-secondary" id="cartModal-<?= $menu['id_menu']; ?>-Label"><?= $menu['menu_name']; ?></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <th>Menu Terjual</th>
                                                        <th width="10">:</th>
                                                        <td><?= $sold['menu_sold']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Stok Menu</th>
                                                        <th width="10">:</th>
                                                        <td><?= $menu['menu_stock']; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="d-flex">
                                                <button class="btn btn-link px-2" onclick="stepDownCart('<?= $menu['id_menu']; ?>')"><i class="fas fa-minus"></i></button>
                                                <input onchange="changeQtyCart('<?= $menu['id_menu']; ?>')" id="<?= $menu['id_menu']; ?>" min="0" name="quantity" value="1" type="number" class="form-control form-control-sm">
                                                <button class="btn btn-link px-2" onclick="stepUpCart('<?= $menu['id_menu']; ?>')"><i class="fas fa-plus"></i></button>
                                                <button class="btn form-control btn-primary" onclick="addToCartV2('<?= $menu['id_menu']; ?>');" id_menu=""><i class="fas fa-cart-plus"></i> Beli</button>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- End: Menu Spesial -->

            </div>
        </div>
    </div>
    <!-- End Content -->
    </div>

    <?php include('inc/foot.phtml'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript">
        function apiAddCart(id_menu, qty) {
            let url = '<?= $config['host']; ?>/api/v1/cart?method=ADD_CART';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id_menu,
                    qty
                },
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        swal("Berhasil", response.content, "success");
                    } else {
                        swal("Error", response.content, "error");
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function addToCart(id_menu) {
            apiAddCart(id_menu, 1);
        }

        function addToCartV2(id_menu) {
            qty = parseInt($('#' + id_menu).val());
            apiAddCart(id_menu, qty);
        }

        function openCart(id_menu) {
            $('#cartModal-' + id_menu).modal('show');
            $.ajax({
                type: "POST",
                url: '<?= $config['host']; ?>/api/v1/cart?method=GET_CART',
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    var myCart = response.cart;
                    if (myCart.data == null) {} else if (((myCart.data).length < 1)) {} else {
                        $.each(myCart.data, function(i, val) {
                            if (val.id_menu === id_menu) {
                                $('#' + id_menu).val(val.qty);
                            }
                        });
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');;
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                    $('#' + id_menu).val(1);
                }
            });
        }

        function stepUpCart(id) {
            qty = parseInt($('#' + id).val());
            $('#' + id).val(qty + 1);
            apiAddCart(id, qty + 1);
        }

        function stepDownCart(id) {
            qty = parseInt($('#' + id).val());
            if (qty < 2) {
                $('#' + id).val(1);
                apiAddCart(id, 1);
                console.log('OK 1')
            } else {
                $('#' + id).val(qty - 1);
                apiAddCart(id, qty - 1);
                console.log('OK 2')
            }
        }

        function changeQtyCart(id) {
            qty = parseInt($('#' + id).val());
            apiAddCart(id, qty);
        }

        $(document).ready(function() {

            $('.owl-carousel-menu').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                // autoplay: true,
                // autoplayTimeout: 3000,
                // autoplayHoverPause: true,
                // center: true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });

            $('.owl-carousel-testimoni').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
                center: true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 1
                    }
                }
            });
        });
    </script>
</body>

</html>