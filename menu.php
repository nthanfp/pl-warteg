<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('lib/config.php');
$config['pagename'] = 'Menu';
$config['title']    = $config['name'] . ' - ' . $config['pagename'];
if ($_SESSION['cst_status'] == 'login') {
    $id_cust    = $_SESSION['cst_id'];
    $query      = mysqli_query($conn, "SELECT * FROM `wrtg_customer` WHERE `id_customer`='$id_cust'");
    $cust       = mysqli_fetch_assoc($query);
}
?>
<!DOCTYPE html>
<html>

<head>
    <?php include('inc/head.phtml') ?>
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?= $config['host']; ?>/assets/external/css/menu.css" />
</head>

<body>

    <?php include('inc/sidebar.phtml'); ?>

    <div class="w3-main" style="margin-left: 340px; margin-right: 40px">

        <?php include('inc/headbox.phtml'); ?>

        <!-- Content -->
        <div class="w3-container" style="margin-top: 10px">
            <h1 class="fs-2" style="color: #2980b9;"><b>Menu Warteg Bahari</b></h1>
            <hr style="width: 50px; border: 5px solid #2980b9; " class="w3-round" />

            <div class="mt-2">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav justify-content-center pt-3" id="myTab" role="tablist">
                    <li class="nav-item">
                        <button class="nav-link active" id="makanan-tab" data-bs-toggle="tab" data-bs-target="#makanan" type="button" role="tab" aria-controls="home" aria-selected="true">Makanan</button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Minuman</button>
                    </li>
                </ul>
                <!-- End: Nav Tab -->

                <!-- Tab Panel -->
                <div class="tab-content mt-3" id="myTabContent">
                    <!-- Makanan -->
                    <div class="tab-pane fade show active" id="makanan" role="tabpanel" aria-labelledby="makanan-tab">
                        <div class="row row-cols-1 row-cols-md-3 g-4 py-5 tableMenu">
                            <?php
                            $query  = "SELECT * FROM `wrtg_menu` WHERE `id_category` IN('105', '106', '108', '109', '112') ORDER BY `menu_name` ASC";
                            $query  = mysqli_query($conn, $query);
                            while ($menu = mysqli_fetch_assoc($query)) {
                                $id_menu = $menu['id_menu'];
                                $sold = mysqli_query($conn, "SELECT `id_menu`, COUNT(id_menu) AS `menu_sold` FROM  `wrtg_order_detail` WHERE `id_menu`='$id_menu'") or die(mysqli_error($conn));
                                $sold = mysqli_fetch_assoc($sold);
                                $space = '';
                                for ($i = 0; $i < (50 - strlen($menu['menu_description'])); $i++) {
                                    $space .= '-';
                                }
                            ?>
                                <div class="col">
                                    <div class="card">
                                        <img src="<?= $config['host']; ?>/<?= $menu['menu_images']; ?>" class="card-img-top" alt="<?= $menu['menu_name']; ?>">
                                        <div class="card-body">
                                            <h5 class="card-title"><?= $menu['menu_name']; ?></h5>
                                            <p class="card-text"><?= $menu['menu_description']; ?> <?= str_replace('-', '&nbsp;', $space); ?></p>
                                        </div>
                                        <div class="mb-5 mx-4 d-flex justify-content-between">
                                            <h3><?= rupiah($menu['menu_price']); ?></h3>
                                            <button class="btn form-control btn-primary" onclick="openCart('<?= $menu['id_menu']; ?>');" id_menu="<?= $menu['id_menu']; ?>"><i class="fas fa-cart-plus"></i> Beli</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="cartModal-<?= $menu['id_menu']; ?>" tabindex="-1" aria-labelledby="cartModal-<?= $menu['id_menu']; ?>-Label" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title text-secondary" id="cartModal-<?= $menu['id_menu']; ?>-Label"><?= $menu['menu_name']; ?></h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <th>Menu Terjual</th>
                                                            <th width="10">:</th>
                                                            <td><?= $sold['menu_sold']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Stok Menu</th>
                                                            <th width="10">:</th>
                                                            <td><?= $menu['menu_stock']; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="d-flex">
                                                    <button class="btn btn-link px-2" onclick="stepDownCart('<?= $menu['id_menu']; ?>')"><i class="fas fa-minus"></i></button>
                                                    <input onchange="changeQtyCart('<?= $menu['id_menu']; ?>')" id="<?= $menu['id_menu']; ?>" min="0" name="quantity" value="1" type="number" class="form-control form-control-sm">
                                                    <button class="btn btn-link px-2" onclick="stepUpCart('<?= $menu['id_menu']; ?>')"><i class="fas fa-plus"></i></button>
                                                    <button class="btn form-control btn-primary" onclick="addToCartV2('<?= $menu['id_menu']; ?>');" id_menu=""><i class="fas fa-cart-plus"></i> Beli</button>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End: Modal -->
                            <?php } ?>
                        </div>
                    </div>

                    <!-- Minuman -->
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="tab-pane fade show active" id="makanan" role="tabpanel" aria-labelledby="makanan-tab">
                            <div class="row row-cols-1 row-cols-md-3 g-4 py-5">
                                <?php
                                $query  = "SELECT * FROM `wrtg_menu` WHERE `id_category` IN('107', '110', '111') ORDER BY `menu_name` ASC";
                                $query  = mysqli_query($conn, $query) or die(mysqli_error($conn));
                                while ($menu = mysqli_fetch_assoc($query)) {
                                    $id_menu = $menu['id_menu'];
                                    $sold = mysqli_query($conn, "SELECT `id_menu`, COUNT(id_menu) AS `menu_sold` FROM  `wrtg_order_detail` WHERE `id_menu`='$id_menu'") or die(mysqli_error($conn));
                                    $sold = mysqli_fetch_assoc($sold);
                                    $space  = '';
                                    for ($i = 0; $i < (50 - strlen($menu['menu_description'])); $i++) {
                                        $space .= '-';
                                    }
                                ?>
                                    <div class="col">
                                        <div class="card">
                                            <img src="<?= $config['host']; ?>/<?= $menu['menu_images']; ?>" class="card-img-top" alt="<?= $menu['menu_name']; ?>">
                                            <div class="card-body">
                                                <h5 class="card-title"><?= $menu['menu_name']; ?></h5>
                                                <p class="card-text"><?= $menu['menu_description']; ?> <?= str_replace('-', '&nbsp;', $space); ?></p>
                                            </div>
                                            <div class="mb-5 mx-4 d-flex justify-content-between">
                                                <h3><?= rupiah($menu['menu_price']); ?></h3>
                                                <button class="btn form-control btn-primary" onclick="openCart('<?= $menu['id_menu']; ?>');" id_menu="<?= $menu['id_menu']; ?>"><i class="fas fa-cart-plus"></i> Beli</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                    <div class="modal fade" id="cartModal-<?= $menu['id_menu']; ?>" tabindex="-1" aria-labelledby="cartModal-<?= $menu['id_menu']; ?>-Label" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-secondary" id="cartModal-<?= $menu['id_menu']; ?>-Label"><?= $menu['menu_name']; ?></h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tbody>
                                                            <tr>
                                                                <th>Menu Terjual</th>
                                                                <th width="10">:</th>
                                                                <td><?= $sold['menu_sold']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Stok Menu</th>
                                                                <th width="10">:</th>
                                                                <td><?= $menu['menu_stock']; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="d-flex">
                                                        <button class="btn btn-link px-2" onclick="stepDownCart('<?= $menu['id_menu']; ?>')"><i class="fas fa-minus"></i></button>
                                                        <input onchange="changeQtyCart('<?= $menu['id_menu']; ?>')" id="<?= $menu['id_menu']; ?>" min="0" name="quantity" value="1" type="number" class="form-control form-control-sm">
                                                        <button class="btn btn-link px-2" onclick="stepUpCart('<?= $menu['id_menu']; ?>')"><i class="fas fa-plus"></i></button>
                                                        <button class="btn form-control btn-primary" onclick="addToCartV2('<?= $menu['id_menu']; ?>');" id_menu=""><i class="fas fa-cart-plus"></i> Beli</button>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: Modal -->
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End: Tab Panel -->

            </div>
        </div>
        <!-- End Content -->
    </div>

    <?php include('inc/foot.phtml'); ?>
    <script type="text/javascript">
        function apiAddCart(id_menu, qty) {
            let url = '<?= $config['host']; ?>/api/v1/cart?method=ADD_CART';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    id_menu,
                    qty
                },
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        swal("Berhasil", response.content, "success");
                    } else {
                        swal("Error", response.content, "error");
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function addToCart(id_menu) {
            apiAddCart(id_menu, 1);
        }

        function addToCartV2(id_menu) {
            qty = parseInt($('#' + id_menu).val());
            apiAddCart(id_menu, qty);
        }

        function openCart(id_menu) {
            $('#cartModal-' + id_menu).modal('show');
            $.ajax({
                type: "POST",
                url: '<?= $config['host']; ?>/api/v1/cart?method=GET_CART',
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    var myCart = response.cart;
                    if (myCart.data == null) {} else if (((myCart.data).length < 1)) {} else {
                        $.each(myCart.data, function(i, val) {
                            if (val.id_menu === id_menu) {
                                $('#' + id_menu).val(val.qty);
                            }
                        });
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');;
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                    $('#' + id_menu).val(1);
                }
            });
        }

        function stepUpCart(id) {
            qty = parseInt($('#' + id).val());
            $('#' + id).val(qty + 1);
            apiAddCart(id, qty + 1);
        }

        function stepDownCart(id) {
            qty = parseInt($('#' + id).val());
            if (qty < 2) {
                $('#' + id).val(1);
                apiAddCart(id, 1);
                console.log('OK 1')
            } else {
                $('#' + id).val(qty - 1);
                apiAddCart(id, qty - 1);
                console.log('OK 2')
            }
        }

        function changeQtyCart(id) {
            qty = parseInt($('#' + id).val());
            apiAddCart(id, qty);
        }

        $(document).ready(function() {});
    </script>
</body>

</html>