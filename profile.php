<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('lib/config.php');
$config['pagename'] = 'Profil';
$config['title']    = $config['name'] . ' - ' . $config['pagename'];
if (isset($_SESSION['cst_status']) && ($_SESSION['cst_status'] == 'login')) {
    $id_cust    = $_SESSION['cst_id'];
    $query      = mysqli_query($conn, "SELECT * FROM `wrtg_customer` WHERE `id_customer`='$id_cust'");
    $cust       = mysqli_fetch_assoc($query);
} else {
    header('Location: ' . $config['host'] . '/login');
    exit;
}
?>
<!DOCTYPE html>
<html>

<head>
    <?php include('inc/head.phtml') ?>
</head>

<body>

    <?php include('inc/sidebar.phtml'); ?>

    <div class="w3-main" style="margin-left: 340px; margin-right: 40px">

        <?php include('inc/headbox.phtml'); ?>

        <!-- Content -->
        <div class="w3-container" style="margin-top: 10px; margin-bottom: 80px;">
            <div class="row">
                <div>
                    <h1 class="fs-2" style="color: #2980b9;"><b>Profil</b></h1>
                    <hr style="width:50px; border: 5px solid #2980b9; " class="w3-round">
                </div>
                <div class="col-xl-6">
                    <div class="alert alert-primary">Akun anda telah terdaftar sejak <?= date('d M Y H:i:s', $cust['cst_register_date']); ?></div>
                    <div class="card mb-4 mb-xl-0">
                        <div class="card-header">Foto Profil</div>
                        <div class="card-body text-center">
                            <!-- Profile picture image-->
                            <img class="img-fluid rounded-circle mb-2" src="<?= get_gravatar($cust['cst_email']); ?>" alt="Foto Profil" width="150">
                            <!-- Profile picture help block-->
                            <div class="small font-italic text-muted mb-2">JPG atau PNG dan ukuran file tidak lebih dari 2MB</div>
                            <!-- Profile picture upload button-->
                            <button class="btn btn-primary" type="button">Unggah Foto Profil Baru</button>
                        </div>
                    </div>
                    <div class="card mt-4 mb-4">
                        <div class="card-header">Data Akun</div>
                        <div class="card-body">
                            <form>
                                <div class="mb-3">
                                    <label class="small mb-1">Email</label>
                                    <input class="form-control" id="cst_email" name="cst_email" type="text" placeholder="" value="<?= $cust['cst_email']; ?>" readonly>
                                </div>
                                <div class="mb-3">
                                    <label class="small mb-1">No Telepon</label>
                                    <input class="form-control" id="cst_phone" name="cst_phone" type="text" placeholder="" value="<?= $cust['cst_phone']; ?>" readonly>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="card mb-4">
                        <div class="card-header">Informasi Pribadi</div>
                        <div class="card-body">
                            <form>
                                <div class="mb-3">
                                    <label class="small mb-1">Nama Lengkap</label>
                                    <input class="form-control" id="cst_full_name" name="cst_full_name" type="text" placeholder="" value="<?= $cust['cst_full_name']; ?>">
                                </div>

                                <?php if (empty($cust['cst_addr_state'])) { ?>
                                    <div class="row gx-3">
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Provinsi</label>
                                            <select class="form-control" id="form_province" name="addr_province">
                                                <option value="">-- Pilih Provinsi --</option>
                                                <?php
                                                $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE CHAR_LENGTH(kode)=2 ORDER BY `nama`");
                                                while ($d = mysqli_fetch_array($daerah)) {
                                                    if ($cust['cst_addr_state'] == $d['nama']) {
                                                        $cst_code_province = $d['kode'];
                                                        echo '<option value="' . $d['kode'] . '" selected>' . $d['nama'] . '</option>';
                                                    } else {
                                                        echo '<option value="' . $d['kode'] . '">' . $d['nama'] . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Kota/Kabupaten</label>
                                            <select class="form-control" id="form_city" name="addr_city">
                                                <option value="">-- Pilih Kota/Kab --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row gx-3">
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Kecamatan</label>
                                            <select class="form-control" id="form_district" name="addr_district">
                                                <option value="">-- Pilih Kecamatan --</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Kelurahan</label>
                                            <select class="form-control" id="form_village" name="addr_village">
                                                <option value="">-- Pilih Kelurahan --</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="row gx-3">
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Provinsi</label>
                                            <select class="form-control" id="form_province" name="addr_province">
                                                <option value="">-- Pilih Provinsi --</option>
                                                <?php
                                                $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE CHAR_LENGTH(kode)=2 ORDER BY `nama`");
                                                while ($d = mysqli_fetch_array($daerah)) {
                                                    if ($cust['cst_addr_state'] == $d['nama']) {
                                                        $cst_code_province = $d['kode'];
                                                        echo '<option value="' . $d['kode'] . '" selected>' . $d['nama'] . '</option>';
                                                    } else {
                                                        echo '<option value="' . $d['kode'] . '">' . $d['nama'] . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Kota/Kabupaten</label>
                                            <select class="form-control" id="form_city" name="addr_city">
                                                <option value="">-- Pilih Kota/Kab --</option>
                                                <?php
                                                $id = $cst_code_province;
                                                $n  = strlen($id);
                                                $m  = ($n == 2 ? 5 : ($n == 5 ? 8 : 13));
                                                $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE LEFT(`kode`,'$n')='$id' AND CHAR_LENGTH(kode)='$m' ORDER BY `nama`");
                                                while ($d = mysqli_fetch_array($daerah)) {
                                                    if ($cust['cst_addr_city'] == $d['nama']) {
                                                        $cst_code_city = $d['kode'];
                                                        echo '<option value="' . $d['kode'] . '" selected>' . $d['nama'] . '</option>';
                                                    } else {
                                                        echo '<option value="' . $d['kode'] . '">' . $d['nama'] . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row gx-3">
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Kecamatan</label>
                                            <select class="form-control" id="form_district" name="addr_district">
                                                <option value="">-- Pilih Kecamatan --</option>
                                                <?php
                                                $id = $cst_code_city;
                                                $n  = strlen($id);
                                                $m  = ($n == 2 ? 5 : ($n == 5 ? 8 : 13));
                                                $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE LEFT(`kode`,'$n')='$id' AND CHAR_LENGTH(kode)='$m' ORDER BY `nama`");
                                                while ($d = mysqli_fetch_array($daerah)) {
                                                    if ($cust['cst_addr_district'] == $d['nama']) {
                                                        $cst_code_district = $d['kode'];
                                                        echo '<option value="' . $d['kode'] . '" selected>' . $d['nama'] . '</option>';
                                                    } else {
                                                        echo '<option value="' . $d['kode'] . '">' . $d['nama'] . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Kelurahan</label>
                                            <select class="form-control" id="form_village" name="addr_village">
                                                <option value="">-- Pilih Kelurahan --</option>
                                                <?php
                                                $id = $cst_code_district;
                                                $n  = strlen($id);
                                                $m  = ($n == 2 ? 5 : ($n == 5 ? 8 : 13));
                                                $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE LEFT(`kode`,'$n')='$id' AND CHAR_LENGTH(kode)='$m' ORDER BY `nama`");
                                                while ($d = mysqli_fetch_array($daerah)) {
                                                    if ($cust['cst_addr_village'] == $d['nama']) {
                                                        $cst_code_village = $d['kode'];
                                                        echo '<option value="' . $d['kode'] . '" selected>' . $d['nama'] . '</option>';
                                                    } else {
                                                        echo '<option value="' . $d['kode'] . '">' . $d['nama'] . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="mb-3">
                                    <label class="small mb-1">Alamat Lengkap</label>
                                    <textarea rows="2" class="form-control" id="cst_address" type="text" placeholder="Alamat Lengkap"><?= $cust['cst_full_address']; ?></textarea>
                                </div>
                                <div class="mb-3">
                                    <button class="btn btn-primary" type="button" onclick="saveProfile()">Simpan Profil</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card mt-4 mb-4">
                        <div class="card-header">Ganti Kata Sandi</div>
                        <div class="card-body">
                            <form>
                                <div class="mb-3">
                                    <label class="small mb-1">Kata Sandi Saat Ini</label>
                                    <input type="password" class="form-control" id="pass_current" name="pass_current" placeholder="Kata Sandi Saat Ini">
                                </div>
                                <div class="mb-3">
                                    <label class="small mb-1">Kata Sandi Baru</label>
                                    <input type="password" class="form-control" id="pass_new" name="pass_new" placeholder="Kata Sandi Baru">
                                </div>
                                <div class="mb-3">
                                    <label class="small mb-1">Konfirmasi Kata Sandi Baru</label>
                                    <input type="password" class="form-control" id="pass_new_confirm" name="pass_new_confirm" placeholder="Konfirmasi Kata Sandi Baru">
                                </div>
                                <div class="mb-3">
                                    <button class="btn btn-primary" type="button" onclick="changePassword()">Ganti Password</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Content -->
    </div>

    <?php include('inc/foot.phtml'); ?>
    <script type="text/javascript">
        function saveProfile() {
            let cst_full_name = $('#cst_full_name').val();
            let cst_full_address = $('#cst_address').val();
            let cst_addr_state = $('#form_province option:selected').html();
            let cst_addr_city = $('#form_city option:selected').html();
            let cst_addr_district = $('#form_district option:selected').html();
            let cst_addr_village = $('#form_village option:selected').html();

            let url = '<?= $config['host']; ?>/api/v1/profile?method=UPDATE_PROFILE';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    cst_full_name,
                    cst_full_address,
                    cst_addr_state,
                    cst_addr_city,
                    cst_addr_district,
                    cst_addr_village
                },
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        swal("Berhasil", response.content, "success");
                    } else {
                        swal("Error", response.content, "error");
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function changePassword() {
            let pass_current = $('#pass_current').val();
            let pass_new = $('#pass_new').val();
            let pass_new_confirm = $('#pass_new_confirm').val();

            let url = '<?= $config['host']; ?>/api/v1/profile?method=CHANGE_PASSWORD';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    pass_current,
                    pass_new,
                    pass_new_confirm
                },
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        swal("Berhasil", response.content, "success");
                    } else {
                        swal("Error", response.content, "error");
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function getProfile() {
            $.ajax({
                type: 'GET',
                url: '<?= $config['host']; ?>/api/v1/profile?method=GET_PROFILE',
                success: function(response) {
                    function myPromise1(response) {
                        return new Promise(function(resolve, reject) {
                            $("#form_province > option").each(function() {
                                if (response.data.cst_addr_state == this.text) {
                                    // alert(this.value + ' ' + this.text);
                                    $("#form_province").val(this.value);
                                    $("#form_province > option[value=" + this.value + "]").prop("selected", true);
                                    loadAddress('CITY', this.value);
                                }
                            });
                            resolve("I love You !!");
                        });
                    }

                    function myPromise2(response) {
                        return new Promise(function(resolve, reject) {
                            alert('OK1');
                            alert($("#form_city").val());
                            $("#form_city > option").each(function() {
                                alert('OK2');
                                alert(this.value + ' ' + this.text);
                                if (response.data.cst_addr_city == this.text) {
                                    $("#form_city").val(this.value);
                                    $("#form_city > option[value=" + this.value + "]").prop("selected", true);
                                    loadAddress('DISTRICT', this.value);
                                }
                            });
                            resolve("I Hatee You !!");
                        });
                    }

                    myPromise1(response).then(myPromise2(response));
                },
                beforeSend: function(a, b, c) {},
                error: function(a, b, c) {}
            });
        }

        function loadAddress(type, id) {
            switch (type) {
                case 'CITY':
                    $.ajax({
                        type: 'GET',
                        url: '<?= $config['host']; ?>/api/v1/region?data=CITY&id=' + id,
                        success: function(response) {
                            $("#form_city").append('<option value="">-- Pilih Kota/Kab --</option>');
                            $.each(response.data, function(i, val) {
                                $("#form_city").append('<option value="' + val.kode + '">' + val.nama + '</option>');
                            });
                        },
                        beforeSend: function(a, b, c) {
                            $("#form_city").html('');
                        },
                        error: function(a, b, c) {
                            $("#form_city").append('<option value="">-- Pilih Kota/Kab --</option>');
                        }
                    });
                    break;

                default:
                    break;
            }
        }

        $(document).ready(function() {

            // getProfile();

            $('body').on("change", "#form_province", function() {
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '<?= $config['host']; ?>/api/v1/region?data=CITY&id=' + id,
                    success: function(response) {
                        $("#form_city").append('<option value="">-- Pilih Kota/Kab --</option>');
                        $.each(response.data, function(i, val) {
                            $("#form_city").append('<option value="' + val.kode + '">' + val.nama + '</option>');
                        });
                    },
                    beforeSend: function(a, b, c) {
                        $("#form_city").html('');
                    },
                    error: function(a, b, c) {
                        $("#form_city").append('<option value="">-- Pilih Kota/Kab --</option>');
                    }
                });
            });

            // ambil data kecamatan/kota ketika data memilih kabupaten
            $('body').on("change", "#form_city", function() {
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '<?= $config['host']; ?>/api/v1/region?data=DISTRICT&id=' + id,
                    success: function(response) {
                        $("#form_district").append('<option value="">-- Pilih Kecamatan --</option>');
                        $.each(response.data, function(i, val) {
                            $("#form_district").append('<option value="' + val.kode + '">' + val.nama + '</option>');
                        });
                    },
                    beforeSend: function(a, b, c) {
                        $("#form_district").html('');
                    },
                    error: function(a, b, c) {
                        $("#form_district").append('<option value="">-- Pilih Kota/Kab --</option>');
                    }
                });
            });

            // ambil data desa ketika data memilih kecamatan/kota
            $('body').on("change", "#form_district", function() {
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '<?= $config['host']; ?>/api/v1/region?data=VILLAGE&id=' + id,
                    success: function(response) {
                        $("#form_village").append('<option value="">-- Pilih Kecamatan --</option>');
                        $.each(response.data, function(i, val) {
                            $("#form_village").append('<option value="' + val.kode + '">' + val.nama + '</option>');
                        });
                    },
                    beforeSend: function(a, b, c) {
                        $("#form_village").html('');
                    },
                    error: function(a, b, c) {
                        $("#form_village").append('<option value="">-- Pilih Kota/Kab --</option>');
                    }
                });
            });


        });
    </script>
</body>

</html>