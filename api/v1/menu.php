<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../../lib/config.php');
header('Content-Type: application/json');

if ($_SESSION['cst_status'] <> 'login') {
    $response['status']     = 0;
    $response['content']    = "Silahkan masuk terlebih dahulu";
} else if (!($_GET['method'])) {
    $response['status']     = 0;
    $response['content']    = 'Method not found!';
} else if ($_GET['method'] == 'GET_MENU') {
    if (empty($_POST['category'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi kode kupon";
    } else {
        $category       = strtoupper(mysqli_real_escape_string($conn, stripslashes($_POST['category'])));
        $id_customer    = mysqli_real_escape_string($conn, stripslashes($_SESSION['cst_id']));

        if ($category == 'FOOD') {
            $ext_where = "WHERE `id_category` IN('105', '106', '108', '109', '112')";
        } else if ($category == 'BEVERAGE') {
            $ext_where = "WHERE `id_category` IN('107', '110', '111')";
        } else {
            $ext_where = '';
        }

        $query  = "SELECT * FROM `wrtg_menu` $ext_where ORDER BY `menu_name` ASC";
        $query  = mysqli_query($conn, $query);
        $temp   = array();
        while ($menu = mysqli_fetch_assoc($query)) {
            array_push($temp, $menu);
        }

        $response['status']     = 1;
        $response['content']    = "Data menu";
        $response['data']       = $temp;
    }
} else if ($_GET['method'] == 'CANCEL') {
    $_SESSION['cart_cst_voucher'] = array();

    $response['status']     = 1;
    $response['content']    = 'OK';
    $response['data']       = null;
}

echo json_encode($response);
