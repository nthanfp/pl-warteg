<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../../lib/config.php');
header('Content-Type: application/json');

function getCart()
{
    return $_SESSION['cart_cst'];
}

function setCart($data)
{
    $_SESSION['cart_cst'] = $data;
    return true;
}

function addMenuToCart($id_menu, $qty = 1)
{
    global $conn;

    if ($id_menu == null) {
        return array(0, 'ID Produk/Menu tidak ada');
    }

    $query  = "SELECT * FROM `wrtg_menu` WHERE `id_menu`='$id_menu'";
    $menu   = mysqli_query($conn, $query);
    if (mysqli_num_rows($menu) < 1) {
        return array(0, 'Produk/Menu tidak ditemukan');
    }

    $menu = mysqli_fetch_assoc($menu);
    $cart = isset($_SESSION['cart_cst']) ? $_SESSION['cart_cst'] : array();

    $menu_exists = false;
    foreach ($cart as $index => $item) {
        if ($item['id_menu'] == $id_menu) {
            if ($qty >= 1) {
                $cart[$index]['qty'] = $qty;
                $menu_exists = true;
            } else {
                $cart[$index]['qty'] += $qty;
                $menu_exists = true;
            }
            break;
        }
    }

    if (!$menu_exists) {
        $item = $menu;
        $item['qty'] = $qty;

        $cart[] = $item;
    }

    setCart($cart);

    return array(1, 'Menu berhasil ditambahkan ke keranjang');
}

function removeMenuFromCart($id_menu)
{
    $cart = getCart();
    for ($i = 0; $i < count($cart); $i++) {
        if ($cart[$i]['id_menu'] == $id_menu) {
            unset($cart[$i]);
        }
    }
    sort($cart);
    setCart($cart);
}

function calculateCart()
{

    $subTotal       = 0;
    $discountAmount = 0;
    $taxAmount      = 0;
    $totalAmount    = 0;
    $ord_data       = getCart();

    foreach ($ord_data as $data) {
        $dtl_qty    = $data['qty'];
        $dtl_price  = $data['menu_price'];
        $dtl_id     = $data['id_menu'];
        $dtl_total  = ($data['qty'] * $data['menu_price']);
        $subTotal  += $dtl_total;
    }

    if (isset($_SESSION['cart_cst_voucher'])) {
        $voucher    = $_SESSION['cart_cst_voucher'];
        $validation = validCoupon($voucher['voc_code'], $subTotal, $_SESSION['cst_id']);
        if ($validation['result'] == 1) {
            $taxPercentage  = 10;
            $discountAmount = $validation['cut_price'];
            $taxAmount      = $subTotal * $taxPercentage / 100;
            $totalAmount    = ($subTotal - $discountAmount) + $taxAmount;
        } else {
            $taxPercentage  = 10;
            $taxAmount      = $subTotal * $taxPercentage / 100;
            $totalAmount    = ($subTotal - $discountAmount) + $taxAmount;
        }
    } else {
        $taxPercentage  = 10;
        $taxAmount      = $subTotal * $taxPercentage / 100;
        $totalAmount    = ($subTotal - $discountAmount) + $taxAmount;
    }

    return array(
        'amount_sub_total' => $subTotal,
        'amount_discount' => $discountAmount,
        'amount_tax' => $taxAmount,
        'amount_total' => $totalAmount
    );
}

function getVoucher()
{
    return $_SESSION['cart_cst_voucher'];
}

function setVoucher($data)
{
    $_SESSION['cart_cst_voucher'] = $data;
    return true;
}

function validCoupon($coupon, $price, $id_customer)
{
    global $conn;
    $array      = array();
    $code       = mysqli_real_escape_string($conn, stripslashes(strtoupper($coupon)));

    // Query-1
    $query      = mysqli_query($conn, "SELECT * FROM `wrtg_voucher` WHERE `voc_code`='$code'");
    $voucher    = mysqli_fetch_assoc($query);
    $id_voucher = $voucher['id_voucher'];

    // Query-2-3
    $query2     = mysqli_query($conn, "SELECT * FROM `wrtg_voucher_used` WHERE `id_voucher`='$id_voucher'");
    $query3     = mysqli_query($conn, "SELECT * FROM `wrtg_voucher_used` WHERE `id_voucher`='$code' AND `id_customer`='$id_customer'");

    $voucher['voc_max_used'] = ($voucher['voc_max_used'] == 'unlimited') ? '999999' : $voucher['voc_max_used'];
    $voucher['voc_max_used_user'] = ($voucher['voc_max_used_user'] == 'unlimited') ? '999999' : $voucher['voc_max_used_user'];
    if (mysqli_num_rows($query) < 1) {
        $array['result']        = 0;
        $array['content']       = 'Kode kupon tidak valid';
        $array['code']          = $code;
        $array['ori_price']     = $price;
        $array['fix_price']     = $price;
        $array['cut_price']     = round(0);
    } else if ($price < $voucher['voc_min_spend']) {
        $array['result']        = 0;
        $array['content']       = 'Minimal pesanan ' . rupiah($voucher['voc_min_spend']) . ' untuk mengggunakan promo ini';
        $array['code']          = $code;
        $array['fix_price']     = $price;
        $array['ori_price']     = $price;
        $array['fix_price']     = $price;
        $array['cut_price']     = round(0);
    } else if (mysqli_num_rows($query2) >= $voucher['voc_max_used']) {
        $array['result']        = 0;
        $array['content']       = 'Kode kupon sudah ditukarkan secara penuh';
        $array['code']          = $code;
        $array['ori_price']     = $price;
        $array['fix_price']     = $price;
        $array['cut_price']     = round(0);
    } else if (mysqli_num_rows($query3) >= $voucher['voc_max_used_user']) {
        $array['result']        = 0;
        $array['content']       = 'Kode kupon sudah pernah ditukarkan dengan akun ini';
        $array['code']          = $code;
        $array['ori_price']     = $price;
        $array['fix_price']     = $price;
        $array['cut_price']     = round(0);
    } else if (strtoupper($voucher['voc_type']) == 'PERCENTAGE') {
        $cut                    = round(($voucher['voc_amount'] / 100) * $price);
        $cut                    = ($cut > $voucher['voc_discount_max']) ?  $voucher['voc_discount_max'] : $cut;
        $fixed                  = round(($price - $cut));
        $array['result']        = 1;
        $array['content']       = 'Success to use coupon';
        $array['code']          = $code;
        $array['ori_price']     = $price;
        $array['fix_price']     = $fixed;
        $array['cut_price']     = $cut;
    } else if (strtoupper($voucher['voc_type']) == 'FIXED') {
        $cut                    = round(($voucher['voc_amount']));
        $fixed                  = round((($price - $cut)));
        $array['result']        = 1;
        $array['content']       = 'Success to use coupon';
        $array['code']          = $code;
        $array['ori_price']     = $price;
        $array['fix_price']     = ($fixed < 0) ? 0 : $fixed;
        $array['cut_price']     = $cut;
    }

    return $array;
}

if ($_SESSION['cst_status'] <> 'login') {
    $response['status']     = 0;
    $response['content']    = "Silahkan masuk terlebih dahulu";
} else if (!($_GET['method'])) {
    $response['status']     = 0;
    $response['content']    = 'Method not found!';
} else if ($_GET['method'] == 'ADD_CART') {
    if (empty($_POST['id_menu'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi ID menu";
    } else if (empty($_POST['qty'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi quantity";
    } else {
        $id_menu    = mysqli_real_escape_string($conn, stripslashes($_POST['id_menu']));
        $qty        = mysqli_real_escape_string($conn, stripslashes($_POST['qty']));

        $query      = "SELECT * FROM `wrtg_menu` WHERE `id_menu`='$id_menu'";
        $menu       = mysqli_query($conn, $query);

        if (!$menu) {
            $response['status']    = 0;
            $response['content']   = 'Error! ' . mysqli_error($conn);
        } else if (mysqli_num_rows($menu) < 1) {
            $response['status']    = 0;
            $response['content']   = 'Data tidak ditemukan';
        } else {
            $menu   = mysqli_fetch_assoc($menu);
            $stock  = $menu['menu_stock'];
            $add    = addMenuToCart($id_menu, $qty);
            if ($add[0] == 1) {
                $response['status']    = 1;
                $response['content']   = 'Sukses menambahkan menu ke keranjang!';
            } else {
                $response['status']    = 0;
                $response['content']   = 'Gagal menambahkan menu ke keranjang!';
            }
        }
    }
} else if ($_GET['method'] == 'GET_CART') {
    // Init
    $calcCart               = calculateCart();
    $response['status']     = 1;
    $response['content']    = 'Data keranjang';
    // $response['data']       = getCart();
    $response['cart']       = array(
        'data' => getCart(),
        'voucher' => getVoucher(),
        'amount_sub_total' => $calcCart['amount_sub_total'],
        'amount_discount' => $calcCart['amount_discount'],
        'amount_tax' => $calcCart['amount_tax'],
        'amount_total' => $calcCart['amount_total']
    );
} else if ($_GET['method'] == 'DELETE_CART') {
    $id_menu    = mysqli_real_escape_string($conn, stripslashes($_POST['id_menu']));
    $delete     = removeMenuFromCart($id_menu);
    $response['status']    = 1;
    $response['content']   = 'Sukses menghapus menu dari keranjang!';
} else if ($_GET['method'] == 'CHECKOUT') {
    if (empty($_POST['ord_name'])) {
        $response['status']     = 0;
        $response['content']    = 'Harap lengkapi nama pemesan';
    } else if (empty($_POST['ord_type'])) {
        $response['status']     = 0;
        $response['content']    = 'Harap lengkapi tipe pesanan';
    } else if (empty($_POST['ord_payment'])) {
        $response['status']     = 0;
        $response['content']    = 'Harap lengkapi metode pembayaran';
    } else if (count(getCart()) < 1) {
        $response['status']    = 0;
        $response['content']   = 'Keranjang kosong!';
    } else {
        // Required
        $ord_customer       = mysqli_escape_string($conn, stripslashes(trim($_SESSION['cst_id'])));
        $ord_name           = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_name'])));
        $ord_type           = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_type'])));
        $ord_payment        = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_payment'])));
        $ord_notes          = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_notes'])));
        $ord_status         = 'Pending';
        $ord_data           = getCart();

        // Optional
        $ord_table_number   = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_table_number'])));
        $ord_address        = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_address'])));

        // Init
        $subTotal       = 0;
        $discountAmount = 0;
        $taxAmount      = 0;
        $totalAmount    = 0;

        $query  = "INSERT INTO `wrtg_order` (`id_customer`, `order_name`, `order_time`, `order_type`, `order_status`, `order_payment_code`, `order_notes`, `order_address`, `order_table`) VALUES ('$ord_customer', '$ord_name', '$time', '$ord_type', '$ord_status', '$ord_payment', '$ord_notes', '$ord_address', '$ord_table_number')";
        $insert = mysqli_query($conn, $query);
        if ($insert) {
            $query  = "SELECT * FROM `wrtg_order` WHERE `order_time`='$time' AND `id_customer`='$ord_customer'";
            $query  = mysqli_query($conn, $query);
            $latest = mysqli_fetch_assoc($query)['id_order'];

            $i = 0;
            foreach ($ord_data as $data) {
                $dtl_qty    = $data['qty'];
                $dtl_price  = $data['menu_price'];
                $dtl_id     = $data['id_menu'];
                $dtl_total  = ($data['qty'] * $data['menu_price']);
                $subTotal  += $dtl_total;

                $query = "INSERT INTO `wrtg_order_detail` (`id_order`, `id_menu`, `qty`, `price`, `total_price`) VALUES ('$latest', '$dtl_id', '$dtl_qty', '$dtl_price', '$dtl_total')";
                $insert = mysqli_query($conn, $query);
                ($insert) ? $i++ : false;

                mysqli_query($conn, "UPDATE `wrtg_menu` SET `menu_stock`=`menu_stock`-1 WHERE `id_menu`='$dtl_id'");
            }

            if (isset($_SESSION['cart_cst_voucher'])) {
                $voucher    = $_SESSION['cart_cst_voucher'];
                $id_voucher = $voucher['id_vocuher'];
                $id_own_v   = $voucher['id_own'];
                $validation = validCoupon($voucher['voc_code'], $subTotal, $_SESSION['cst_id']);
                if ($validation['result'] == 1) {
                    $taxPercentage  = 10;
                    $discountAmount = $validation['cut_price'];
                    $taxAmount      = $subTotal * $taxPercentage / 100;
                    $totalAmount    = ($subTotal - $discountAmount) + $taxAmount;

                    mysqli_query($conn, "INSERT INTO `wrtg_voucher_used` (`id_order`, `id_voucher`, `id_customer`, `used_spend`, `used_amount`, `used_fixed`, `created_at`) VALUES ('$latest', '$id_voucher', '$ord_customer', '$subTotal', '$discountAmount', '$totalAmount', '$time')");
                    mysqli_query($conn, "UPDATE `wrtg_voucher_own` SET `has_used`='Y' WHERE `id_own`='$id_own_v'");
                } else {
                    $taxPercentage  = 10;
                    $taxAmount      = $subTotal * $taxPercentage / 100;
                    $totalAmount    = ($subTotal - $discountAmount) + $taxAmount;
                }
            } else {
                $taxPercentage  = 10;
                $taxAmount      = $subTotal * $taxPercentage / 100;
                $totalAmount    = ($subTotal - $discountAmount) + $taxAmount;
            }

            $orderPayment   = $totalAmount;
            $orderPayBack   = 0;


            if ($ord_payment == 'COD') {
                mysqli_query($conn, "UPDATE `wrtg_order` SET `order_subtotal`='$subTotal', `order_discount`='$discountAmount', `order_tax`='$taxAmount', `order_total`='$totalAmount', `order_pay`='$orderPayment',  `order_pay_back`='$orderPayBack', `order_payment_status`='Pending' WHERE `id_order`='$latest'");
            } else if ($ord_payment == 'QRIS-1') {
                mysqli_query($conn, "UPDATE `wrtg_order` SET `order_subtotal`='$subTotal', `order_discount`='$discountAmount', `order_tax`='$taxAmount', `order_total`='$totalAmount', `order_pay`='$orderPayment',  `order_pay_back`='$orderPayBack', `order_payment_status`='Pending' WHERE `id_order`='$latest'");
            } else if ($ord_payment == 'QRIS-2') {
                mysqli_query($conn, "UPDATE `wrtg_order` SET `order_subtotal`='$subTotal', `order_discount`='$discountAmount', `order_tax`='$taxAmount', `order_total`='$totalAmount', `order_pay`='$orderPayment',  `order_pay_back`='$orderPayBack', `order_payment_status`='Pending' WHERE `id_order`='$latest'");
            } else {
                mysqli_query($conn, "UPDATE `wrtg_order` SET `order_subtotal`='$subTotal', `order_discount`='$discountAmount', `order_tax`='$taxAmount', `order_total`='$totalAmount', `order_pay`='$orderPayment',  `order_pay_back`='$orderPayBack', `order_payment_status`='Pending' WHERE `id_order`='$latest'");
            }

            $response['status']             = 1;
            $response['content']            = 'Berhasil membuat pesanan dengan nomor pesanan: #' . $latest;
            $response['redirect']           = $config['host'] . '/order';
            $response['data']['id_order']   = $latest;

            $wallet_log_name        = 'Pesanan #' . $latest;
            $wallet_log_type        = 'IN';
            $wallet_log_notes       = '-';
            $wallet_log_amount_in   = $totalAmount;
            $wallet_log_amount_out  = 0;
            mysqli_query($conn, "INSERT INTO `wrtg_wallet_log` (`id_wallet`, `type`, `name`, `amount_in`, `amount_out`, `notes`, `created_at`, `updated_at`) VALUES ('901', '$wallet_log_type', '$wallet_log_name', '$wallet_log_amount_in', '$wallet_log_amount_out', '$wallet_log_notes', '$time', '$time')");
            mysqli_query($conn, "UPDATE `wrtg_wallet` SET `wl_balance`=`wl_balance`+$wallet_log_amount_in WHERE `id_wallet`='901'");

            setCart([]);
            setVoucher(array());
        } else {
            $response['status']     = 0;
            $response['content']    = 'Oops.. Nampaknya ada masalah :(';
            // $response['data']       = $_POST;
        }
    }
} else if ($_GET['method'] == 'USE_VOUCHER') {
    if (empty($_POST['voc_code'])) {
        $response['status']     = 1;
        $response['content']    = 'Harap lengkapi kode kupon';
    } else {
        $voc_code       = mysqli_real_escape_string($conn, stripslashes(strtoupper($_POST['voc_code'])));
        $id_own         = mysqli_real_escape_string($conn, stripslashes(strtoupper($_POST['id_own'])));
        $id_customer    = $_SESSION['cst_id'];

        // Validate
        $query  = "SELECT *
        FROM `wrtg_voucher_own` `o`
        INNER JOIN `wrtg_voucher` `v`
            USING(id_voucher)
        WHERE `v`.`voc_code`='$voc_code'
            AND `o`.`id_own`='$id_own'";
        $query  = mysqli_query($conn, $query) or die(mysqli_error($conn));

        if (mysqli_num_rows($query) < 1) {
            $response['status']     = 1;
            $response['content']    = 'Kupon tidak ditemukan';
        } else {
            $voucher        = mysqli_fetch_assoc($query);
            $sub_total      = calculateCart()['amount_sub_total'];
            $add_coupon     = validCoupon($voc_code, $sub_total, $id_customer);
            if ($add_coupon['result'] == 1) {
                setVoucher($voucher);

                $response['status']     = 1;
                $response['content']    = 'Kupon berhasil digunakan';
            } else {
                $response['status']     = 0;
                $response['content']    = 'Kupon tidak dapat digunakan: ' . $add_coupon['content'];
            }
        }
    }
}

echo json_encode($response);
