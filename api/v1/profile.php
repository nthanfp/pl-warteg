<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../../lib/config.php');
header('Content-Type: application/json');

if ($_SESSION['cst_status'] <> 'login') {
    $response['status']     = 0;
    $response['content']    = "Silahkan masuk terlebih dahulu";
} else if (!($_GET['method'])) {
    $response['status']     = 0;
    $response['content']    = 'Method not found!';
} else if ($_GET['method'] == 'UPDATE_PROFILE') {
    if (empty($_POST['cst_full_name'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Nama Lengkap";
    } else if (empty($_POST['cst_full_address'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Alamat Lengkap";
    } else if (empty($_POST['cst_addr_state'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Nama Provinsi";
    } else if (empty($_POST['cst_addr_city'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Nama Kota";
    } else if (empty($_POST['cst_addr_district'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Nama Kecamatan";
    } else if (empty($_POST['cst_addr_village'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Nama Kelurahan";
    } else {
        $id_customer        = mysqli_escape_string($conn, stripslashes(trim($_SESSION['cst_id'])));
        $cst_full_name      = mysqli_real_escape_string($conn, stripslashes($_POST['cst_full_name']));
        $cst_full_address   = mysqli_real_escape_string($conn, stripslashes($_POST['cst_full_address']));
        $cst_addr_state     = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_state']));
        $cst_addr_city      = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_city']));
        $cst_addr_district  = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_district']));
        $cst_addr_village   = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_village']));

        $query  = "UPDATE `wrtg_customer` SET `cst_full_name`='$cst_full_name', `cst_full_address`='$cst_full_address', `cst_addr_state`='$cst_addr_state', `cst_addr_city`='$cst_addr_city', `cst_addr_district`='$cst_addr_district', `cst_addr_village`='$cst_addr_village' WHERE  `id_customer`='$id_customer'";
        $update = mysqli_query($conn, $query);
        if ($update) {
            $response['status']     = 1;
            $response['content']    = "Profil sukses diperbaharui";
        } else {
            $response['status']     = 0;
            $response['content']    = "Profil gagal diperbaharui";
        }
    }
} else if ($_GET['method'] == 'CHANGE_PASSWORD') {
    if (empty($_POST['pass_current'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Kata Sandi ini";
    } else if (empty($_POST['pass_new'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Kata Sandi baru";
    } else if (empty($_POST['pass_new_confirm'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Konfirmasi Kata Sandi baru";
    } else {
        $id_customer        = mysqli_escape_string($conn, stripslashes(trim($_SESSION['cst_id'])));
        $pass_current       = md5(mysqli_real_escape_string($conn, stripslashes($_POST['pass_current'])));
        $pass_new           = md5(mysqli_real_escape_string($conn, stripslashes($_POST['pass_new'])));
        $pass_new_confirm   = md5(mysqli_real_escape_string($conn, stripslashes($_POST['pass_new_confirm'])));

        $query      = "SELECT `id_customer`, `cst_password` FROM `wrtg_customer` WHERE `id_customer`='$id_customer'";
        $select     = mysqli_query($conn, $query);
        $curr_pass  = mysqli_fetch_assoc($select)['cst_password'];

        if ($curr_pass <> $pass_current) {
            $response['status']     = 0;
            $response['content']    = "Kata Sandi saat ini tidak cocok";
        } else if ($pass_new <> $pass_new_confirm) {
            $response['status']     = 0;
            $response['content']    = "Kata Sandi baaru dan konfirmasi tidak cocok";
        } else {
            $query      = "UPDATE `wrtg_customer` SET `cst_password`='$pass_new' WHERE `id_customer`='$id_customer'";
            $update     = mysqli_query($conn, $query);
            if ($update) {
                $response['status']     = 1;
                $response['content']    = "Kata Sandi sukses diperbaharui";
            } else {
                $response['status']     = 0;
                $response['content']    = "Kata Sandi gagal diperbaharui";
            }
        }
    }
} else if ($_GET['method'] == 'GET_PROFILE') {
    $id_cust    = $_SESSION['cst_id'];
    $query      = mysqli_query($conn, "SELECT * FROM `wrtg_customer` WHERE `id_customer`='$id_cust'");
    $cust       = mysqli_fetch_assoc($query);

    $response['status']     = 1;
    $response['content']    = "Profil";
    $response['data']       = $cust;
}

echo json_encode($response);
