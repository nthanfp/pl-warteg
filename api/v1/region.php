<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();

$data   = strtoupper($_GET['data']);
$id     = $_GET['id'];

$n = strlen($id);
$m = ($n == 2 ? 5 : ($n == 5 ? 8 : 13));

$response['data'] = [];
if ($data == 'PROVINCE') {
    $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE CHAR_LENGTH(kode)='2' ORDER BY `nama`");
    while ($d = mysqli_fetch_assoc($daerah)) {
        array_push($response['data'], $d);
    }
} else if ($data == 'CITY') {
    $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE LEFT(`kode`,'$n')='$id' AND CHAR_LENGTH(kode)='$m' ORDER BY `nama`");
    while ($d = mysqli_fetch_assoc($daerah)) {
        array_push($response['data'], $d);
    }
} else if ($data == 'DISTRICT') {
    $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE LEFT(`kode`,'$n')='$id' AND CHAR_LENGTH(kode)='$m' ORDER BY `nama`");
    while ($d = mysqli_fetch_assoc($daerah)) {
        array_push($response['data'], $d);
    }
} else if ($data == 'VILLAGE') {
    $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE LEFT(`kode`,'$n')='$id' AND CHAR_LENGTH(kode)='$m' ORDER BY `nama`");
    while ($d = mysqli_fetch_assoc($daerah)) {
        array_push($response['data'], $d);
    }
}

print_r(json_encode($response));
