<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../../lib/config.php');
header('Content-Type: application/json');

if ($_SESSION['cst_status'] <> 'login') {
    $response['status']     = 0;
    $response['content']    = "Silahkan masuk terlebih dahulu";
} else if (!($_GET['method'])) {
    $response['status']     = 0;
    $response['content']    = 'Method not found!';
} else if ($_GET['method'] == 'CLAIM') {
    if (empty($_POST['voc_code'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi kode kupon";
    } else {
        $voc_code       = mysqli_real_escape_string($conn, stripslashes($_POST['voc_code']));
        $id_customer    = mysqli_real_escape_string($conn, stripslashes($_SESSION['cst_id']));

        // Get coupon data
        $query      = "SELECT * FROM `wrtg_voucher` WHERE `voc_code`='$voc_code'";
        $query      = mysqli_query($conn, $query);
        $voucher    = mysqli_fetch_assoc($query);
        $id_voucher = $voucher['id_voucher'];

        $query2     = mysqli_query($conn, "SELECT * FROM `wrtg_voucher_own` WHERE `id_voucher`='$id_voucher'");
        $query3     = mysqli_query($conn, "SELECT * FROM `wrtg_voucher_own` WHERE `id_voucher`='$id_voucher' AND `id_customer`='$id_customer'");

        if (mysqli_num_rows($query) < 1) {
            $response['status']     = 0;
            $response['content']    = 'Kupon tidak ditemukan!';
        } else if (mysqli_num_rows($query2) >= $voucher['voc_max_used']) {
            $response['status']     = 0;
            $response['content']    = 'Kupon telah diklaim sepenuhnya! ' . mysqli_num_rows($query2) . ' - ' . $voucher['voc_max_used'];
        } else if (mysqli_num_rows($query3) >= $voucher['voc_max_used_user']) {
            $response['status']     = 0;
            $response['content']    = 'Kupon telah diklaim dengan akun anda!';
        } else {
            mysqli_query($conn, "INSERT INTO `wrtg_voucher_own` (`id_voucher`, `id_customer`, `created_at`, `updated_at`) VALUES ('$id_voucher', '$id_customer', '$time', '$time')") or die(mysqli_error($conn));
            $response['status']     = 1;
            $response['content']    = 'Kupon berhasil diklaim';
        }
    }
} else if ($_GET['method'] == 'CANCEL') {
    $_SESSION['cart_cst_voucher'] = array();

    $response['status']     = 1;
    $response['content']    = 'OK';
    $response['data']       = null;
} else if ($_GET['method'] == 'GET_MY') {
    $id_cust    = $_SESSION['cst_id'];
    $query      = "SELECT *
    FROM `wrtg_voucher_own` `o`
    INNER JOIN `wrtg_voucher` `v`
        USING(id_voucher)
    WHERE `id_customer`='$id_cust'
        AND `has_used`='N'";
    $query  = mysqli_query($conn, $query);

    $data = [];
    while ($voc = mysqli_fetch_assoc($query)) {
        array_push($data, $voc);
    }

    $id_own_current = $_SESSION['cart_cst_voucher']['id_own'];
    for ($i = 0; $i < count($data); $i++) {
        if ($id_own_current == $data[$i]['id_own']) {
            $data[$i]['selected'] = true;
        } else {
            $data[$i]['selected'] = false;
        }
    }

    $response['status']     = 1;
    $response['content']    = 'Data keranjang';
    $response['data']       = $data;
} else if ($_GET['method'] == 'DELETE_CART') {
    $id_menu    = mysqli_real_escape_string($conn, stripslashes($_POST['id_menu']));
    $delete     = removeMenuFromCart($id_menu);
    $response['status']    = 1;
    $response['content']   = 'Sukses menghapus menu dari keranjang!';
} else if ($_GET['method'] == 'CHECKOUT') {
    if (empty($_POST['ord_name'])) {
        $response['status']     = 1;
        $response['content']    = 'Harap lengkapi nama pemesan';
    } else if (empty($_POST['ord_type'])) {
        $response['status']     = 0;
        $response['content']    = 'Harap lengkapi tipe pesanan';
    } else if (empty($_POST['ord_payment'])) {
        $response['status']     = 0;
        $response['content']    = 'Harap lengkapi metode pembayaran';
    } else if (count(getCart()) < 1) {
        $response['status']    = 0;
        $response['content']   = 'Keranjang kosong!';
    } else {
        // Required
        $ord_customer       = mysqli_escape_string($conn, stripslashes(trim($_SESSION['cst_id'])));
        $ord_name           = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_name'])));
        $ord_type           = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_type'])));
        $ord_payment        = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_payment'])));
        $ord_notes          = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_notes'])));
        $ord_status         = 'Pending';
        $ord_data           = getCart();

        // Optional
        $ord_table_number   = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_table_number'])));
        $ord_address        = mysqli_escape_string($conn, stripslashes(trim($_POST['ord_address'])));

        // Init
        $subTotal       = 0;
        $discountAmount = 0;
        $taxAmount      = 0;
        $totalAmount    = 0;

        $query  = "INSERT INTO `wrtg_order` (`id_customer`, `order_name`, `order_time`, `order_type`, `order_status`, `order_payment_code`, `order_notes`, `order_address`, `order_table`) VALUES ('$ord_customer', '$ord_name', '$time', '$ord_type', '$ord_status', '$ord_payment', '$ord_notes', '$ord_address', '$ord_table_number')";
        $insert = mysqli_query($conn, $query);
        if ($insert) {
            $query  = "SELECT * FROM `wrtg_order` WHERE `order_time`='$time' AND `id_customer`='$ord_customer'";
            $query  = mysqli_query($conn, $query);
            $latest = mysqli_fetch_assoc($query)['id_order'];

            $i = 0;
            foreach ($ord_data as $data) {
                $dtl_qty    = $data['qty'];
                $dtl_price  = $data['menu_price'];
                $dtl_id     = $data['id_menu'];
                $dtl_total  = ($data['qty'] * $data['menu_price']);
                $subTotal  += $dtl_total;

                $query = "INSERT INTO `wrtg_order_detail` (`id_order`, `id_menu`, `qty`, `price`, `total_price`) VALUES ('$latest', '$dtl_id', '$dtl_qty', '$dtl_price', '$dtl_total')";
                $insert = mysqli_query($conn, $query);
                ($insert) ? $i++ : false;

                mysqli_query($conn, "UPDATE `wrtg_menu` SET `menu_stock`=`menu_stock`-1 WHERE `id_menu`='$dtl_id'");
            }

            $taxPercentage  = 10;
            $taxAmount      = $subTotal * $taxPercentage / 100;
            $totalAmount    = ($subTotal - $discountAmount) + $taxAmount;
            $orderPayment   = $totalAmount;
            $orderPayBack   = 0;

            if ($ord_payment == 'COD') {
                mysqli_query($conn, "UPDATE `wrtg_order` SET `order_subtotal`='$subTotal', `order_discount`='$discountAmount', `order_tax`='$taxAmount', `order_total`='$totalAmount', `order_pay`='$orderPayment',  `order_pay_back`='$orderPayBack', `order_payment_status`='Pending' WHERE `id_order`='$latest'");
            } else if ($ord_payment == 'QRIS-1') {
                mysqli_query($conn, "UPDATE `wrtg_order` SET `order_subtotal`='$subTotal', `order_discount`='$discountAmount', `order_tax`='$taxAmount', `order_total`='$totalAmount', `order_pay`='$orderPayment',  `order_pay_back`='$orderPayBack', `order_payment_status`='Pending' WHERE `id_order`='$latest'");
            } else if ($ord_payment == 'QRIS-2') {
                mysqli_query($conn, "UPDATE `wrtg_order` SET `order_subtotal`='$subTotal', `order_discount`='$discountAmount', `order_tax`='$taxAmount', `order_total`='$totalAmount', `order_pay`='$orderPayment',  `order_pay_back`='$orderPayBack', `order_payment_status`='Pending' WHERE `id_order`='$latest'");
            } else {
                mysqli_query($conn, "UPDATE `wrtg_order` SET `order_subtotal`='$subTotal', `order_discount`='$discountAmount', `order_tax`='$taxAmount', `order_total`='$totalAmount', `order_pay`='$orderPayment',  `order_pay_back`='$orderPayBack', `order_payment_status`='Pending' WHERE `id_order`='$latest'");
            }

            $response['status']             = 1;
            $response['content']            = 'Berhasil membuat pesanan dengan nomor pesanan: #' . $latest;
            $response['redirect']           = $config['host'] . '/order';
            $response['data']['id_order']   = $latest;

            $wallet_log_name        = 'Pesanan #' . $latest;
            $wallet_log_type        = 'IN';
            $wallet_log_notes       = '-';
            $wallet_log_amount_in   = $totalAmount;
            $wallet_log_amount_out  = 0;
            mysqli_query($conn, "INSERT INTO `wrtg_wallet_log` (`id_wallet`, `type`, `name`, `amount_in`, `amount_out`, `notes`, `created_at`, `updated_at`) VALUES ('901', '$wallet_log_type', '$wallet_log_name', '$wallet_log_amount_in', '$wallet_log_amount_out', '$wallet_log_notes', '$time', '$time')");
            mysqli_query($conn, "UPDATE `wrtg_wallet` SET `wl_balance`=`wl_balance`+$wallet_log_amount_in WHERE `id_wallet`='901'");

            setCart([]);
        } else {
            $response['status']     = 0;
            $response['content']    = 'Oops.. Nampaknya ada masalah :(';
            // $response['data']       = $_POST;
        }
    }
}

echo json_encode($response);
