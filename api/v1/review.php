<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../../lib/config.php');
header('Content-Type: application/json');

if ($_SESSION['cst_status'] <> 'login') {
    $response['status']     = 0;
    $response['content']    = "Silahkan masuk terlebih dahulu";
} else if (!($_GET['method'])) {
    $response['status']     = 0;
    $response['content']    = 'Method not found!';
} else if ($_GET['method'] == 'GET_REVIEW') {
    if (empty($_POST['id_order'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi pesanan";
    } else {
        $id_order       = strtoupper(mysqli_real_escape_string($conn, stripslashes($_POST['id_order'])));
        $id_customer    = mysqli_real_escape_string($conn, stripslashes($_SESSION['cst_id']));

        $query  = "SELECT * FROM `wrtg_order_review` WHERE `id_order`='$id_order' AND `id_customer`='$id_customer'";
        $query  = mysqli_query($conn, $query) or die(mysqli_error($conn));
        $review = mysqli_fetch_assoc($query);
        if (mysqli_num_rows($query) < 1) {
            $response['status']     = 0;
            $response['content']    = "Data tidak ditemukan";
            $response['data']       = $review;
        } else {
            $response['status']     = 1;
            $response['content']    = "Data review";
            $response['data']       = $review;
        }
    }
} else if ($_GET['method'] == 'ADD_REVIEW') {
    if (empty($_POST['id_order'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi pesanan";
    } else if (empty($_POST['rev_name'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Nama Lengkap";
    } else if (empty($_POST['rev_email'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Alamat Email";
    } else if (empty($_POST['rev_message'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Ulasan";
    } else if (empty($_POST['rev_rating'])) {
        $response['status']     = 0;
        $response['content']    = "Harap lengkapi Rating";
    } else {

        $id_customer    = mysqli_real_escape_string($conn, stripslashes($_SESSION['cst_id']));
        $id_order       = mysqli_real_escape_string($conn, stripslashes($_POST['id_order']));
        $rev_name       = mysqli_real_escape_string($conn, stripslashes($_POST['rev_name']));
        $rev_email      = mysqli_real_escape_string($conn, stripslashes($_POST['rev_email']));
        $rev_message    = mysqli_real_escape_string($conn, stripslashes($_POST['rev_message']));
        $rev_rating     = mysqli_real_escape_string($conn, stripslashes($_POST['rev_rating']));

        $query  = "INSERT INTO `wrtg_order_review` (`id_order`, `id_customer`, `rev_name`, `rev_email`, `rev_message`, `rev_rating`, `created_at`, `updated_at`) VALUES ('$id_order', '$id_customer', '$rev_name', '$rev_email', '$rev_message', '$rev_rating', '$time', '$time')";
        $query  = mysqli_query($conn, $query);
        if ($query) {
            $response['status']     = 1;
            $response['content']    = "Berhasil memberikan ulasan!";
        } else {
            $response['status']     = 0;
            $response['content']    = "Gagal memberikan ulasan!";
        }
    }
}

echo json_encode($response);
