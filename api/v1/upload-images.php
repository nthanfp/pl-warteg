<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
require('../../lib/config.php');

$image_name = $_FILES['image']['name'];
$image_type = $_FILES['image']['type'];
$image_size = $_FILES['image']['size'];
$image_tmp  = $_FILES['image']['tmp_name'];

$unique_image_name  = md5(uniqid(rand(), true)) . '.' . pathinfo($image_name, PATHINFO_EXTENSION);
$dir_name           = 'uploads/images/';
$target_dir         =  '../../' . $dir_name;
$target_file        = $target_dir . $unique_image_name;
$image_path         = $dir_name . $unique_image_name;

$allowed_types = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
if (in_array(exif_imagetype($image_tmp), $allowed_types)) {
    if (move_uploaded_file($image_tmp, $target_file)) {
        $now = time();
        $sql = "INSERT INTO `wrtg_images` (`image_name`, `image_path`, `created_at`, `updated_at`) VALUES ('$unique_image_name', '$image_path', '$now', '$now')";
        if (mysqli_query($conn, $sql)) {
            $response['status'] = 1;
            $response['content'] = "Gambar berhasil diupload dan data gambar berhasil disimpan ke dalam database.";
            $response['data']['file_name'] = $unique_image_name;
            $response['data']['file_path'] = $image_path;
        } else {
            $response['status'] = 0;
            $response['content'] = "Gagal menyimpan data gambar ke dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Gagal mengupload gambar.";
    }
} else {
    $response['status'] = 0;
    $response['content'] = "Tipe file tidak didukung. Hanya file gambar yang diperbolehkan.";
}

echo json_encode($response);
