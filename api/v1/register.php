<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();

if (empty($_POST['email'])) {
    $response['status']     = 0;
    $response['content']    = "Harap isi alamat email";
} else if (empty($_POST['full_name'])) {
    $response['status']     = 0;
    $response['content']    = "Harap isi nama lengkap";
} else if (empty($_POST['password'])) {
    $response['status']     = 0;
    $response['content']    = "Harap isi kata sandi";
} else if (empty($_POST['phone'])) {
    $response['status']     = 0;
    $response['content']    = "Harap isi nomor handphone";
} else if (empty($_POST['conf_password'])) {
    $response['status']     = 0;
    $response['content']    = "Harap isi konfirmasi kata sandi";
} else {
    $cst_full_name  = mysqli_real_escape_string($conn, stripslashes($_POST['full_name']));
    $cst_email      = mysqli_real_escape_string($conn, stripslashes($_POST['email']));
    $cst_phone      = mysqli_real_escape_string($conn, stripslashes($_POST['phone']));
    $cst_password   = mysqli_real_escape_string($conn, stripslashes($_POST['password']));
    $cst_password   = md5($cst_password);

    // buat tanggal dan waktu saat ini
    $cst_register_date  = time();
    $cst_last_login     = time();
    $cst_ip             = $_SERVER['REMOTE_ADDR'];

    // query untuk menambahkan data ke tabel wrtg_customer
    $sql = "INSERT INTO `wrtg_customer` (`cst_full_name`, `cst_email`, `cst_password`, `cst_phone`, `cst_register_date`, `cst_last_login`, `cst_ip`) VALUES ('$cst_full_name', '$cst_email', '$cst_password', '$cst_phone', '$cst_register_date', '$cst_last_login', '$cst_ip')";

    // jalankan query
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Berhasil mendaftarkan akun";
    } else {
        $response['status']     = 0;
        $response['content']    = "Akun gagal didaftarkan";
    }
}

echo json_encode($response);
