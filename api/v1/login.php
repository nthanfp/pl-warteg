<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if (empty($_POST['email']) || empty($_POST['password'])) {
    $response['status'] = 0;
    $response['content'] = "Inputan tidak lengkap";
}

$email          = mysqli_real_escape_string($conn, $_POST['email']);
$password       = mysqli_real_escape_string($conn, $_POST['password']);
$password_hash  = md5($password);

$query  = "SELECT * FROM `wrtg_customer` WHERE `cst_email` = '$email' AND `cst_password` = '$password_hash'";
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));

if (mysqli_num_rows($result) > 0) {
    $customer = mysqli_fetch_assoc($result);

    $response['status']     = 1;
    $response['content']    = "Berhasil login...";
    $response['data']       = $customer;

    // Set session
    $cst_id                     = $customer['id_customer'];
    $_SESSION['cst_id']         = $customer['id_customer'];
    $_SESSION['cst_name']       = $customer['cst_full_name'];
    $_SESSION['cst_email']      = $customer['cst_email'];
    $_SESSION['cst_status']     = 'login';

    // Query update last login
    mysqli_query($conn, "UPDATE `wrtg_customer` SET `cst_last_login`='$time' WHERE  `id_customer`='$cst_id'");
} else {
    $response['status'] = 0;
    $response['content'] = "Email/Telepon dan password tidak cocok!";
}

print_r(json_encode($response));
