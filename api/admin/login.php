<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if (empty($_POST['email_or_phone']) || empty($_POST['password'])) {
    $response['status'] = 0;
    $response['content'] = "Inputan tidak lengkap";
}

$email_or_phone = mysqli_real_escape_string($conn, $_POST['email_or_phone']);
$password       = mysqli_real_escape_string($conn, $_POST['password']);
$password_hash  = md5($password);

$query  = "SELECT * FROM `wrtg_employee` WHERE (`emp_email` = '$email_or_phone' OR `emp_phone` = '$email_or_phone') AND `emp_password` = '$password_hash'";
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));

if (mysqli_num_rows($result) > 0) {
    $employee       = mysqli_fetch_assoc($result);
    $employee_job   = $employee['emp_job_id'];
    $job_query      = mysqli_query($conn, "SELECT * FROM `wrtg_job` WHERE `id_job`='$employee_job'");
    $job_data       = mysqli_fetch_assoc($job_query);

    $response['status']     = 1;
    $response['content']    = "Berhasil login... <script>window.location = '" . $config['host'] . "/admin-page';</script>";
    $response['data']       = $employee;

    // Set session
    $emp_id                     = $employee['id_employee'];
    $_SESSION['emp_id']         = $employee['id_employee'];
    $_SESSION['emp_name']       = $employee['emp_full_name'];
    $_SESSION['emp_email']      = $employee['emp_email'];
    $_SESSION['emp_job_id']     = $employee['emp_job_id'];
    $_SESSION['emp_job_name']   = strtoupper($job_data['job_name']);
    $_SESSION['emp_status']     = 'login';

    // Query update last login
    mysqli_query($conn, "UPDATE `acpnmodm_warteg`.`wrtg_employee` SET `emp_last_login`='$time' WHERE  `id_employee`='$emp_id'");
} else {
    $response['status'] = 0;
    $response['content'] = "Email/Telepon dan password tidak cocok!";
}

print_r(json_encode($response));
