<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else {
    $report_type    = mysqli_real_escape_string($conn, stripslashes($_POST['report_type']));
    $report_format  = mysqli_real_escape_string($conn, stripslashes($_POST['report_format']));
    $range_start    = mysqli_real_escape_string($conn, stripslashes($_POST['range_start']));
    $range_end      = mysqli_real_escape_string($conn, stripslashes($_POST['range_end']));

    switch ($report_type) {
        case '1':
            // 1: Laporan per Transaksi
            switch ($report_format) {
                case 'D': // Format Detail
                    $data = http_build_query(array(
                        'report_type' => $report_type,
                        'report_format' => $report_format,
                        'range_start' => $range_start,
                        'range_end' => $range_end
                    ));

                    $response['status']     = 1;
                    $response['content']    = "Data laporan";
                    $response['data']       = array('report_url' => $config['host_admin'] . '/report/1?' . $data);

                    break;
                case 'R': // Format Rekap
                    $data = http_build_query(array(
                        'report_type' => $report_type,
                        'report_format' => $report_format,
                        'range_start' => $range_start,
                        'range_end' => $range_end
                    ));

                    $response['status']     = 1;
                    $response['content']    = "Data laporan";
                    $response['data']       = array('report_url' => $config['host_admin'] . '/report/1?' . $data);

                    break;
                default:
                    $response['status']     = 0;
                    $response['content']    = "Format laporan belum dipilih";
            }
            break;
        case '2':
            // 1: Laporan per Transaksi per Pelanggan
            switch ($report_format) {
                case 'D': // Format Detail
                    $data = http_build_query(array(
                        'report_type' => $report_type,
                        'report_format' => $report_format,
                        'range_start' => $range_start,
                        'range_end' => $range_end
                    ));

                    $response['status']     = 1;
                    $response['content']    = "Data laporan";
                    $response['data']       = array('report_url' => $config['host_admin'] . '/report/2?' . $data);

                    break;
                case 'R': // Format Rekap
                    $data = http_build_query(array(
                        'report_type' => $report_type,
                        'report_format' => $report_format,
                        'range_start' => $range_start,
                        'range_end' => $range_end
                    ));

                    $response['status']     = 1;
                    $response['content']    = "Data laporan";
                    $response['data']       = array('report_url' => $config['host_admin'] . '/report/2?' . $data);

                    break;
                default:
                    $response['status']     = 0;
                    $response['content']    = "Format laporan belum dipilih";
            }
            break;
        case '3':
            // 1: Laporan per Transaksi per Menu
            switch ($report_format) {
                case 'D': // Format Detail
                    $data = http_build_query(array(
                        'report_type' => $report_type,
                        'report_format' => $report_format,
                        'range_start' => $range_start,
                        'range_end' => $range_end
                    ));

                    $response['status']     = 1;
                    $response['content']    = "Data laporan";
                    $response['data']       = array('report_url' => $config['host_admin'] . '/report/3?' . $data);

                    break;
                case 'R': // Format Rekap
                    $data = http_build_query(array(
                        'report_type' => $report_type,
                        'report_format' => $report_format,
                        'range_start' => $range_start,
                        'range_end' => $range_end
                    ));

                    $response['status']     = 1;
                    $response['content']    = "Data laporan";
                    $response['data']       = array('report_url' => $config['host_admin'] . '/report/3?' . $data);

                    break;
                default:
                    $response['status']     = 0;
                    $response['content']    = "Format laporan belum dipilih";
            }
            break;
        default:
            $response['status']     = 0;
            $response['content']    = "Jenis laporan belum dipilih";
    }

    print json_encode($response);
}
