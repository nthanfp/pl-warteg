<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $table          = 'wrtg_customer';
    $primaryKey     = 'id_customer';
    $columns        = array(
        array(
            'db' => 'id_customer',
            'dt' => 0
        ),
        array(
            'db' => 'cst_email',
            'dt' => 1
        ),
        array(
            'db' => 'cst_phone',
            'dt' => 2
        ),
        array(
            'db' => 'cst_full_name',
            'dt' => 3
        ),
        array(
            'db' => 'cst_full_address',
            'dt' => 4
        ),
        array(
            'db' => 'cst_register_date',
            'dt' => 5,
            'formatter' => function ($d, $row) {
                return date('d M Y H:i:s', $d);
            }
        ),
        array(
            'db' => 'cst_last_login',
            'dt' => 6,
            'formatter' => function ($d, $row) {
                return date('d M Y H:i:s', $d);
            }
        ),
        array(
            'db' => 'cst_ip',
            'dt' => 7
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
} else if ($_GET['method'] == "CREATE") {
    if (empty($_POST['cst_full_name'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Nama Lengkap harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_email'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Email harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_password'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Password harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_phone'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Telepon harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_full_address'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Alamat Lengkap harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_addr_state'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Provinsi harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_addr_district'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Kabupaten/Kota harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_addr_village'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Kecamatan harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_pos_code'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Kode Pos harus diisi";
        print json_encode($response);
        exit;
    } else {
        $cst_full_name      = mysqli_real_escape_string($conn, stripslashes($_POST['cst_full_name']));
        $cst_email          = mysqli_real_escape_string($conn, stripslashes($_POST['cst_email']));
        $cst_password       = mysqli_real_escape_string($conn, stripslashes($_POST['cst_password']));
        $cst_phone          = mysqli_real_escape_string($conn, stripslashes($_POST['cst_phone']));
        $cst_full_address   = mysqli_real_escape_string($conn, stripslashes($_POST['cst_full_address']));
        $cst_addr_state     = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_state']));
        $cst_addr_district  = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_district']));
        $cst_addr_village   = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_village']));
        $cst_pos_code       = mysqli_real_escape_string($conn, stripslashes($_POST['cst_pos_code']));

        $sql = "INSERT INTO wrtg_customer (cst_full_name, cst_email, cst_password, cst_phone, cst_full_address, cst_addr_state, cst_addr_district, cst_addr_village, cst_pos_code) VALUES ('$cst_full_name', '$cst_email', '$cst_password', '$cst_phone', '$cst_full_address', '$cst_addr_state', '$cst_addr_district', '$cst_addr_village', '$cst_pos_code')";

        if (mysqli_query($conn, $sql)) {
            $response['status']     = 1;
            $response['content']    = "Data customer berhasil ditambahkan";
        } else {
            $response['status']     = 0;
            $response['content']    = "Gagal menambahkan data customer: " . mysqli_error($conn);
        }

        print json_encode($response);
        exit;
    }
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_customer']) && !empty($_POST['id_customer'])) {
        $id_customer = mysqli_real_escape_string($conn, stripslashes($_POST['id_customer']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID pelanggan tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_customer` WHERE `id_customer`='$id_customer'";
    if ($query = mysqli_query($conn, $sql)) {
        $customer = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data customer";
        $response['data']       = $customer;
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data customer: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'UPDATE') {
    if (empty($_POST['cst_full_name'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Nama Lengkap harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_email'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Email harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_phone'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Telepon harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_full_address'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Alamat Lengkap harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_addr_state'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Provinsi harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_addr_district'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Kabupaten/Kota harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_addr_village'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Kecamatan harus diisi";
        print json_encode($response);
        exit;
    } else if (empty($_POST['cst_pos_code'])) {
        $response['status']     = 0;
        $response['content']    = "Kolom Kode Pos harus diisi";
        print json_encode($response);
        exit;
    } else {
        $id_customer            = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $cst_full_name          = mysqli_real_escape_string($conn, stripslashes($_POST['cst_full_name']));
        $cst_email              = mysqli_real_escape_string($conn, stripslashes($_POST['cst_email']));
        $cst_phone              = mysqli_real_escape_string($conn, stripslashes($_POST['cst_phone']));
        $cst_full_address       = mysqli_real_escape_string($conn, stripslashes($_POST['cst_full_address']));
        $cst_addr_state         = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_state']));
        $cst_addr_district      = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_district']));
        $cst_addr_village       = mysqli_real_escape_string($conn, stripslashes($_POST['cst_addr_village']));
        $cst_pos_code           = mysqli_real_escape_string($conn, stripslashes($_POST['cst_pos_code']));

        //mengecek apakah data berhasil diupdate
        $sql = "UPDATE `wrtg_customer` SET `cst_full_name`='$cst_full_name', `cst_email`='$cst_email', `cst_phone`='$cst_phone', `cst_full_address`='$cst_full_address', `cst_addr_state`='$cst_addr_state', `cst_addr_district`='$cst_addr_district', `cst_addr_village`='$cst_addr_village', `cst_pos_code`='$cst_pos_code' WHERE `id_customer`='$id_customer'";

        if (mysqli_query($conn, $sql)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil diupdate";
        } else {
            $response['status'] = 0;
            $response['content'] = "Data gagal diupdate: " . mysqli_error($conn);
        }

        echo json_encode($response);
    }
} else if ($_GET['method'] == "DELETE") {
    if (isset($_POST['id_customer']) && !empty($_POST['id_customer'])) {
        $id_customer = mysqli_real_escape_string($conn, stripslashes($_POST['id_customer']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID pelanggan tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "DELETE FROM `wrtg_customer` WHERE `id_customer`='$id_customer'";
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Data customer berhasil dihapus";
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal menghapus data customer: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
}
