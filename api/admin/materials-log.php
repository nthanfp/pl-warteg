<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST_IN") {
    $table          = 'wrtg_materials_log';
    $joinQuery      = "FROM `{$table}` AS `l` INNER JOIN `wrtg_materials` AS `m` ON (`l`.`id_material` = `m`.`id_material`)";
    $where          = "log_type='IN'";
    $primaryKey     = 'id_log';
    $columns        = array(
        array(
            'db' => 'id_log',
            'dt' => 0,
            'field' => 'id_log'
        ),
        array(
            'db' => 'l.updated_at',
            'dt' => 1,
            'field' => 'updated_at',
            'formatter' => function ($d, $row) {
                return date('d M Y H:i:s', $d);
            }
        ),
        array(
            'db' => 'material_name',
            'dt' => 2,
            'field' => 'material_name'
        ),
        array(
            'db' => 'material_unit',
            'dt' => 3,
            'field' => 'material_unit',
        ),
        array(
            'db' => 'log_qty',
            'dt' => 4,
            'field' => 'log_qty',
            'formatter' => function ($d, $row) {
                return $d . ' ' . $row[3];
            }
        ),
        array(
            'db' => 'log_price',
            'dt' => 5,
            'field' => 'log_price',
            'formatter' => function ($d, $row) {
                return rupiah($d);
            }
        ),
        array(
            'db' => 'log_notes',
            'dt' => 6,
            'field' => 'log_notes',
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where));
} else if ($_GET['method'] == "READ_LIST_OUT") {
    $table          = 'wrtg_materials_log';
    $joinQuery      = "FROM `{$table}` AS `l` INNER JOIN `wrtg_materials` AS `m` ON (`l`.`id_material` = `m`.`id_material`)";
    $where          = "log_type='OUT'";
    $primaryKey     = 'id_log';
    $columns        = array(
        array(
            'db' => 'id_log',
            'dt' => 0,
            'field' => 'id_log'
        ),
        array(
            'db' => 'l.updated_at',
            'dt' => 1,
            'field' => 'updated_at',
            'formatter' => function ($d, $row) {
                return date('d M Y H:i:s', $d);
            }
        ),
        array(
            'db' => 'material_name',
            'dt' => 2,
            'field' => 'material_name'
        ),
        array(
            'db' => 'material_unit',
            'dt' => 3,
            'field' => 'material_unit',
        ),
        array(
            'db' => 'log_qty',
            'dt' => 4,
            'field' => 'log_qty',
            'formatter' => function ($d, $row) {
                return $d . ' ' . $row[3];
            }
        ),
        array(
            'db' => 'log_notes',
            'dt' => 5,
            'field' => 'log_notes',
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where));
} else if ($_GET['method'] == "CREATE_IN") {
    if (isset($_POST['id_material']) && isset($_POST['log_qty']) && isset($_POST['log_price'])) {
        $id_material    = mysqli_real_escape_string($conn, stripslashes($_POST['id_material']));
        $log_qty        = mysqli_real_escape_string($conn, stripslashes($_POST['log_qty']));
        $log_price      = mysqli_real_escape_string($conn, stripslashes($_POST['log_price']));
        $log_price      = filter_var(str_replace(".", "", $log_price), FILTER_SANITIZE_NUMBER_INT);
        $log_notes      = mysqli_real_escape_string($conn, stripslashes($_POST['log_notes']));

        $query = "INSERT INTO `wrtg_materials_log` (`id_material`, `log_type`, `log_qty`, `log_price`, `log_notes`, `created_at`, `updated_at`) VALUES ('$id_material', 'IN', '$log_qty', '$log_price', '$log_notes', '$time', '$time')";
        if (mysqli_query($conn, $query)) {
            $getLast    = "SELECT * FROM `wrtg_materials_log` WHERE `created_at`='$time' AND `id_material`='$id_material'";
            $getLast    = mysqli_query($conn, $getLast);
            $latest     = mysqli_fetch_assoc($getLast)['id_log'];

            $wallet_log_name        = 'Pembelian Bahan Baku #' . $latest;
            $wallet_log_type        = 'OUT';
            $wallet_log_notes       = '-';
            $wallet_log_amount_in   = 0;
            $wallet_log_amount_out  = $log_price;

            mysqli_query($conn, "INSERT INTO `wrtg_wallet_log` (`id_wallet`, `type`, `name`, `amount_in`, `amount_out`, `notes`, `created_at`, `updated_at`) VALUES ('901', '$wallet_log_type', '$wallet_log_name', '$wallet_log_amount_in', '$wallet_log_amount_out', '$wallet_log_notes', '$time', '$time')");
            mysqli_query($conn, "UPDATE `wrtg_wallet` SET `wl_balance`=`wl_balance`-$wallet_log_amount_out WHERE `id_wallet`='901'");
            mysqli_query($conn, "UPDATE `wrtg_materials` SET `material_stock`=`material_stock`+'$log_qty', `updated_at`='$time' WHERE `id_material`='$id_material'") or die(mysqli_error($conn));

            $response['status'] = 1;
            $response['content'] = "Data berhasil ditambahkan ke dalam database";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat menambahkan data ke dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "CREATE_OUT") {
    if (isset($_POST['id_material']) && isset($_POST['log_qty'])) {
        $id_material    = mysqli_real_escape_string($conn, stripslashes($_POST['id_material']));
        $log_qty        = mysqli_real_escape_string($conn, stripslashes($_POST['log_qty']));
        $log_notes      = mysqli_real_escape_string($conn, stripslashes($_POST['log_notes']));

        $query = "INSERT INTO `wrtg_materials_log` (`id_material`, `log_type`, `log_qty`, `log_notes`, `created_at`, `updated_at`) VALUES ('$id_material', 'OUT', '$log_qty', '$log_notes', '$time', '$time')";
        if (mysqli_query($conn, $query)) {
            mysqli_query($conn, "UPDATE `wrtg_materials` SET `material_stock`=`material_stock`-'$log_qty', `updated_at`='$time' WHERE `id_material`='$id_material'") or die(mysqli_error($conn));
            $response['status'] = 1;
            $response['content'] = "Data berhasil ditambahkan ke dalam database";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat menambahkan data ke dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_material']) && !empty($_POST['id_material'])) {
        $id_material = mysqli_real_escape_string($conn, stripslashes($_POST['id_material']));
    } else {
        $response['status'] = 0;
        $response['content'] = "ID material tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_materials` WHERE `id_material`='$id_material'";
    if ($query = mysqli_query($conn, $sql)) {
        $material               = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data material";
        $response['data']       = $material;
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data material: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_CHILD") {
    if (isset($_POST['id_variant_group']) && !empty($_POST['id_variant_group'])) {
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID varian group tidak boleh kosong";

        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_variant` WHERE `id_variant_group`='$id_variant_group'";
    $query = mysqli_query($conn, $sql);
    if ($query) {
        $response['status']     = 1;
        $response['content']    = "Data varian child";
        $response['data']       = array();
        while ($row = mysqli_fetch_assoc($query)) {
            array_push($response['data'], $row);
        }
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data child varian group: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'UPDATE') {
    if (isset($_POST['material_name']) && isset($_POST['material_unit']) && isset($_POST['material_stock'])) {
        $id_material = $_POST['id_data'];
        $material_name = mysqli_real_escape_string($conn, stripslashes($_POST['material_name']));
        $material_unit = mysqli_real_escape_string($conn, stripslashes($_POST['material_unit']));
        $material_stock = mysqli_real_escape_string($conn, stripslashes($_POST['material_stock']));
        $time = time();

        $query = "UPDATE `wrtg_materials` SET `material_name`='$material_name', `material_unit`='$material_unit', `material_stock`='$material_stock', `updated_at`='$time' WHERE `id_material`='$id_material'";

        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil diubah";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat mengubah data di dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "DELETE_IN") {
    if (isset($_POST['id_log']) && !empty($_POST['id_log'])) {
        $id_log = mysqli_real_escape_string($conn, stripslashes($_POST['id_log']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID log tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "DELETE FROM `wrtg_materials_log` WHERE `id_log`='$id_log'";
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Data material berhasil dihapus";
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal menghapus data material: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
}
