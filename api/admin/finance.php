<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

function getTotalIn($wallet, $range, $range_str = 0, $range_end = 0)
{
    global $conn;

    switch ($range) {
        case 'ALL':
            $total_in = mysqli_query($conn, "SELECT SUM(amount_in) AS `total_in` FROM `wrtg_wallet_log` WHERE `id_wallet`='$wallet'");
            $total_in = mysqli_fetch_assoc($total_in)['total_in'];
            break;
        case 'CUSTOM':
            $total_in   = mysqli_query($conn, "SELECT SUM(`amount_in`) AS `total_in` FROM `wrtg_wallet_log` WHERE FROM_UNIXTIME(`created_at`, '%Y-%m-%d') BETWEEN '$range_str' AND '$range_end' AND `id_wallet`='$wallet'");
            $total_in = mysqli_fetch_assoc($total_in)['total_in'];
            break;
        default:
            $total_in = mysqli_query($conn, "SELECT SUM(amount_in) AS `total_in` FROM `wrtg_wallet_log` WHERE `id_wallet`='$wallet'");
            $total_in = mysqli_fetch_assoc($total_in)['total_in'];
            break;
    }

    return $total_in;
}

function getTotalOut($wallet, $range, $range_str = 0, $range_end = 0)
{
    global $conn;

    switch ($range) {
        case 'ALL':
            $total_out  = mysqli_query($conn, "SELECT SUM(amount_out) AS `total_out` FROM `wrtg_wallet_log` WHERE `id_wallet`='901'");
            $total_out  = mysqli_fetch_assoc($total_out)['total_out'];
            break;
        case 'CUSTOM':
            $total_out  = mysqli_query($conn, "SELECT SUM(`amount_out`) AS `total_out` FROM `wrtg_wallet_log` WHERE FROM_UNIXTIME(`created_at`, '%Y-%m-%d') BETWEEN '$range_str' AND '$range_end' AND `id_wallet`='$wallet'");
            $total_out  = mysqli_fetch_assoc($total_out)['total_out'];
            break;
        default:
            $total_out  = mysqli_query($conn, "SELECT SUM(amount_out) AS `total_out` FROM `wrtg_wallet_log` WHERE `id_wallet`='901'");
            $total_out  = mysqli_fetch_assoc($total_out)['total_out'];
            break;
    }

    return $total_out;
}

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $range_str = $_GET['range_str'];
    $range_end = $_GET['range_end'];

    $table          = 'wrtg_wallet_log';
    $joinQuery      = "FROM `{$table}` AS `l` INNER JOIN `wrtg_wallet` AS `w` ON (`l`.`id_wallet` = `w`.`id_wallet`)";
    $primaryKey     = 'id_log';

    if ($range_str == 'undefined' || $range_end == 'undefined') {
        $where = "";
    } else {
        $where = "FROM_UNIXTIME(`l`.`created_at`, '%Y-%m-%d') BETWEEN '$range_str' AND '$range_end'";
    }

    $columns = array(
        array(
            'db' => 'id_log',
            'dt' => 0,
            'field' => 'id_log'
        ),
        array(
            'db' => 'l.updated_at',
            'dt' => 1,
            'field' => 'updated_at',
            'formatter' => function ($d, $row) {
                return date('d M Y H:i:s', $d);
            }
        ),
        array(
            'db' => 'amount_in',
            'dt' => 2,
            'field' => 'amount_in',
            'formatter' => function ($d, $row) {
                return rupiah($d);
            }
        ),
        array(
            'db' => 'amount_out',
            'dt' => 3,
            'field' => 'amount_out',
            'formatter' => function ($d, $row) {
                return rupiah($d);
            }
        ),
        array(
            'db' => 'amount_out',
            'dt' => 4,
            'field' => 'amount_out',
            'formatter' => function ($d, $row) {
                return rupiah(0);
            }
        ),
        array(
            'db' => 'name',
            'dt' => 5,
            'field' => 'name',
        ),
        array(
            'db' => 'notes',
            'dt' => 6,
            'field' => 'notes',
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where));
} else if ($_GET['method'] == "READ_LIST_OUT") {
    $table          = 'wrtg_wallet_log';
    $joinQuery      = "FROM `{$table}` AS `l` INNER JOIN `wrtg_materials` AS `m` ON (`l`.`id_material` = `m`.`id_material`)";
    $where          = "log_type='OUT'";
    $primaryKey     = 'id_log';
    $columns        = array(
        array(
            'db' => 'id_log',
            'dt' => 0,
            'field' => 'id_log'
        ),
        array(
            'db' => 'l.updated_at',
            'dt' => 1,
            'field' => 'updated_at',
            'formatter' => function ($d, $row) {
                return date('d M Y H:i:s', $d);
            }
        ),
        array(
            'db' => 'material_name',
            'dt' => 2,
            'field' => 'material_name'
        ),
        array(
            'db' => 'material_unit',
            'dt' => 3,
            'field' => 'material_unit',
        ),
        array(
            'db' => 'log_qty',
            'dt' => 4,
            'field' => 'log_qty',
            'formatter' => function ($d, $row) {
                return $d . ' ' . $row[3];
            }
        ),
        array(
            'db' => 'log_notes',
            'dt' => 5,
            'field' => 'log_notes',
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $where));

    if (isset($_POST['id_material']) && isset($_POST['log_qty'])) {
        $id_material    = mysqli_real_escape_string($conn, stripslashes($_POST['id_material']));
        $log_qty        = mysqli_real_escape_string($conn, stripslashes($_POST['log_qty']));
        $log_notes      = mysqli_real_escape_string($conn, stripslashes($_POST['log_notes']));

        $query = "INSERT INTO `wrtg_materials_log` (`id_material`, `log_type`, `log_qty`, `log_notes`, `created_at`, `updated_at`) VALUES ('$id_material', 'OUT', '$log_qty', '$log_notes', '$time', '$time')";
        if (mysqli_query($conn, $query)) {
            mysqli_query($conn, "UPDATE `wrtg_materials` SET `material_stock`=`material_stock`-'$log_qty' WHERE `id_material`='$id_material'") or die(mysqli_error($conn));
            $response['status'] = 1;
            $response['content'] = "Data berhasil ditambahkan ke dalam database";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat menambahkan data ke dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "CALCULATE_FINANCE") {
    $range_str = mysqli_real_escape_string($conn, stripslashes($_POST['range_str']));
    $range_end = mysqli_real_escape_string($conn, stripslashes($_POST['range_end']));
    if ((empty($_POST['range_str'])) || (empty($_POST['range_end']))) {
        $response['status']     = 1;
        $response['content']    = 'Data keuangan';
        $response['data']       = array(
            'total_in' => getTotalIn(901, 'ALL'),
            'total_out' => getTotalOut(901, 'ALL'),
            'total_in_rp' => rupiah(getTotalIn(901, 'ALL')),
            'total_out_rp' => rupiah(getTotalOut(901, 'ALL'))
        );
    } else {
        $response['status']     = 1;
        $response['content']    = 'Data keuangan';
        $response['data']       = array(
            'total_in' => getTotalIn(901, 'CUSTOM', $range_str, $range_end),
            'total_out' => getTotalOut(901, 'CUSTOM', $range_str, $range_end),
            'total_in_rp' => rupiah(getTotalIn(901, 'CUSTOM', $range_str, $range_end)),
            'total_out_rp' => rupiah(getTotalOut(901, 'CUSTOM', $range_str, $range_end))
        );
    }

    print json_encode($response);
}
