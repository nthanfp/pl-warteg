<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $table          = 'wrtg_order';
    $joinQuery      = null;
    $primaryKey     = 'id_order';
    $columns        = array(
        array(
            'db' => 'id_order',
            'dt' => 0,
        ),
        array(
            'db' => 'order_time',
            'dt' => 1,
            'formatter' => function ($d, $row) {
                return date('d M Y H:i:s', $d);
            }
        ),
        array(
            'db' => 'id_customer',
            'dt' => 2,
            'formatter' => function ($d, $row) {
                global $conn;
                $query  = mysqli_query($conn, "SELECT * FROM `wrtg_customer` WHERE `id_customer`='$d'");
                $data   = mysqli_fetch_assoc($query);

                if ($data['id_customer'] == 102) {
                    return ('-- KASIR WRTG --');
                } else {
                    return ('(' . $data['cst_phone'] . ') ' . $data['cst_full_name']);
                }
            }
        ),
        array(
            'db' => 'order_name',
            'dt' => 3,
        ),
        array(
            'db' => 'order_total',
            'dt' => 4,
            'formatter' => function ($d, $row) {
                return rupiah($d);
            }
        ),
        array(
            'db' => 'order_type',
            'dt' => 5,
            'formatter' => function ($d, $row) {
                $d = strtoupper($d);
                if ($d == 'TAKEAWAY') {
                    $ret = ('<span class="badge badge-info">' . strtoupper(strtolower($d)) . '</span>');
                } else if ($d == 'DELIVERY') {
                    $ret = ('<span class="badge badge-warning">' . strtoupper(strtolower($d)) . '</span>');
                } else if ($d == 'DINEIN') {
                    $ret = ('<span class="badge badge-primary">' . strtoupper(strtolower($d)) . '</span>');
                }

                return $ret;
            }
        ),
        array(
            'db' => 'order_status',
            'dt' => 6,
            'formatter' => function ($d, $row) {
                $d = ucwords($d);
                if ($d == 'Pending') {
                    $ret = ('<span class="badge badge-warning badge-lg">' . strtoupper(strtolower($d)) . '</span>');
                } else if ($d == 'Process') {
                    $ret = ('<span class="badge badge-primary">' . strtoupper(strtolower($d)) . '</span>');
                } else if ($d == 'Completed') {
                    $ret = ('<span class="badge badge-success">' . strtoupper(strtolower($d)) . '</span>');
                } else if ($d == 'Canceled') {
                    $ret = ('<span class="badge badge-danger">' . strtoupper(strtolower($d)) . '</span>');
                }

                return $ret;
            }
        ),
        array(
            'db' => 'order_payment_status',
            'dt' => 7,
            'formatter' => function ($d, $row) {
                $d = ucwords($d);
                if ($d == 'Pending') {
                    $ret = ('<span class="badge badge-warning badge-lg">' . strtoupper(strtolower($d)) . '</span>');
                } else if ($d == 'Verification') {
                    $ret = ('<span class="badge badge-primary">' . strtoupper(strtolower($d)) . '</span>');
                } else if ($d == 'Confirmed') {
                    $ret = ('<span class="badge badge-success">' . strtoupper(strtolower($d)) . '</span>');
                } else if ($d == 'Canceled') {
                    $ret = ('<span class="badge badge-danger">' . strtoupper(strtolower($d)) . '</span>');
                }

                return $ret;
            }
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery));
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_order']) && !empty($_POST['id_order'])) {
        $id_order = mysqli_real_escape_string($conn, stripslashes($_POST['id_order']));
    } else {
        $response['status'] = 0;
        $response['content'] = "ID order tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT *
    FROM `wrtg_order` `o`
    INNER JOIN `wrtg_customer` `c`
        ON (`o`.`id_customer`=`c`.`id_customer`)
    WHERE `id_order`='$id_order'";
    if ($query = mysqli_query($conn, $sql)) {
        $order                  = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data order";
        $response['data']       = $order;

        $response['data']['order_account'] = '(' . $order['cst_phone'] . ') ' . $order['cst_full_name'];

        $d = strtoupper($response['data']['order_type']);
        if ($d == 'TAKEAWAY') {
            $response['data']['order_type'] = ('<span class="badge badge-primary">' . strtoupper(strtolower($d)) . '</span>');
        } else if ($d == 'DELIVERY') {
            $response['data']['order_type'] = ('<span class="badge badge-primary">' . strtoupper(strtolower($d)) . '</span>');
        } else if ($d == 'DINEIN') {
            $response['data']['order_type'] = ('<span class="badge badge-primary">' . strtoupper(strtolower($d)) . '</span>');
        }

        $d = ucwords($response['data']['order_status']);
        if ($d == 'Pending') {
            $response['data']['order_status'] = ('<span class="badge badge-warning badge-lg">' . strtoupper(strtolower($d)) . '</span>');
        } else if ($d == 'Process') {
            $response['data']['order_status'] = ('<span class="badge badge-primary">' . strtoupper(strtolower($d)) . '</span>');
        } else if ($d == 'Completed') {
            $response['data']['order_status'] = ('<span class="badge badge-success">' . strtoupper(strtolower($d)) . '</span>');
        } else if ($d == 'Canceled') {
            $response['data']['order_status'] = ('<span class="badge badge-danger">' . strtoupper(strtolower($d)) . '</span>');
        }
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data order: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_CHILD") {
    if (isset($_POST['id_order']) && !empty($_POST['id_order'])) {
        $id_order = mysqli_real_escape_string($conn, stripslashes($_POST['id_order']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID pesanan tidak boleh kosong";

        print json_encode($response);
        exit;
    }

    $sql = "SELECT *
    FROM `wrtg_order_detail` `d`
    INNER JOIN `wrtg_menu` `m`
        ON (`d`.`id_menu`=`m`.`id_menu`)
    WHERE `id_order`='$id_order'";
    $query = mysqli_query($conn, $sql);
    if ($query) {
        $response['status']     = 1;
        $response['content']    = "Data detail order";
        $response['data']       = array();
        while ($row = mysqli_fetch_assoc($query)) {
            array_push($response['data'], $row);
        }
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data order detail group: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "UPDATE_STATUS") {
    if (isset($_POST['id_order']) && isset($_POST['status'])) {
        $id_order = mysqli_real_escape_string($conn, stripslashes($_POST['id_order']));
        $status = mysqli_real_escape_string($conn, stripslashes($_POST['status']));
        $time = time();

        $query = "UPDATE `wrtg_order` SET `order_status`='$status' WHERE `id_order`='$id_order'";

        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil diubah";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat mengubah data di dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "UPDATE_PAYMENT") {
    if (isset($_POST['id_order']) && isset($_POST['payment_status'])) {
        $id_order = mysqli_real_escape_string($conn, stripslashes($_POST['id_order']));
        $payment_status = mysqli_real_escape_string($conn, stripslashes($_POST['payment_status']));
        $time = time();

        $query = "UPDATE `wrtg_order` SET `order_payment_status`='$payment_status' WHERE `id_order`='$id_order'";

        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil diubah";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat mengubah data di dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
}
