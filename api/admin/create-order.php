<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

function getCart()
{
    return $_SESSION['cart'];
}

function setCart($data)
{
    $_SESSION['cart'] = $data;
    return true;
}

function addMenuToCart($id_menu, $qty = 1)
{
    global $conn;

    if ($id_menu == null) {
        return array(0, 'ID Produk/Menu tidak ada');
    }

    $query  = "SELECT * FROM `wrtg_menu` WHERE `id_menu`='$id_menu'";
    $menu   = mysqli_query($conn, $query);
    if (mysqli_num_rows($menu) < 1) {
        return array(0, 'Produk/Menu tidak ditemukan');
    }

    $menu = mysqli_fetch_assoc($menu);
    $cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : array();

    $menu_exists = false;
    foreach ($cart as $index => $item) {
        if ($item['id_menu'] == $id_menu) {
            $cart[$index]['qty'] += $qty;
            $menu_exists = true;
            break;
        }
    }

    if (!$menu_exists) {
        $item = array(
            'id_menu'       => $id_menu,
            'menu_name'     => $menu['menu_name'],
            'menu_price'    => $menu['menu_price'],
            'qty'           => $qty,
        );

        $cart[] = $item;
    }

    setCart($cart);

    return array(1, 'Menu berhasil ditambahkan ke keranjang');
}

function removeMenuFromCart($id_menu)
{
    $cart = getCart();
    for ($i = 0; $i < count($cart); $i++) {
        if ($cart[$i]['id_menu'] == $id_menu) {
            unset($cart[$i]);
        }
    }
    sort($cart);
    setCart($cart);
}

$response = array();

if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';
} else if ($_GET['method'] == 'CREATE_ORDER') {
    if (empty($_POST['ord_name'])) {
        $response['status']    = 0;
        $response['content']   = 'Harap isi atas nama pesanan';
    } else if (empty($_POST['ord_status'])) {
        $response['status']    = 0;
        $response['content']   = 'Harap isi status pesanan';
    } else if (empty($_POST['ord_type'])) {
        $response['status']    = 0;
        $response['content']   = 'Harap isi tipe pesanan';
    } else if (count(getCart()) < 1) {
        $response['status']    = 0;
        $response['content']   = 'Keranjang kosong!';
    } else {
        $ord_customer   = mysqli_real_escape_string($conn, stripslashes($_POST['ord_customer']));
        $ord_name       = mysqli_real_escape_string($conn, stripslashes($_POST['ord_name']));
        $ord_status     = mysqli_real_escape_string($conn, stripslashes($_POST['ord_status']));
        $ord_type       = mysqli_real_escape_string($conn, stripslashes($_POST['ord_type']));
        $ord_pay        = mysqli_real_escape_string($conn, stripslashes($_POST['ord_pay']));
        $ord_pay        = filter_var(str_replace(".", "", $ord_pay), FILTER_SANITIZE_NUMBER_INT);
        $ord_pay_code   = 'COD';
        $ord_data       = getCart();

        $subTotal       = 0;
        $discountAmount = 0;
        $taxAmount      = 0;
        $totalAmount    = 0;

        $query  = "INSERT INTO `wrtg_order` (`id_customer`, `order_name`, `order_time`, `order_type`, `order_status`, `order_payment_code`) VALUES ('$ord_customer', '$ord_name', '$time', '$ord_type', '$ord_status', '$ord_pay_code')";
        $insert = mysqli_query($conn, $query);

        if ($insert) {
            $query  = "SELECT * FROM `wrtg_order` WHERE `order_time`='$time' AND `id_customer`='$ord_customer'";
            $query  = mysqli_query($conn, $query);
            $latest = mysqli_fetch_assoc($query)['id_order'];

            $i = 0;
            foreach ($ord_data as $data) {
                $dtl_qty    = $data['qty'];
                $dtl_price  = $data['menu_price'];
                $dtl_id     = $data['id_menu'];
                $dtl_total  = ($data['qty'] * $data['menu_price']);
                $subTotal  += $dtl_total;

                $query = "INSERT INTO `wrtg_order_detail` (`id_order`, `id_menu`, `qty`, `price`, `total_price`) VALUES ('$latest', '$dtl_id', '$dtl_qty', '$dtl_price', '$dtl_total')";
                $insert = mysqli_query($conn, $query);
                ($insert) ? $i++ : false;

                mysqli_query($conn, "UPDATE `wrtg_menu` SET `menu_stock`=`menu_stock`-1 WHERE `id_menu`='$dtl_id'");
            }

            $taxPercentage  = 10;
            $taxAmount      = $subTotal * $taxPercentage / 100;
            $totalAmount    = ($subTotal - $discountAmount) + $taxAmount;
            $orderPayment   = $ord_pay;
            $orderPayBack   = $orderPayment - $totalAmount;

            mysqli_query($conn, "UPDATE `wrtg_order` SET `order_subtotal`='$subTotal', `order_discount`='$discountAmount', `order_tax`='$taxAmount', `order_total`='$totalAmount', `order_pay`='$orderPayment',  `order_pay_back`='$orderPayBack', `order_payment_status`='Completed' WHERE `id_order`='$latest'");

            $response['status']             = 1;
            $response['content']            = 'Berhasil membuat pesanan dengan nomor pesanan: #' . $latest;
            $response['data']['id_order']   = $latest;

            $wallet_log_name        = 'Pesanan #' . $latest;
            $wallet_log_type        = 'IN';
            $wallet_log_notes       = '-';
            $wallet_log_amount_in   = $totalAmount;
            $wallet_log_amount_out  = 0;
            mysqli_query($conn, "INSERT INTO `wrtg_wallet_log` (`id_wallet`, `type`, `name`, `amount_in`, `amount_out`, `notes`, `created_at`, `updated_at`) VALUES ('901', '$wallet_log_type', '$wallet_log_name', '$wallet_log_amount_in', '$wallet_log_amount_out', '$wallet_log_notes', '$time', '$time')");
            mysqli_query($conn, "UPDATE `wrtg_wallet` SET `wl_balance`=`wl_balance`+$wallet_log_amount_in WHERE `id_wallet`='901'");

            setCart([]);
        } else {
            $response['status']    = 0;
            $response['content']   = 'Gagal membuat pesanan ' . mysqli_error($conn);
        }
    }
} else if ($_GET['method'] == 'READ_MENU') {
    $response['status']     = 1;
    $response['content']    = '-';
    $response['data']       = array();

    if (isset($_GET['category']) && isset($_GET['search'])) {
        $category   = mysqli_real_escape_string($conn, stripslashes($_GET['category']));
        $search     = mysqli_real_escape_string($conn, stripslashes($_GET['search']));
        $query      = "SELECT * FROM `wrtg_menu` WHERE `id_category`='$category' AND `menu_name` LIKE '%$search%' ORDER BY `menu_name` ASC";
    } else if (isset($_GET['category'])) {
        $category   = mysqli_real_escape_string($conn, stripslashes($_GET['category']));
        $query      = "SELECT * FROM `wrtg_menu` WHERE `id_category`='$category' ORDER BY `menu_name` ASC";
    } else if (isset($_GET['search'])) {
        $search     = mysqli_real_escape_string($conn, stripslashes($_GET['search']));
        $query      = "SELECT * FROM `wrtg_menu` WHERE `menu_name` LIKE '%$search%'  ORDER BY `menu_name` ASC";
    } else {
        $query      = "SELECT * FROM `wrtg_menu` ORDER BY `menu_name` ASC";
    }

    $exec   = mysqli_query($conn, $query);
    while ($row = mysqli_fetch_assoc($exec)) {
        array_push($response['data'], $row);
    }

    for ($i = 0; $i < count($response['data']); $i++) {
        $row = $response['data'][$i];
        $response['data'][$i]['html_display'] = '
        <div class="col-lg-4 col-md-4 col-sm-4 col-6 mb-3">
            <table class="table-menu" id="' . $row['id_menu'] . '">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <img src="' . $config['host'] . '/' . $row['menu_images'] . '" class="img-fluid rounded">
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="2">' . $row['menu_name'] . '</td>
                    </tr>
                    <tr>
                        <td><strong>Harga</strong></td>
                        <td>' . rupiah($row['menu_price']) . '</td>
                    </tr>
                    <tr>
                        <td><strong>Stok</strong></td>
                        <td>' . $row['menu_stock'] . '</td>
                    </tr>
                </tbody>
            </table>
        </div>';
    }
} else if ($_GET['method'] == 'READ_MENU_SINGLE') {
    if (!($_GET['id_menu'])) {
        $response['status']    = 0;
        $response['content']   = 'ID menu tidak ada!';
    } else {
        $id_menu    = mysqli_real_escape_string($conn, stripslashes($_GET['id_menu']));
        $query      = "SELECT * FROM `wrtg_menu` WHERE `id_menu`='$id_menu'";
        $menu       = mysqli_query($conn, $query);

        if (!$menu) {
            $response['status']    = 0;
            $response['content']   = 'Error! ' . mysqli_error($conn);
        } else if (mysqli_num_rows($menu) < 1) {
            $response['status']    = 0;
            $response['content']   = 'Data tidak ditemukan';
        } else {
            $menu   = mysqli_fetch_assoc($menu);
            $query  = "SELECT *
            FROM `wrtg_menu_detail` `d`
            INNER JOIN `wrtg_variant_group` `g`
                ON (`d`.`id_variant_group`=`g`.`id_variant_group`)
            WHERE `id_menu`='$id_menu'";
            $variant_group  = mysqli_query($conn, $query);
            $variant_groupN = mysqli_num_rows($variant_group);
            $variant_array  = array();

            if ($variant_group) {
                while ($item = mysqli_fetch_assoc($variant_group)) {
                    $id_variant_group = $item['id_variant_group'];
                    $query = "SELECT * FROM `wrtg_variant` `v`
                    INNER JOIN `wrtg_variant_group` `g`
                        ON (`v`.`id_variant_group`=`g`.`id_variant_group`)
                    WHERE `v`.`id_variant_group`='$id_variant_group'";
                    $variant        = mysqli_query($conn, $query) or die(mysqli_error($conn));
                    $item['items']  = array();

                    while ($variantX = mysqli_fetch_assoc($variant)) {
                        array_push($item['items'], $variantX);
                    }

                    array_push($variant_array, $item);
                }

                $add = addMenuToCart($menu['id_menu']);

                $response['status']     = 1;
                $response['content']    = 'Data menu';
                $response['data']       = array(
                    'id_menu' => $menu['id_menu'],
                    'id_category' => $menu['id_category'],
                    'menu_name' => $menu['menu_name'],
                    'menu_description' => $menu['menu_description'],
                    'menu_images' => $menu['menu_images'],
                    'menu_price' => $menu['menu_price'],
                    'menu_stock' => $menu['menu_stock'],
                    'menu_variant_num' => $variant_groupN,
                    'menu_variant' => array(),
                    'add' => $add
                );

                $menu_variant = $response['data']['menu_variant'];
                for ($i = 0; $i < count($variant_array); $i++) {
                    $response['data']['menu_variant'][$i]['id_variant_group'] = $variant_array[$i]['id_variant_group'];
                    $response['data']['menu_variant'][$i]['group_name'] = $variant_array[$i]['group_name'];
                    $response['data']['menu_variant'][$i]['variant_items_num'] = count($variant_array[$i]['items']);

                    for ($j = 0; $j < count($variant_array[$i]['items']); $j++) {
                        $response['data']['menu_variant'][$i]['variant_items'][$j]['id_variant'] = $variant_array[$i]['items'][$j]['id_variant'];
                        $response['data']['menu_variant'][$i]['variant_items'][$j]['variant_name'] = $variant_array[$i]['items'][$j]['variant_name'];
                        $response['data']['menu_variant'][$i]['variant_items'][$j]['variant_price'] = $variant_array[$i]['items'][$j]['variant_price'];
                        $response['data']['menu_variant'][$i]['variant_items'][$j]['variant_description'] = $variant_array[$i]['items'][$j]['variant_description'];
                    }
                }
            } else {
                $response['status']    = 0;
                $response['content']   = 'Error! ' . mysqli_error($conn);
            }
        }
    }
} else if ($_GET['method'] == 'READ_CART') {
    $response['data'] = getCart();
} else if ($_GET['method'] == 'DELETE_CART') {
    if (!isset($_GET['id_menu'])) {
        $response['status']    = 0;
        $response['content']   = 'ID menu tidak ada!';
    } else {
        removeMenuFromCart($_GET['id_menu']);
        $response['status']    = 1;
        $response['content']   = 'Keranjang telah dihapus!';
    }
}

print json_encode($response);
exit;
