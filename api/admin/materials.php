<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $table          = 'wrtg_materials';
    $joinQuery      = null;
    $primaryKey     = 'id_material';
    $columns        = array(
        array(
            'db' => 'id_material',
            'dt' => 0,
        ),
        array(
            'db' => 'material_name',
            'dt' => 1,
        ),
        array(
            'db' => 'material_unit',
            'dt' => 2,
        ),
        array(
            'db' => 'material_stock',
            'dt' => 3,
            'formatter' => function ($d, $row) {
                return $d . ' ' . $row[2];
            }
        ),
        array(
            'db' => 'updated_at',
            'dt' => 4,
            'formatter' => function ($d, $row) {
                return date('d M Y H:i:s', $d);
            }
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery));
} else if ($_GET['method'] == "CREATE") {
    if (isset($_POST['material_name']) && isset($_POST['material_unit']) && isset($_POST['material_stock'])) {
        $material_name = mysqli_real_escape_string($conn, stripslashes($_POST['material_name']));
        $material_unit = mysqli_real_escape_string($conn, stripslashes($_POST['material_unit']));
        $material_stock = mysqli_real_escape_string($conn, stripslashes($_POST['material_stock']));

        $query = "INSERT INTO `wrtg_materials` (`material_name`, `material_unit`, `material_stock`, `created_at`, `updated_at`) VALUES ('$material_name', '$material_unit', '$material_stock', '$time', '$time')";
        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil ditambahkan ke dalam database";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat menambahkan data ke dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_material']) && !empty($_POST['id_material'])) {
        $id_material = mysqli_real_escape_string($conn, stripslashes($_POST['id_material']));
    } else {
        $response['status'] = 0;
        $response['content'] = "ID material tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_materials` WHERE `id_material`='$id_material'";
    if ($query = mysqli_query($conn, $sql)) {
        $material               = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data material";
        $response['data']       = $material;
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data material: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_CHILD") {
    if (isset($_POST['id_variant_group']) && !empty($_POST['id_variant_group'])) {
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID varian group tidak boleh kosong";

        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_variant` WHERE `id_variant_group`='$id_variant_group'";
    $query = mysqli_query($conn, $sql);
    if ($query) {
        $response['status']     = 1;
        $response['content']    = "Data varian child";
        $response['data']       = array();
        while ($row = mysqli_fetch_assoc($query)) {
            array_push($response['data'], $row);
        }
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data child varian group: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'UPDATE') {
    if (isset($_POST['material_name']) && isset($_POST['material_unit']) && isset($_POST['material_stock'])) {
        $id_material = $_POST['id_data'];
        $material_name = mysqli_real_escape_string($conn, stripslashes($_POST['material_name']));
        $material_unit = mysqli_real_escape_string($conn, stripslashes($_POST['material_unit']));
        $material_stock = mysqli_real_escape_string($conn, stripslashes($_POST['material_stock']));
        $time = time();

        $query = "UPDATE `wrtg_materials` SET `material_name`='$material_name', `material_unit`='$material_unit', `material_stock`='$material_stock', `updated_at`='$time' WHERE `id_material`='$id_material'";

        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil diubah";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat mengubah data di dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "DELETE") {
    if (isset($_POST['id_material']) && !empty($_POST['id_material'])) {
        $id_material = mysqli_real_escape_string($conn, stripslashes($_POST['id_material']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID material varian tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "DELETE FROM `wrtg_materials` WHERE `id_material`='$id_material'";
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Data material berhasil dihapus";
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal menghapus data material: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
}
