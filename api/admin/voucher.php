<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $table          = 'wrtg_voucher';
    $joinQuery      = null;
    $primaryKey     = 'id_voucher';
    $columns        = array(
        array(
            'db' => 'id_voucher',
            'dt' => 0,
        ),
        array(
            'db' => 'voc_image',
            'dt' => 1,
            'formatter' => function ($d, $row) {
                global $config;
                return '<img src="' . $config['host'] . '/' . $d . '" class="img-fluid rounded" width="150" height="100" alt="Kupon Images">';
            }
        ),
        array(
            'db' => 'voc_name',
            'dt' => 2,
        ),
        array(
            'db' => 'voc_code',
            'dt' => 3,
        ),
        array(
            'db' => 'voc_description',
            'dt' => 4,
        ),
        array(
            'db' => 'voc_type',
            'dt' => 5,
        ),
        array(
            'db' => 'voc_amount',
            'dt' => 6,
            'formatter' => function ($d, $row) {
                if ($row[5] == 'Fixed') {
                    return rupiah($d);
                } else if ($row[5] == 'Percentage') {
                    return $d . '%';
                }
            }
        ),
        array(
            'db' => 'voc_discount_max',
            'dt' => 7,
            'formatter' => function ($d, $row) {
                return rupiah($d);
            }
        ),
        array(
            'db' => 'voc_min_spend',
            'dt' => 8,
            'formatter' => function ($d, $row) {
                return rupiah($d);
            }
        ),
        array(
            'db' => 'voc_max_used',
            'dt' => 9,
            'formatter' => function ($d, $row) {
                return rupiah($d);
            }
        ),
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery));
} else if ($_GET['method'] == "CREATE") {
    if (!isset($_POST['voc_name']) || empty($_POST['voc_name'])) {
        $response['status'] = 0;
        $response['content'] = "Nama voucher harus diisi";
    } else if (!isset($_POST['voc_code']) || empty($_POST['voc_code'])) {
        $response['status'] = 0;
        $response['content'] = "Kode voucher harus diisi";
    } else if (!isset($_POST['voc_description']) || empty($_POST['voc_description'])) {
        $response['status'] = 0;
        $response['content'] = "Deskripsi voucher harus diisi";
    } else if (!isset($_POST['voc_type']) || empty($_POST['voc_type'])) {
        $response['status'] = 0;
        $response['content'] = "Tipe voucher harus diisi";
    } else if (!isset($_POST['voc_amount']) || empty($_POST['voc_amount'])) {
        $response['status'] = 0;
        $response['content'] = "Nilai voucher harus diisi";
    } else if (!isset($_POST['voc_discount_max']) || empty($_POST['voc_discount_max'])) {
        $response['status'] = 0;
        $response['content'] = "Maksimal diskon voucher harus diisi";
    } else if (!isset($_POST['voc_min_spend']) || empty($_POST['voc_min_spend'])) {
        $response['status'] = 0;
        $response['content'] = "Minimal pembelanjaan voucher harus diisi";
    } else if (!isset($_POST['voc_max_used']) || empty($_POST['voc_max_used'])) {
        $response['status'] = 0;
        $response['content'] = "Maksimal penggunaan voucher harus diisi";
    } else if (!isset($_POST['voc_max_used_daily']) || empty($_POST['voc_max_used_daily'])) {
        $response['status'] = 0;
        $response['content'] = "Maksimal penggunaan voucher per hari harus diisi";
    } else if (!isset($_POST['voc_start_at']) || empty($_POST['voc_start_at'])) {
        $response['status'] = 0;
        $response['content'] = "Tanggal mulai voucher harus diisi";
    } else if (!isset($_POST['voc_expired_at']) || empty($_POST['voc_expired_at'])) {
        $response['status'] = 0;
        $response['content'] = "Tanggal berakhir";
    } else {
        // Menghindari SQL Injection
        $voc_name           = mysqli_real_escape_string($conn, stripslashes($_POST['voc_name']));
        $voc_code           = mysqli_real_escape_string($conn, stripslashes($_POST['voc_code']));
        $voc_image          = mysqli_real_escape_string($conn, stripslashes($_POST['voc_image']));
        $voc_description    = mysqli_real_escape_string($conn, stripslashes($_POST['voc_description']));
        $voc_type           = mysqli_real_escape_string($conn, stripslashes($_POST['voc_type']));
        $voc_amount         = mysqli_real_escape_string($conn, stripslashes($_POST['voc_amount']));
        $voc_discount_max   = mysqli_real_escape_string($conn, stripslashes($_POST['voc_discount_max']));
        $voc_min_spend      = mysqli_real_escape_string($conn, stripslashes($_POST['voc_min_spend']));
        $voc_max_used       = mysqli_real_escape_string($conn, stripslashes($_POST['voc_max_used']));
        $voc_max_used_daily = mysqli_real_escape_string($conn, stripslashes($_POST['voc_max_used_daily']));
        $voc_start_at       = mysqli_real_escape_string($conn, stripslashes($_POST['voc_start_at']));
        $voc_expired_at     = mysqli_real_escape_string($conn, stripslashes($_POST['voc_expired_at']));

        $sql = "INSERT INTO `wrtg_voucher` (`voc_image`, `voc_name`, `voc_code`, `voc_description`, `voc_type`, `voc_amount`, `voc_discount_max`, `voc_min_spend`, `voc_max_used`, `voc_max_used_daily`, `voc_start_at`, `voc_expired_at`, `created_at`, `updated_at`) VALUES ('$voc_image', '$voc_name', '$voc_code', '$voc_description', '$voc_type', '$voc_amount', '$voc_discount_max', '$voc_min_spend', '$voc_max_used', '$voc_max_used_daily', '$voc_start_at', '$voc_expired_at', '$time', '$time')";

        if (mysqli_query($conn, $sql)) {
            $response['status']     = 1;
            $response['content']    = "Data berhasil ditambahkan";
        } else {
            $response['status']     = 0;
            $response['content']    = "Terjadi kesalahan saat menambahkan data: " . mysqli_error($conn);
        }
    }

    echo json_encode($response);
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_voucher']) && !empty($_POST['id_voucher'])) {
        $id_voucher = mysqli_real_escape_string($conn, stripslashes($_POST['id_voucher']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID voucher tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_voucher` WHERE `id_voucher`='$id_voucher'";
    if ($query = mysqli_query($conn, $sql)) {
        $voucher                = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data voucher";
        $response['data']       = $voucher;
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data voucher: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_CHILD") {
    if (isset($_POST['id_variant_group']) && !empty($_POST['id_variant_group'])) {
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID varian group tidak boleh kosong";

        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_variant` WHERE `id_variant_group`='$id_variant_group'";
    $query = mysqli_query($conn, $sql);
    if ($query) {
        $response['status']     = 1;
        $response['content']    = "Data varian child";
        $response['data']       = array();
        while ($row = mysqli_fetch_assoc($query)) {
            array_push($response['data'], $row);
        }
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data child varian group: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'UPDATE') {
    if (!isset($_POST['id_data']) || empty($_POST['id_data'])) {
        $response['status'] = 0;
        $response['content'] = "ID voucher harus diisi";
    } else if (!isset($_POST['voc_name']) || empty($_POST['voc_name'])) {
        $response['status'] = 0;
        $response['content'] = "Nama voucher harus diisi";
    } else if (!isset($_POST['voc_code']) || empty($_POST['voc_code'])) {
        $response['status'] = 0;
        $response['content'] = "Kode voucher harus diisi";
    } else if (!isset($_POST['voc_description']) || empty($_POST['voc_description'])) {
        $response['status'] = 0;
        $response['content'] = "Deskripsi voucher harus diisi";
    } else if (!isset($_POST['voc_type']) || empty($_POST['voc_type'])) {
        $response['status'] = 0;
        $response['content'] = "Tipe voucher harus diisi";
    } else if (!isset($_POST['voc_amount']) || empty($_POST['voc_amount'])) {
        $response['status'] = 0;
        $response['content'] = "Nilai voucher harus diisi";
    } else if (!isset($_POST['voc_discount_max']) || empty($_POST['voc_discount_max'])) {
        $response['status'] = 0;
        $response['content'] = "Maksimal diskon voucher harus diisi";
    } else if (!isset($_POST['voc_min_spend']) || empty($_POST['voc_min_spend'])) {
        $response['status'] = 0;
        $response['content'] = "Minimal pembelanjaan voucher harus diisi";
    } else if (!isset($_POST['voc_max_used']) || empty($_POST['voc_max_used'])) {
        $response['status'] = 0;
        $response['content'] = "Maksimal penggunaan voucher harus diisi";
    } else if (!isset($_POST['voc_max_used_daily']) || empty($_POST['voc_max_used_daily'])) {
        $response['status'] = 0;
        $response['content'] = "Maksimal penggunaan voucher per hari harus diisi";
    } else if (!isset($_POST['voc_start_at']) || empty($_POST['voc_start_at'])) {
        $response['status'] = 0;
        $response['content'] = "Tanggal mulai voucher harus diisi";
    } else if (!isset($_POST['voc_expired_at']) || empty($_POST['voc_expired_at'])) {
        $response['status'] = 0;
        $response['content'] = "Tanggal berakhir";
    } else {
        // Menghindari SQL Injection
        $id_voucher         = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $voc_name           = mysqli_real_escape_string($conn, stripslashes($_POST['voc_name']));
        $voc_image          = mysqli_real_escape_string($conn, stripslashes($_POST['voc_image']));
        $voc_code           = strtoupper(mysqli_real_escape_string($conn, stripslashes($_POST['voc_code'])));
        $voc_description    = mysqli_real_escape_string($conn, stripslashes($_POST['voc_description']));
        $voc_type           = mysqli_real_escape_string($conn, stripslashes($_POST['voc_type']));
        $voc_amount         = mysqli_real_escape_string($conn, stripslashes($_POST['voc_amount']));
        $voc_discount_max   = mysqli_real_escape_string($conn, stripslashes($_POST['voc_discount_max']));
        $voc_min_spend      = mysqli_real_escape_string($conn, stripslashes($_POST['voc_min_spend']));
        $voc_max_used       = mysqli_real_escape_string($conn, stripslashes($_POST['voc_max_used']));
        $voc_max_used_daily = mysqli_real_escape_string($conn, stripslashes($_POST['voc_max_used_daily']));
        $voc_start_at       = mysqli_real_escape_string($conn, stripslashes($_POST['voc_start_at']));
        $voc_expired_at     = mysqli_real_escape_string($conn, stripslashes($_POST['voc_expired_at']));

        $sql = "UPDATE `wrtg_voucher` SET `voc_image`='$voc_image', `voc_name`='$voc_name', `voc_code`='$voc_code', `voc_description`='$voc_description', `voc_type`='$voc_type', `voc_amount`='$voc_amount', `voc_discount_max`='$voc_discount_max', `voc_min_spend`='$voc_min_spend', `voc_max_used`='$voc_max_used', `voc_max_used_daily`='$voc_max_used_daily', `voc_start_at`='$voc_start_at', `voc_expired_at`='$voc_expired_at', `updated_at`='$time' WHERE `id_voucher`='$id_voucher'";

        if (mysqli_query($conn, $sql)) {
            $response['status']     = 1;
            $response['content']    = "Data berhasil diupdate";
        } else {
            $response['status']     = 0;
            $response['content']    = "Terjadi kesalahan saat mengupdate data: " . mysqli_error($conn);
        }
    }

    echo json_encode($response);
} else if ($_GET['method'] == "DELETE") {
    if (isset($_POST['id_voucher']) && !empty($_POST['id_voucher'])) {
        $id_voucher = mysqli_real_escape_string($conn, stripslashes($_POST['id_voucher']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID kupon varian tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "DELETE FROM `wrtg_voucher` WHERE `id_voucher`='$id_voucher'";
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Data kupon berhasil dihapus";
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal menghapus data kupon: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
}
