<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $table          = 'wrtg_menu_category';
    $joinQuery      = null;
    $primaryKey     = 'id_category';
    $columns        = array(
        array(
            'db' => 'id_category',
            'dt' => 0,
        ),
        array(
            'db' => 'category_name',
            'dt' => 1,
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery));
} else if ($_GET['method'] == "CREATE") {
    if (isset($_POST['category_name'])) {
        $category_name = mysqli_real_escape_string($conn, stripslashes($_POST['category_name']));

        $query = "INSERT INTO `wrtg_menu_category` (`category_name`) VALUES ('$category_name')";
        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil ditambahkan ke dalam database";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat menambahkan data ke dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_category']) && !empty($_POST['id_category'])) {
        $id_category = mysqli_real_escape_string($conn, stripslashes($_POST['id_category']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID kategori group tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_menu_category` WHERE `id_category`='$id_category'";
    if ($query = mysqli_query($conn, $sql)) {
        $employee               = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data varian group";
        $response['data']       = $employee;
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data varian group: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_CHILD") {
    if (isset($_POST['id_variant_group']) && !empty($_POST['id_variant_group'])) {
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID varian group tidak boleh kosong";

        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_variant` WHERE `id_variant_group`='$id_variant_group'";
    $query = mysqli_query($conn, $sql);
    if ($query) {
        $response['status']     = 1;
        $response['content']    = "Data varian child";
        $response['data']       = array();
        while ($row = mysqli_fetch_assoc($query)) {
            array_push($response['data'], $row);
        }
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data child varian group: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'UPDATE') {
    if (isset($_POST['category_name'])) {
        $id_category = $_POST['id_data'];
        $category_name = mysqli_real_escape_string($conn, stripslashes($_POST['category_name']));

        $query = "UPDATE `wrtg_menu_category` SET `category_name`='$category_name' WHERE `id_category`='$id_category'";

        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil diubah";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat mengubah data di dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "DELETE") {
    if (isset($_POST['id_category']) && !empty($_POST['id_category'])) {
        $id_category = mysqli_real_escape_string($conn, stripslashes($_POST['id_category']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID kategori varian tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "DELETE FROM `wrtg_menu_category` WHERE `id_category`='$id_category'";
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Data kategori berhasil dihapus";
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal menghapus data kategori: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
}
