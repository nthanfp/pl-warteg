<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();

if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $table          = 'wrtg_variant';
    $joinQuery      = "FROM `{$table}` AS `v` INNER JOIN `wrtg_variant_group` AS `g` ON (`v`.`id_variant_group` = `g`.`id_variant_group`)";
    $primaryKey     = 'id_variant';
    $columns        = array(
        array(
            'db' => 'id_variant',
            'dt' => 0,
            'field' => 'id_variant'
        ),
        array(
            'db' => 'g.group_name',
            'dt' => 1,
            'field' => 'group_name'
        ),
        array(
            'db' => 'variant_name',
            'dt' => 2,
            'field' => 'variant_name'
        ),
        array(
            'db' => 'variant_description',
            'dt' => 3,
            'field' => 'variant_description'
        ),
        array(
            'db' => 'variant_price',
            'dt' => 4,
            'field' => 'variant_price',
            'formatter' => function ($d, $row) {
                return rupiah($d);
            }
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery));
} else if ($_GET['method'] == "CREATE") {
    if (empty($_POST['id_variant_group'])) {
        $response['status'] = 0;
        $response['content'] = "ID variant group tidak boleh kosong";
        echo json_encode($response);
        exit();
    } else if (empty($_POST['variant_name'])) {
        $response['status'] = 0;
        $response['content'] = "Nama variant tidak boleh kosong";
        echo json_encode($response);
        exit();
    } else if (empty($_POST['variant_description'])) {
        $response['status'] = 0;
        $response['content'] = "Deskripsi variant tidak boleh kosong";
    } else {
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));
        $variant_name = mysqli_real_escape_string($conn, stripslashes($_POST['variant_name']));
        $variant_description = mysqli_real_escape_string($conn, stripslashes($_POST['variant_description']));
        $variant_price = mysqli_real_escape_string($conn, stripslashes($_POST['variant_price']));
        $variant_price = ($variant_price < 1) ? 0 : $variant_price;

        $query = "INSERT INTO `wrtg_variant` (`id_variant_group`, `variant_name`, `variant_description`, `variant_price`) VALUES ('$id_variant_group', '$variant_name', '$variant_description', '$variant_price')";
        $result = mysqli_query($conn, $query);

        if ($result) {
            $response['status'] = 1;
            $response['content'] = "Data variant berhasil ditambahkan ke dalam database";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat menambahkan data ke dalam database";
        }
    }

    echo json_encode($response);
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_variant']) && !empty($_POST['id_variant'])) {
        $id_variant = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID varian tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_variant` WHERE `id_variant`='$id_variant'";
    if ($query = mysqli_query($conn, $sql)) {
        $employee               = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data varian";
        $response['data']       = $employee;
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data varian: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'UPDATE') {
    if (empty($_POST['id_variant_group'])) {
        $response['status'] = 0;
        $response['content'] = "ID variant group tidak boleh kosong";
        echo json_encode($response);
        exit();
    } else if (empty($_POST['variant_name'])) {
        $response['status'] = 0;
        $response['content'] = "Nama variant tidak boleh kosong";
        echo json_encode($response);
        exit();
    } else if (empty($_POST['variant_description'])) {
        $response['status'] = 0;
        $response['content'] = "Deskripsi variant tidak boleh kosong";
    } else if (empty($_POST['variant_price'])) {
        $response['status'] = 0;
        $response['content'] = "Harga variant tidak boleh kosong";
    } else {
        $id_variant = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));
        $variant_name = mysqli_real_escape_string($conn, stripslashes($_POST['variant_name']));
        $variant_description = mysqli_real_escape_string($conn, stripslashes($_POST['variant_description']));
        $variant_price = mysqli_real_escape_string($conn, stripslashes($_POST['variant_price']));

        $query = "UPDATE `wrtg_variant` SET `id_variant_group`='$id_variant_group', `variant_name`='$variant_name', `variant_description`='$variant_description', `variant_price`='$variant_price' WHERE `id_variant`='$id_variant'";
        $result = mysqli_query($conn, $query) or die(mysqli_error($conn));

        if ($result) {
            $response['status'] = 1;
            $response['content'] = "Data variant berhasil diupdate ke dalam database";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat mengupdate data ke dalam database";
        }
    }

    echo json_encode($response);
} else if ($_GET['method'] == "DELETE") {
    if (isset($_POST['id_variant']) && !empty($_POST['id_variant'])) {
        $id_variant = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID varian tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "DELETE FROM `wrtg_variant` WHERE `id_variant`='$id_variant'";
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Data varian berhasil dihapus";
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal menghapus data varian: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
}
