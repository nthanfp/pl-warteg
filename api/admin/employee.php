<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $table          = 'wrtg_employee';
    $joinQuery      = "FROM `{$table}` AS `e` INNER JOIN `wrtg_job` AS `j` ON (`e`.`emp_job_id` = `j`.`id_job`)";
    $primaryKey     = 'id_employee';
    $columns        = array(
        array(
            'db' => 'e.id_employee',
            'dt' => 0,
            'field' => 'id_employee',
        ),
        array(
            'db' => 'emp_email',
            'dt' => 1,
            'field' => 'emp_email'
        ),
        array(
            'db' => 'emp_phone',
            'dt' => 2,
            'field' => 'emp_phone'
        ),
        array(
            'db' => 'emp_full_name',
            'dt' => 3,
            'field' => 'emp_full_name'
        ),
        array(
            'db' => 'j.job_name',
            'dt' => 4,
            'field' => 'job_name'
        ),
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery));
} else if ($_GET['method'] == "CREATE") {
    $error_message = '';
    if (empty($_POST['emp_full_name'])) {
        $error_message = 'Nama lengkap karyawan harus diisi. ';
    } else if (empty($_POST['emp_email'])) {
        $error_message = 'Email karyawan harus diisi. ';
    } else if (!filter_var($_POST['emp_email'], FILTER_VALIDATE_EMAIL)) {
        $error_message = 'Format email tidak valid. ';
    } else if (empty($_POST['emp_password'])) {
        $error_message = 'Password karyawan harus diisi. ';
    } else if (strlen($_POST['emp_password']) < 6) {
        $error_message = 'Password minimal 6 karakter. ';
    } else if (empty($_POST['emp_phone'])) {
        $error_message = 'Nomor telepon karyawan harus diisi. ';
    } else if (!preg_match('/^[0-9]+$/', $_POST['emp_phone'])) {
        $error_message = 'Nomor telepon harus angka. ';
    } else if (empty($_POST['emp_job_id'])) {
        $error_message = 'Pekerjaan karyawan harus dipilih. ';
    }

    if (!empty($error_message)) {
        $response['status'] = 0;
        $response['content'] = $error_message;
        echo json_encode($response);
        exit;
    } else {
        $emp_full_name = mysqli_real_escape_string($conn, stripslashes($_POST['emp_full_name']));
        $emp_email = mysqli_real_escape_string($conn, stripslashes($_POST['emp_email']));
        $emp_password = mysqli_real_escape_string($conn, stripslashes($_POST['emp_password']));
        $emp_phone = mysqli_real_escape_string($conn, stripslashes($_POST['emp_phone']));
        $emp_job_id = mysqli_real_escape_string($conn, stripslashes($_POST['emp_job_id']));
        $emp_register_date = time();

        $query = "INSERT INTO `wrtg_employee` (`emp_full_name`, `emp_email`, `emp_password`, `emp_phone`, `emp_job_id`, `emp_register_date`) VALUES ('$emp_full_name', '$emp_email', '$emp_password', '$emp_phone', '$emp_job_id', '$emp_register_date')";
        $result = mysqli_query($conn, $query);

        $response = array();
        if ($result) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil ditambahkan ke dalam database";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat menambahkan data ke dalam database " . mysqli_error($conn);
        }

        echo json_encode($response);
    }
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_employee']) && !empty($_POST['id_employee'])) {
        $id_employee = mysqli_real_escape_string($conn, stripslashes($_POST['id_employee']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID pegawai tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_employee` WHERE `id_employee`='$id_employee'";
    if ($query = mysqli_query($conn, $sql)) {
        $employee               = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data pegawai";
        $response['data']       = $employee;
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data pegawai: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'UPDATE') {
    $error_message = '';
    if (empty($_POST['emp_full_name'])) {
        $error_message = 'Nama lengkap karyawan harus diisi. ';
    } else if (empty($_POST['emp_email'])) {
        $error_message = 'Email karyawan harus diisi. ';
    } else if (!filter_var($_POST['emp_email'], FILTER_VALIDATE_EMAIL)) {
        $error_message = 'Format email tidak valid. ';
    } else if (empty($_POST['emp_phone'])) {
        $error_message = 'Nomor telepon karyawan harus diisi. ';
    } else if (!preg_match('/^[0-9]+$/', $_POST['emp_phone'])) {
        $error_message = 'Nomor telepon harus angka. ';
    } else if (empty($_POST['emp_job_id'])) {
        $error_message = 'Pekerjaan karyawan harus dipilih. ';
    }

    if (!empty($error_message)) {
        $response['status'] = 0;
        $response['content'] = $error_message;
        echo json_encode($response);
        exit;
    } else {
        $id_employee = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $emp_full_name = mysqli_real_escape_string($conn, stripslashes($_POST['emp_full_name']));
        $emp_email = mysqli_real_escape_string($conn, stripslashes($_POST['emp_email']));
        $emp_password = md5(mysqli_real_escape_string($conn, stripslashes($_POST['emp_password'])));
        $emp_phone = mysqli_real_escape_string($conn, stripslashes($_POST['emp_phone']));
        $emp_job_id = mysqli_real_escape_string($conn, stripslashes($_POST['emp_job_id']));

        $query = "UPDATE `wrtg_employee` SET `emp_full_name`='$emp_full_name', `emp_email`='$emp_email', `emp_phone`='$emp_phone', `emp_job_id`='$emp_job_id' WHERE `id_employee`='$id_employee'";

        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data pegawai berhasil diupdate";
        } else {
            $response['status'] = 0;
            $response['content'] = "Gagal mengupdate data pegawai";
        }

        echo json_encode($response);
    }
} else if ($_GET['method'] == "DELETE") {
    if (isset($_POST['id_employee']) && !empty($_POST['id_employee'])) {
        $id_employee = mysqli_real_escape_string($conn, stripslashes($_POST['id_employee']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID pegawai tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "DELETE FROM `wrtg_employee` WHERE `id_employee`='$id_employee'";
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Data pegawai berhasil dihapus";
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal menghapus data pegawai: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
}
