<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();

if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $table          = 'wrtg_menu';
    $joinQuery      = "FROM `{$table}` AS `v` INNER JOIN `wrtg_variant_group` AS `g` ON (`v`.`id_variant_group` = `g`.`id_variant_group`)";
    $joinQuery      = null;
    $primaryKey     = 'id_menu';
    $columns        = array(
        array(
            'db' => 'id_menu',
            'dt' => 0,
            'field' => 'id_menu'
        ),
        array(
            'db' => 'menu_images',
            'dt' => 1,
            'field' => 'menu_images',
            'formatter' => function ($d, $row) {
                global $config;
                return '<img src="' . $config['host'] . '/' . $d . '" class="img-fluid rounded" width="150" height="100" alt="Menu Images">';
            }
        ),
        array(
            'db' => 'menu_name',
            'dt' => 2,
            'field' => 'menu_name'
        ),
        array(
            'db' => 'menu_description',
            'dt' => 3,
            'field' => 'menu_description'
        ),
        array(
            'db' => 'menu_stock',
            'dt' => 4,
            'field' => 'menu_stock'
        ),
        array(
            'db' => 'menu_price',
            'dt' => 5,
            'field' => 'menu_price',
            'formatter' => function ($d, $row) {
                return rupiah($d);
            }
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery));
} else if ($_GET['method'] == "CREATE") {
    if (empty($_POST['menu_name'])) {
        $response['status'] = 0;
        $response['content'] = "Nama menu tidak boleh kosong";
        echo json_encode($response);
        exit();
    } else if (empty($_POST['menu_description'])) {
        $response['status'] = 0;
        $response['content'] = "Deskripsi menu tidak boleh kosong";
        echo json_encode($response);
        exit();
    } else if (empty($_POST['menu_price'])) {
        $response['status'] = 0;
        $response['content'] = "Harga menu tidak boleh kosong";
    } else if (empty($_POST['menu_category'])) {
        $response['status'] = 0;
        $response['content'] = "Kategori menu tidak boleh kosong";
    } else {
        $menu_name = ucwords(mysqli_real_escape_string($conn, stripslashes($_POST['menu_name'])));
        $menu_description = mysqli_real_escape_string($conn, stripslashes($_POST['menu_description']));
        $menu_category = mysqli_real_escape_string($conn, stripslashes($_POST['menu_category']));
        $menu_images = mysqli_real_escape_string($conn, stripslashes($_POST['menu_images']));
        $menu_price = mysqli_real_escape_string($conn, stripslashes($_POST['menu_price']));
        $menu_stock = mysqli_real_escape_string($conn, stripslashes($_POST['menu_stock']));
        $menu_stock = ($menu_stock < 1) ? 0 : $menu_stock;

        // query untuk menambahkan data ke database
        $sql = "INSERT INTO `wrtg_menu` (`id_category`, `menu_name`, `menu_description`, `menu_price`, `menu_stock`, `menu_images`) VALUES ('$menu_category', '$menu_name', '$menu_description', '$menu_price', '$menu_stock', '$menu_images')";

        // jalankan query dan cek status
        if (mysqli_query($conn, $sql)) {
            $response['status'] = 1;
            $response['content'] = "Data menu berhasil ditambahkan";
        } else {
            $response['status'] = 0;
            $response['content'] = "Gagal menambahkan data menu: " . mysqli_error($conn);
        }
    }

    echo json_encode($response);
} else if ($_GET['method'] == 'CREATE_MENU_VARIAN') {
    if (empty($_POST['id_menu'])) {
        $response['status'] = 0;
        $response['content'] = "ID menu tidak boleh kosong";

        echo json_encode($response);
        exit();
    } else if (empty($_POST['id_variant_group'])) {
        $response['status'] = 0;
        $response['content'] = "ID variant grup tidak boleh kosong";

        echo json_encode($response);
        exit();
    } else {
        $id_menu = mysqli_real_escape_string($conn, stripslashes($_POST['id_menu']));
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));

        $sql = "INSERT INTO `wrtg_menu_detail` (`id_variant_group`, `id_menu`) VALUES ('$id_variant_group', '$id_menu')";

        if (mysqli_query($conn, $sql)) {
            $response['status'] = 1;
            $response['content'] = "Data menu varian berhasil ditambahkan";
        } else {
            $response['status'] = 0;
            $response['content'] = "Gagal mengupdate data menu: " . mysqli_error($conn);
        }
    }

    echo json_encode($response);
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_menu']) && !empty($_POST['id_menu'])) {
        $id_menu = mysqli_real_escape_string($conn, stripslashes($_POST['id_menu']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID menu tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_menu` WHERE `id_menu`='$id_menu'";
    if ($query = mysqli_query($conn, $sql)) {
        $employee               = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data menu";
        $response['data']       = $employee;
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data menu: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_CHILD") {
    if (isset($_POST['id_menu']) && !empty($_POST['id_menu'])) {
        $id_menu = mysqli_real_escape_string($conn, stripslashes($_POST['id_menu']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID varian group tidak boleh kosong";

        print json_encode($response);
        exit;
    }

    $sql = "SELECT *
    FROM `wrtg_menu` `m`
    INNER JOIN `wrtg_menu_detail` `d`
        ON (`m`.`id_menu` = `d`.`id_menu`)
    INNER JOIN `wrtg_variant_group` `v`
        ON (`d`.`id_variant_group` = `v`.`id_variant_group`)
    WHERE `d`.`id_menu`='$id_menu'";
    $query = mysqli_query($conn, $sql);
    if ($query) {
        $response['status']     = 1;
        $response['content']    = "Data varian child";
        $response['num_data']   = mysqli_num_rows($query);
        $response['data']       = array();
        while ($row = mysqli_fetch_assoc($query)) {
            array_push($response['data'], $row);
        }

        for ($i = 0; $i < count($response['data']); $i++) {
            $query = mysqli_query($conn, "SELECT * FROM `wrtg_variant_group`");
            $response['data'][$i]['html_data_edit'] = '';
            $response['data'][$i]['html_data_edit'] .= '<option value="' . $response['data'][$i]['id_variant_group'] . '">-- (' . $response['data'][$i]['id_variant_group'] . ') ' . $response['data'][$i]['group_name'] . ' --</option>';
            while ($data = mysqli_fetch_assoc($query)) {
                $response['data'][$i]['html_data_edit'] .= '<option value="' . $data['id_variant_group'] . '">(' . $data['id_variant_group'] . ') ' . $data['group_name'] . '</option>';
            }
        }
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data child varian group: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'UPDATE') {
    if (empty($_POST['menu_name'])) {
        $response['status'] = 0;
        $response['content'] = "Nama menu tidak boleh kosong";
        echo json_encode($response);
        exit();
    } else if (empty($_POST['menu_description'])) {
        $response['status'] = 0;
        $response['content'] = "Deskripsi menu tidak boleh kosong";
        echo json_encode($response);
        exit();
    } else if (empty($_POST['menu_price'])) {
        $response['status'] = 0;
        $response['content'] = "Harga menu tidak boleh kosong";
    } else if (empty($_POST['menu_stock'])) {
        $response['status'] = 0;
        $response['content'] = "Stok menu tidak boleh kosong";
    } else if (empty($_POST['menu_category'])) {
        $response['status'] = 0;
        $response['content'] = "Kategori menu tidak boleh kosong";
    } else {
        $id_menu = mysqli_real_escape_string($conn, stripslashes($_POST['id_data']));
        $menu_name = ucwords(mysqli_real_escape_string($conn, stripslashes($_POST['menu_name'])));
        $menu_description = mysqli_real_escape_string($conn, stripslashes($_POST['menu_description']));
        $menu_price = mysqli_real_escape_string($conn, stripslashes($_POST['menu_price']));
        $menu_stock = mysqli_real_escape_string($conn, stripslashes($_POST['menu_stock']));
        $menu_category = mysqli_real_escape_string($conn, stripslashes($_POST['menu_category']));
        $menu_images = mysqli_real_escape_string($conn, stripslashes($_POST['menu_images']));

        $sql = "UPDATE `wrtg_menu` SET `id_category`='$menu_category', `menu_name` = '$menu_name', `menu_description` = '$menu_description', `menu_price` = '$menu_price', `menu_stock` = '$menu_stock', `menu_images`='$menu_images' WHERE `id_menu` = '$id_menu'";

        if (mysqli_query($conn, $sql)) {
            $response['status'] = 1;
            $response['content'] = "Data menu berhasil diupdate";
        } else {
            $response['status'] = 0;
            $response['content'] = "Gagal mengupdate data menu: " . mysqli_error($conn);
        }
    }

    echo json_encode($response);
} else if ($_GET['method'] == 'UPDATE_MENU_VARIAN') {
    if (empty($_POST['id_menu'])) {
        $response['status'] = 0;
        $response['content'] = "ID menu tidak boleh kosong";

        echo json_encode($response);
        exit();
    } else if (empty($_POST['id_variant_group'])) {
        $response['status'] = 0;
        $response['content'] = "ID variant grup tidak boleh kosong";

        echo json_encode($response);
        exit();
    } else {
        $id_menu = mysqli_real_escape_string($conn, stripslashes($_POST['id_menu']));
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));

        $sql = "UPDATE `wrtg_menu_detail` SET `id_variant_group` = '$id_variant_group' WHERE `id_menu` = '$id_menu'";

        if (mysqli_query($conn, $sql)) {
            $response['status'] = 1;
            $response['content'] = "Data menu berhasil diupdate";
        } else {
            $response['status'] = 0;
            $response['content'] = "Gagal mengupdate data menu: " . mysqli_error($conn);
        }
    }

    echo json_encode($response);
} else if ($_GET['method'] == "DELETE") {
    if (isset($_POST['id_menu']) && !empty($_POST['id_menu'])) {
        $id_menu = mysqli_real_escape_string($conn, stripslashes($_POST['id_menu']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID menu tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "DELETE FROM `wrtg_menu` WHERE `id_menu`='$id_menu'";
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Data menu berhasil dihapus";
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal menghapus data menu: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'DELETE_MENU_VARIAN') {
    if (empty($_POST['id_menu'])) {
        $response['status'] = 0;
        $response['content'] = "ID menu tidak boleh kosong";

        echo json_encode($response);
        exit();
    } else if (empty($_POST['id_variant_group'])) {
        $response['status'] = 0;
        $response['content'] = "ID variant grup tidak boleh kosong";

        echo json_encode($response);
        exit();
    } else {
        $id_menu = mysqli_real_escape_string($conn, stripslashes($_POST['id_menu']));
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));

        $sql = "DELETE FROM `wrtg_menu_detail` WHERE `id_variant_group`='$id_variant_group' AND `id_menu`='$id_menu'";

        if (mysqli_query($conn, $sql)) {
            $response['status'] = 1;
            $response['content'] = "Data menu varian berhasil dihapus";
        } else {
            $response['status'] = 0;
            $response['content'] = "Gagal mengupdate data menu: " . mysqli_error($conn);
        }
    }

    echo json_encode($response);
}
