<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');
$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['result']    = 0;
    $response['content']   = 'Session error';
} else if ($_GET['method'] == 'UPDATE_BASIC') {
    $x_key = array_keys($_POST);
    $x_val = array_values($_POST);

    $r_1 = 0;
    $r_0 = 0;

    for ($i = 0; $i < count($x_key); $i++) {
        $c_key = $x_key[$i];
        $c_val = $x_val[$i];
        $query = "UPDATE `wrtg_config` SET `value`='$c_val' WHERE `name`='$c_key'";
        $query = mysqli_query($conn, $query);
        ($query) ? $r_1++ : $r_0++;
    }

    if ($r_1 > 0) {
        $response['result']    = 1;
        $response['content']   = $r_1 . ' Data berhasil diupdate';
    } else {
        $response['result']    = 0;
        $response['content']   = 'Error ' . mysqli_error($conn);
    }
}
print_r(json_encode($response));
