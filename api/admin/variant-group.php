<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
header('Content-Type: application/json');
require('../../lib/config.php');

$response = array();
if ($_SESSION['emp_status'] != 'login') {
    $response['status']    = 0;
    $response['content']   = 'Session error';

    print json_encode($response);
    exit;
} else if (!($_GET['method'])) {
    $response['status']    = 0;
    $response['content']   = 'Method not found!';

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_LIST") {
    $table          = 'wrtg_variant_group';
    $joinQuery      = null;
    $primaryKey     = 'id_variant_group';
    $columns        = array(
        array(
            'db' => 'id_variant_group',
            'dt' => 0,
        ),
        array(
            'db' => 'group_name',
            'dt' => 1,
        ),
        array(
            'db' => 'description',
            'dt' => 2,
        )
    );

    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery));
} else if ($_GET['method'] == "CREATE") {
    if (isset($_POST['group_name']) && isset($_POST['description'])) {
        $group_name = mysqli_real_escape_string($conn, stripslashes($_POST['group_name']));
        $description = mysqli_real_escape_string($conn, stripslashes($_POST['description']));

        $query = "INSERT INTO `wrtg_variant_group` (`group_name`, `description`) VALUES ('$group_name', '$description')";
        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil ditambahkan ke dalam database";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat menambahkan data ke dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "READ_SINGLE") {
    if (isset($_POST['id_variant_group']) && !empty($_POST['id_variant_group'])) {
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID varian group tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_variant_group` WHERE `id_variant_group`='$id_variant_group'";
    if ($query = mysqli_query($conn, $sql)) {
        $employee               = mysqli_fetch_assoc($query);
        $response['status']     = 1;
        $response['content']    = "Data varian group";
        $response['data']       = $employee;
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data varian group: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == "READ_CHILD") {
    if (isset($_POST['id_variant_group']) && !empty($_POST['id_variant_group'])) {
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID varian group tidak boleh kosong";

        print json_encode($response);
        exit;
    }

    $sql = "SELECT * FROM `wrtg_variant` WHERE `id_variant_group`='$id_variant_group'";
    $query = mysqli_query($conn, $sql);
    if ($query) {
        $response['status']     = 1;
        $response['content']    = "Data varian child";
        $response['data']       = array();
        while ($row = mysqli_fetch_assoc($query)) {
            array_push($response['data'], $row);
        }
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal mengambil data child varian group: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
} else if ($_GET['method'] == 'UPDATE') {
    if (isset($_POST['group_name']) && isset($_POST['description'])) {
        $id_variant_group = $_POST['id_data'];
        $group_name = mysqli_real_escape_string($conn, stripslashes($_POST['group_name']));
        $description = mysqli_real_escape_string($conn, stripslashes($_POST['description']));

        $query = "UPDATE `wrtg_variant_group` SET `group_name`='$group_name', `description`='$description' WHERE `id_variant_group`='$id_variant_group'";
        if (mysqli_query($conn, $query)) {
            $response['status'] = 1;
            $response['content'] = "Data berhasil diubah";
        } else {
            $response['status'] = 0;
            $response['content'] = "Terjadi kesalahan saat mengubah data di dalam database: " . mysqli_error($conn);
        }
    } else {
        $response['status'] = 0;
        $response['content'] = "Data yang dimasukkan tidak lengkap";
    }

    echo json_encode($response);
} else if ($_GET['method'] == "DELETE") {
    if (isset($_POST['id_variant_group']) && !empty($_POST['id_variant_group'])) {
        $id_variant_group = mysqli_real_escape_string($conn, stripslashes($_POST['id_variant_group']));
    } else {
        $response['status']     = 0;
        $response['content']    = "ID grup varian tidak boleh kosong";
        print json_encode($response);
        exit;
    }

    $sql = "DELETE FROM `wrtg_variant_group` WHERE `id_variant_group`='$id_variant_group'";
    if (mysqli_query($conn, $sql)) {
        $response['status']     = 1;
        $response['content']    = "Data grup varian berhasil dihapus";
    } else {
        $response['status']     = 0;
        $response['content']    = "Gagal menghapus data grup varian: " . mysqli_error($conn);
    }

    print json_encode($response);
    exit;
}
