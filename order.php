<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('lib/config.php');
$config['pagename'] = 'Pesanan';
$config['title']    = $config['name'] . ' - ' . $config['pagename'];
if (isset($_SESSION['cst_status']) && ($_SESSION['cst_status'] == 'login')) {
    $id_cust    = $_SESSION['cst_id'];
    $query      = mysqli_query($conn, "SELECT * FROM `wrtg_customer` WHERE `id_customer`='$id_cust'");
    $cust       = mysqli_fetch_assoc($query);
} else {
    header('Location: ' . $config['host'] . '/login');
    exit;
}

// Method
if (isset($_GET['method'])) {
    if (($_GET['method'] == 'CONFIRM') && isset($_GET['id_order'])) {
        $id_order   = $_GET['id_order'];
        $query      = "SELECT * FROM `wrtg_order` WHERE `id_order`='$id_order'";
        $data       = mysqli_query($conn, $query) or die(mysqli_error($conn));
        $order      = mysqli_fetch_assoc($data);
        if ($order['order_status'] == 'Process' || $order['order_status'] == 'OnDelivery') {
            mysqli_query($conn, "UPDATE `wrtg_order` SET `order_status`='Completed' WHERE  `id_order`='$id_order'");
            header('Location: ' . $config['host'] . '/order/detail/' . $id_order);
        } else {
            header('Location: ' . $config['host'] . '/order/detail/' . $id_order);
        }
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <?php include('inc/head.phtml') ?>
    <style>
        .detail-icon {
            width: 1.6rem;
            font-size: 1.2rem;
            color: #2980B9;
            text-align: center;
            margin-left: 0px;
            margin-right: 5px;
        }

        .rating {
            display: flex;
            flex-direction: row-reverse;
            justify-content: center
        }

        .rating>input {
            display: none
        }

        .rating>label {
            position: relative;
            width: 1em;
            font-size: 65px;
            font-weight: 300;
            color: #FFD600;
            cursor: pointer
        }

        .rating>label::before {
            content: "\2605";
            position: absolute;
            opacity: 0
        }

        .rating>label:hover:before,
        .rating>label:hover~label:before {
            opacity: 1 !important
        }

        .rating>input:checked~label:before {
            opacity: 1
        }

        .rating:hover>input:checked~label:before {
            opacity: 0.4
        }
    </style>
</head>

<body>

    <?php include('inc/sidebar.phtml'); ?>

    <div class="w3-main" style="margin-left: 340px; margin-right: 40px">

        <?php include('inc/headbox.phtml'); ?>

        <!-- Content -->
        <div class="w3-container" style="margin-top: 10px" id="showcase">

            <h1 class="fs-2" style="color: #2980b9;"><b>Pesanan</b></h1>
            <hr style="width: 50px; border: 5px solid #2980b9; " class="w3-round" />

            <?php
            if (isset($_GET['id_order'])) {
                $id_order   = $_GET['id_order'];
                $query      = "SELECT * FROM `wrtg_order` WHERE `id_order`='$id_order'";
                $data       = mysqli_query($conn, $query) or die(mysqli_error($conn));
                $order      = mysqli_fetch_assoc($data);
            ?>
                <!-- Order Detail Section -->
                <div class="row">

                    <div class="col-lg-8">

                        <?php if ($order['order_status'] <> 'Completed' && $order['order_status'] <> 'Canceled') { ?>
                            <!-- Info -->
                            <div class="alert alert-info">Harap konfirmasi <b>Pesanan Diterima</b> jika sudah menerima semua pesanan, jika tidak maka akan terkonfirmasi otomatis setelah 3 jam pesanan dibuat.</div>
                            <!-- End: info -->
                        <?php } ?>

                        <!-- Payment -->
                        <?php if ($order['order_payment_status'] == 'Pending') { ?>
                            <div class="card mb-4">
                                <div class="card-header">Cara Pembayaran</div>
                                <div class="card-body">
                                    <?php if ($order['order_payment_code'] == 'COD') { ?>
                                        <p class="mb-2" style="text-align: justify;">
                                            Harap siapkan pembayaran berjumlah <strong><?= rupiah($order['order_total']); ?></strong> dengan metode pembayaran <strong>Bayar di Tempat/COD</strong>. Setelah ini maka pelayan akan datang untuk menyelesaikan pembayaran dan memproses pesanan Anda.
                                        </p>
                                    <?php } else if ($order['order_payment_code'] == 'QRIS-1') { ?>
                                        <div class="mb-2">
                                            <img src="https://th.bing.com/th/id/R.fa5437cc7deebacde437b404a5a8facd?rik=ukBycy7vkBJBgg&riu=http%3a%2f%2flmentalmkt.files.wordpress.com%2f2013%2f06%2fcodigo-qr.jpg&ehk=Hdf5jUhNlNZZmJ6pR0EOAYSLd9iWMh1ydvvthw2P3OE%3d&risl=&pid=ImgRaw&r=0" height="150" class="mx-auto d-block">
                                        </div>
                                        <p class="mb-2" style="text-align: justify;">
                                            Silahkan melakukan pembayaran berjumlah <strong><?= rupiah($order['order_total']); ?></strong> dengan metode pembayaran <strong>QRIS</strong>. QR tersebut bisa <i>discan</i> melalui GoPay, OVO, DANA, ShopeePay, BCA Mobile, BRIMO, dan aplikasi m-banking lainnya.
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <!-- End: Payment -->

                        <!-- Details -->
                        <div class="card mb-4">
                            <div class="card-header">Detail Pemesanan</div>
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-sm-12 col-md-6 mb-2">
                                        <span class=""><i class="detail-icon fas fa-shopping-cart"></i> Pesanan #<?= $order['id_order']; ?></span>
                                    </div>
                                    <div class="col-sm-12 col-md-6 mb-2">
                                        <span class="float-md-end"><i class="detail-icon fas fa-user"></i> <?= $order['order_name']; ?></span>
                                    </div>
                                    <div class="col-sm-12 col-md-6 mb-2">
                                        <span class="me-3"><i class="detail-icon fas fa-clock"></i> <?= date('d M Y H:i:s', $order['order_time']); ?></span>
                                    </div>
                                    <div class="col-sm-12 col-md-6 mb-2">
                                        <?php
                                        if ($order['order_status'] == 'Pending') {
                                            print('<span class="badge bg-warning float-md-end">Menunggu Konfirmasi</span>');
                                        } else if ($order['order_status'] == 'Process') {
                                            print('<span class="badge bg-primary float-md-end">Dalam Proses</span>');
                                        } else if ($order['order_status'] == 'Completed') {
                                            print('<span class="badge bg-success float-md-end">Selesai</span>');
                                        } else if ($order['order_status'] == 'Canceled') {
                                            print('<span class="badge bg-danger float-md-end">Dibatalkan</span>');
                                        } else {
                                        }
                                        ?>
                                    </div>
                                </div>
                                <p class="fs-6 fw-border"><i class="detail-icon fas fa-list"></i> Daftar Pesanan</p>
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <td width="10" class="text-nowrap text-center">#</td>
                                                <td class="text-nowrap d-none d-md-block text-center">Gambar</td>
                                                <td class="text-nowrap text-left">Menu</td>
                                                <td class="text-nowrap text-center">Qty</td>
                                                <td class="text-nowrap text-end">Harga</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $query  = "SELECT *
                                        FROM `wrtg_order_detail` `d`
                                        INNER JOIN `wrtg_menu` `m`
                                            ON (`d`.`id_menu`=`m`.`id_menu`)
                                        WHERE `id_order`='$id_order'";
                                            $data   = mysqli_query($conn, $query) or die(mysqli_error($conn));
                                            $cntr   = 0;
                                            while ($row = mysqli_fetch_assoc($data)) {
                                                $cntr++;
                                            ?>
                                                <tr>
                                                    <td class="text-nowrap text-center text-muted"><?= $cntr; ?></td>
                                                    <td class="text-nowrap d-none d-md-block justify-content-center"><img width="120" class="img-fluid rounded mx-auto d-block" src="<?= $config['host']; ?>/<?= $row['menu_images']; ?>"></td>
                                                    <td class="text-nowrap text-muted"><?= $row['menu_name']; ?></td>
                                                    <td class="text-nowrap text-muted text-center"><?= $row['qty']; ?></td>
                                                    <td class="text-nowrap text-muted text-end"><?= rupiah($row['price']); ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="md-1 px-2">
                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">Total Bayar</p>
                                        <p class="mb-2" id="ch_subtotal"><?= rupiah($order['order_subtotal']); ?></p>
                                    </div>

                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">Diskon</p>
                                        <p class="mb-2 text-danger" id="ch_discount">- <?= rupiah($order['order_discount']); ?></p>
                                    </div>

                                    <div class="d-flex justify-content-between">
                                        <p class="mb-2">Pajak</p>
                                        <p class="mb-2" id="ch_tax"><?= rupiah($order['order_tax']); ?></p>
                                    </div>

                                    <div class="fw-bold d-flex justify-content-between mb-2">
                                        <p class="mb-2">Total (Termasuk Pajak)</p>
                                        <p class="mb-2" id="ch_total"><?= rupiah($order['order_total']); ?></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- End: Details -->
                    </div>

                    <div class="col-lg-4">

                        <?php if ($order['order_status'] <> 'Completed' && $order['order_status'] <> 'Canceled') { ?>
                            <div class="card mb-4">
                                <div class="card-header">Konfirmasi Pesanan</div>
                                <div class="card-body">
                                    <a href="<?= $config['host']; ?>/order/confirm/<?= $order['id_order']; ?>">
                                        <button type="button" class="w-100 btn btn-primary">Pesanan Sudah Diterima</button>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>

                        <!-- Payment -->
                        <div class="card mb-4">
                            <div class="card-header">Status Pembayaran</div>
                            <div class="card-body">
                                <div class="table-responsive text-nowrap">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <th>Metode Pembayaran</th>
                                                <th>:</th>
                                                <td><?= $order['order_payment_code']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Total Pembayaran</th>
                                                <th>:</th>
                                                <td><?= rupiah($order['order_pay']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <th>:</th>
                                                <td>
                                                    <?php
                                                    if ($order['order_payment_status'] == 'Pending') {
                                                        print('<span class="badge bg-warning">Menunggu Pembayaran</span>');
                                                    } else if ($order['order_payment_status'] == 'Verification') {
                                                        print('<span class="badge bg-primary">Proses Verifikasi</span>');
                                                    } else if ($order['order_payment_status'] == 'Confirmed') {
                                                        print('<span class="badge bg-success">Terkonfirmasi</span>');
                                                    } else if ($order['order_payment_status'] == 'Canceled') {
                                                        print('<span class="badge bg-danger">Dibatalkan</span>');
                                                    } else {
                                                        print('<span class="badge bg-danger">Unknown</span>');
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- End: Payment -->

                        <!-- Customer Notes -->
                        <div class="card mb-4">
                            <div class="card-header">Catatan Pelanggan</div>
                            <div class="card-body">
                                <p><?= $order['order_notes']; ?></p>
                            </div>
                        </div>
                        <!-- End: Customer Notes -->


                        <!-- Shipping information -->
                        <div class="card mb-4">
                            <div class="card-header">Informasi Pengiriman</div>
                            <div class="card-body">
                                <?php
                                $d = strtoupper($order['order_type']);
                                if ($d == 'TAKEAWAY') {
                                ?>

                                <?php
                                } else if ($d == 'DELIVERY') {
                                ?>
                                    <strong>Bahari Delivery</strong>
                                    <hr style="margin-bottom: 3px; margin-top: 3px;">
                                    <h3 class="h6">Alamat Tujuan</h3>
                                    <address>
                                        <strong><?= $order['order_name']; ?></strong><br>
                                        <?= $order['order_address']; ?>
                                        <abbr title="Phone">No Telp:</abbr> <?= $cust['cst_phone']; ?>
                                    </address>
                                <?php
                                } else if ($d == 'DINEIN') {
                                ?>
                                    <h3 class="h6">No Meja</h3>
                                    <p>36</p>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- End: Shipping information -->
                    </div>
                </div>
                <!-- End: Order Detail Section -->

            <?php
            } else {
            ?>

                <!-- Nav Tabs -->
                <ul class="nav nav-tabs nav justify-content-center pt-3" id="myTab" role="tablist">
                    <li class="nav-item">
                        <button class="nav-link active" id="tab_ord_process" data-bs-toggle="tab" data-bs-target="#ord_process" type="button" role="tab" aria-controls="home" aria-selected="true">Diproses</button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link" id="tab_ord_done" data-bs-toggle="tab" data-bs-target="#ord_done" type="button" role="tab" aria-controls="profile" aria-selected="false">Selesai</button>
                    </li>
                </ul>
                <!-- End: Nav Tabs -->

                <!-- Tab Panel -->
                <div class="tab-content mt-3" id="myTabContent">

                    <!-- Order Proses -->
                    <div class="tab-pane fade show active" id="ord_process" role="tabpanel" aria-labelledby="tab_ord_process">
                        <?php
                        $query  = "SELECT * FROM `wrtg_order` WHERE `id_customer`='$id_cust' AND `order_status` IN('Process', 'Pending') ORDER BY `id_order` DESC";
                        $data   = mysqli_query($conn, $query) or die(mysqli_error($conn));
                        if (mysqli_num_rows($data) < 1) { ?>
                            <div class="alert alert-warning">Tidak ada pesanan dengan status <b>Dalam Proses</b></div>
                            <?php } else {
                            while ($row = mysqli_fetch_assoc($data)) {
                            ?>

                                <div class="card mb-2">
                                    <div class="card-header justify-content-between d-flex">
                                        <span>Pesanan #<?= $row['id_order']; ?></span>
                                        <?php
                                        if ($row['order_status'] == 'Pending') {
                                            print('<span class="badge bg-warning">Menunggu Konfirmasi</span>');
                                        } else if ($row['order_status'] == 'Process') {
                                            print('<span class="badge bg-primary">Dalam Proses</span>');
                                        } else if ($row['order_status'] == 'Completed') {
                                            print('<span class="badge bg-success">Selesai</span>');
                                        } else if ($row['order_status'] == 'Canceled') {
                                            print('<span class="badge bg-danger">Dibatalkan</span>');
                                        } else {
                                        }
                                        ?>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 mb-2">
                                                <?php
                                                $d = strtoupper($row['order_type']);
                                                if ($d == 'TAKEAWAY') {
                                                    print('<span class="text-muted"><i class="fas fa-motorcycle"></i> Takeaway</span>');
                                                } else if ($d == 'DELIVERY') {
                                                    print('<span class="text-muted"><i class="fas fa-motorcycle"></i> Delivery</span>');
                                                } else if ($d == 'DINEIN') {
                                                    print('<span class="text-muted"><i class="fas fa-utensils"></i> Makan di Tempat</span>');
                                                }
                                                ?>
                                            </div>
                                            <div class="col-md-6 col-lg-6 mb-2">
                                                <span class="text-muted float-md-end"><i class="fas fa-clock"></i> <?= date('d M Y H:i:s', $row['order_time']); ?></span>
                                            </div>
                                        </div>
                                        <div class="justify-content-start d-flex mb-2">
                                            <a href="<?= $config['host']; ?>/order/detail/<?= $row['id_order']; ?>" class="btn btn-sm btn-primary" style="margin-right: 6px;"><i class="fas fa-info"></i> Detail Pesanan</a>
                                            <a href="" class="btn btn-sm btn-outline-primary"><i class="fas fa-pen-alt"></i> Tulis Ulasan Pesanan</a>
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
                    <!-- End: Order Proses -->

                    <!-- Order Done -->
                    <div class="tab-pane fade" id="ord_done" role="tabpanel" aria-labelledby="tab_ord_done">
                        <?php
                        $query  = "SELECT * FROM `wrtg_order` WHERE `id_customer`='$id_cust' AND `order_status` IN('Completed', 'Canceled') ORDER BY `id_order` DESC";
                        $data   = mysqli_query($conn, $query) or die(mysqli_error($conn));
                        if (mysqli_num_rows($data) < 1) { ?>
                            <div class="alert alert-warning">Tidak ada pesanan dengan status <b>Selesai</b></div>
                            <?php } else {
                            while ($row = mysqli_fetch_assoc($data)) {
                            ?>
                                <div class="card mb-2">
                                    <div class="card-header justify-content-between d-flex">
                                        <span>Pesanan #<?= $row['id_order']; ?></span>
                                        <?php
                                        if ($row['order_status'] == 'Pending') {
                                            print('<span class="badge bg-warning">Menunggu Konfirmasi</span>');
                                        } else if ($row['order_status'] == 'Process') {
                                            print('<span class="badge bg-primary">Dalam Proses</span>');
                                        } else if ($row['order_status'] == 'Completed') {
                                            print('<span class="badge bg-success">Selesai</span>');
                                        } else if ($row['order_status'] == 'Canceled') {
                                            print('<span class="badge bg-danger">Dibatalkan</span>');
                                        } else {
                                        }
                                        ?>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 mb-2">
                                                <?php
                                                $d = strtoupper($row['order_type']);
                                                if ($d == 'TAKEAWAY') {
                                                    print('<span class="text-muted"><i class="fas fa-motorcycle"></i> Takeaway</span>');
                                                } else if ($d == 'DELIVERY') {
                                                    print('<span class="text-muted"><i class="fas fa-motorcycle"></i> Delivery</span>');
                                                } else if ($d == 'DINEIN') {
                                                    print('<span class="text-muted"><i class="fas fa-utensils"></i> Makan di Tempat</span>');
                                                }
                                                ?>
                                            </div>
                                            <div class="col-md-6 col-lg-6 mb-2">
                                                <span class="text-muted float-md-end"><i class="fas fa-clock"></i> <?= date('d M Y H:i:s', $row['order_time']); ?></span>
                                            </div>
                                        </div>
                                        <div class="justify-content-start d-flex mb-2">
                                            <a href="<?= $config['host']; ?>/order/detail/<?= $row['id_order']; ?>" class="btn btn-sm btn-primary" style="margin-right: 6px;"><i class="fas fa-info"></i> Detail Pesanan</a>
                                            <button class="btn btn-sm btn-outline-primary" onclick="openFormReview('<?= $row['id_order']; ?>');"><i class="fas fa-pen-alt"></i> Tulis Ulasan
                                                Pesanan</button>
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }
                        }
                        ?>

                        <!-- Modal -->
                        <div class="modal fade" id="reviewModal" tabindex="-1" aria-labelledby="reviewModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="reviewModalLabel">Berikan Ulasan</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div id="Review-Data" style="display: none;">
                                            <figure style="margin: 8px;">
                                                <blockquote class="blockquote">
                                                    <p id="dt_rev_message">A well-known quote, contained in a blockquote element.</p>
                                                </blockquote>
                                                <figcaption class="blockquote-footer">
                                                    <span id="dt_rev_stars"></span> <strong><span id="dt_rev_rating"></span>.0</strong> dari <strong>5.0</strong>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <form method="POST" action="<?= $config['host']; ?>/api/v1/review?method=ADD_REVIEW" id="Add-Review-Form" style="display: block;">
                                            <input type="hidden" name="id_order" id="val_id_order">
                                            <div class="row">
                                                <div class="mb-3 col-md-6">
                                                    <div class="form-group">
                                                        <label class="label" for="rev_name">Nama Lengkap</label>
                                                        <input type="text" class="form-control" name="rev_name" id="rev_name" placeholder="Nama Lengkap" value="<?= $_SESSION['cst_name']; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="mb-3 col-md-6">
                                                    <div class="form-group">
                                                        <label class="label" for="rev_email">Alamat Email</label>
                                                        <input type="email" class="form-control" name="rev_email" id="rev_email" placeholder="Alamat Email" value="<?= $_SESSION['cst_email']; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="mb-3 col-md-12">
                                                    <div class="form-group">
                                                        <label class="label" for="#">Ulasan Mengenai Pesanan</label>
                                                        <textarea name="rev_message" class="form-control" id="rev_message" cols="30" rows="4" placeholder="Berikan ulasan anda..."></textarea>
                                                    </div>
                                                </div>
                                                <div class="mb-3 col-md-12">
                                                    <div class="form-group">
                                                        <label class="label" for="#">Berikan Rating</label>
                                                        <div class="rate text-white">
                                                            <div class="rating">
                                                                <input type="radio" name="rev_rating" value="5" id="5">
                                                                <label for="5">☆</label>
                                                                <input type="radio" name="rev_rating" value="4" id="4">
                                                                <label for="4">☆</label>
                                                                <input type="radio" name="rev_rating" value="3" id="3">
                                                                <label for="3">☆</label>
                                                                <input type="radio" name="rev_rating" value="2" id="2">
                                                                <label for="2">☆</label>
                                                                <input type="radio" name="rev_rating" value="1" id="1">
                                                                <label for="1">☆</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mb-3">
                                                    <button type="submit" class="btn btn-primary">Kirim Ulasan</button>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End: Order Done -->

                </div>
                <!-- End: Tab Panel -->

            <?php } ?>

        </div>
        <!-- End Content -->
    </div>

    <?php include('inc/foot.phtml'); ?>
    <script type="text/javascript">
        function openFormReview(id_order) {
            $('#val_id_order').val(id_order);
            $.ajax({
                url: '<?= $config['host']; ?>/api/v1/review?method=GET_REVIEW',
                data: {
                    id_order
                },
                timeout: false,
                type: 'POST',
                dataType: 'JSON',
                success: function(hasil) {
                    if (hasil.status) {
                        let htmlStar = '';
                        for (let i = 0; i < 5; i++) {
                            if (i < parseInt(hasil.data.rev_rating)) {
                                htmlStar += '<i class="fas fa-star" style="color: #FD7E14;"></i>';
                            } else {
                                htmlStar += '<i class="far fa-star"></i>';
                            }
                        }

                        $('#reviewModal').modal('show');
                        $('#dt_rev_message').html(hasil.data.rev_message);
                        $('#dt_rev_rating').html(hasil.data.rev_rating);
                        $('#dt_rev_stars').html(htmlStar);
                        $('#Review-Data').show();
                        $('#Add-Review-Form').hide();
                    } else {
                        $('#reviewModal').modal('show');
                        $('#Review-Data').hide();
                        $('#Add-Review-Form').show();
                    }
                },
                error: function(a, b, c) {},
                beforeSend: function() {}
            });
        }

        $(document).ready(function() {
            $("form#Add-Review-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('Kirim Ulasan');
                        if (hasil.status) {
                            swal("Berhasil!", hasil.content, "success");
                            $('#reviewModal').modal('hide');
                            $("#rev_review").val('');
                            $("#rev_rating").val('');
                        } else {
                            swal("Gagal!", hasil.content, "error");
                        }
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('Kirim Ulasan');
                        $('#reviewModal').modal('hide');
                        $("#rev_review").val('');
                        $("#rev_rating").val('');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("button").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                    }
                });
                return false
            });
        });
    </script>
</body>

</html>