<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Kelola Pelanggan';
$allow_position = array('DEVELOPER', 'PEMILIK', 'MANAGER');
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
} else if (in_array(strtoupper($_SESSION['emp_job_name']), $allow_position) == false) {
    header('Location:' . $config['host_admin']);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php include('../inc/admin-page/admin-header.phtml'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Kelola Data Pelanggan</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-database"></i> Data Pelanggan</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <a class="btn btn-info" id="btnTambah" href="#" data-toggle="modal" data-target="#modalAdd" role="button"><i class="fas fa-plus"></i> Tambah Pelanggan Baru</a>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="List-Data" class="display table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Email</th>
                                                    <th class="text-nowrap">No Telepon</th>
                                                    <th class="text-nowrap">Nama Lengkap</th>
                                                    <th class="text-nowrap">Alamat</th>
                                                    <th class="text-nowrap">Tanggal Registrasi</th>
                                                    <th class="text-nowrap">Terakhir Login</th>
                                                    <th class="text-nowrap">Alamat IP</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Email</th>
                                                    <th class="text-nowrap">No Telepon</th>
                                                    <th class="text-nowrap">Nama Lengkap</th>
                                                    <th class="text-nowrap">Alamat</th>
                                                    <th class="text-nowrap">Tanggal Registrasi</th>
                                                    <th class="text-nowrap">Terakhir Login</th>
                                                    <th class="text-nowrap">Alamat IP</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.container-fluid -->

                <!-- Start: Modal View Data -->
                <div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="modalView" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">View API Parameter</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Required</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody id="Api-Param">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal View Data -->

                <!-- Start: Modal Edit Data -->
                <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit User</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="<?= $config['host']; ?>/api/admin/customer?method=UPDATE" id="Edit-Data-Form">
                                    <input type="hidden" id="val_id_data" name="id_data">
                                    <div class="form-group">
                                        <label for="cst_full_name">Nama Lengkap</label>
                                        <input type="text" name="cst_full_name" id="val_cst_full_name" class="form-control" placeholder="Masukkan Nama Lengkap" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_email">Email</label>
                                        <input type="email" name="cst_email" id="val_cst_email" class="form-control" placeholder="Masukkan Email" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_phone">Nomor Telepon</label>
                                        <input type="text" name="cst_phone" id="val_cst_phone" class="form-control" placeholder="Masukkan Nomor Telepon" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_full_address">Alamat Lengkap</label>
                                        <textarea name="cst_full_address" id="val_cst_full_address" class="form-control" placeholder="Masukkan Alamat Lengkap" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_addr_state">Provinsi</label>
                                        <input type="text" name="cst_addr_state" id="val_cst_addr_state" class="form-control" placeholder="Masukkan Provinsi" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_addr_district">Kabupaten/Kota</label>
                                        <input type="text" name="cst_addr_district" id="val_cst_addr_district" class="form-control" placeholder="Masukkan Kabupaten/Kota" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_addr_village">Kecamatan/Kelurahan</label>
                                        <input type="text" name="cst_addr_village" id="val_cst_addr_village" class="form-control" placeholder="Masukkan Kecamatan/Kelurahan" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_pos_code">Kode Pos</label>
                                        <input type="text" name="cst_pos_code" id="val_cst_pos_code" class="form-control" placeholder="Masukkan Kode Pos" required>
                                    </div>
                                    <div class="row gx-3">
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Provinsi</label>
                                            <select class="form-control" id="val_cst_addr_state" name="cst_addr_state">
                                                <option value="">-- Pilih Provinsi --</option>
                                                <?php
                                                $daerah = mysqli_query($conn, "SELECT `kode`, `nama` FROM `wilayah` WHERE CHAR_LENGTH(kode)=2 ORDER BY `nama`");
                                                while ($d = mysqli_fetch_array($daerah)) {
                                                    if ($cust['cst_addr_state'] == $d['nama']) {
                                                        $cst_code_province = $d['kode'];
                                                        echo '<option value="' . $d['kode'] . '" selected>' . $d['nama'] . '</option>';
                                                    } else {
                                                        echo '<option value="' . $d['kode'] . '">' . $d['nama'] . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Kota/Kabupaten</label>
                                            <select class="form-control" id="form_city" name="addr_city">
                                                <option value="">-- Pilih Kota/Kab --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row gx-3">
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Kecamatan</label>
                                            <select class="form-control" id="form_district" name="addr_district">
                                                <option value="">-- Pilih Kecamatan --</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="small mb-1">Kelurahan</label>
                                            <select class="form-control" id="form_village" name="addr_village">
                                                <option value="">-- Pilih Kelurahan --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_3" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal Edit Data -->

                <!-- Start: Modal Add Data -->
                <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Tambah Pelanggan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="<?= $config['host']; ?>/api/admin/customer?method=CREATE" id="Add-Data-Form">
                                    <div class="form-group">
                                        <label for="cst_full_name">Nama Lengkap</label>
                                        <input type="text" name="cst_full_name" class="form-control" placeholder="Masukkan Nama Lengkap" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_email">Email</label>
                                        <input type="email" name="cst_email" class="form-control" placeholder="Masukkan Email" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_password">Password</label>
                                        <input type="password" name="cst_password" class="form-control" placeholder="Masukkan Password" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_phone">Nomor Telepon</label>
                                        <input type="text" name="cst_phone" class="form-control" placeholder="Masukkan Nomor Telepon" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_full_address">Alamat Lengkap</label>
                                        <textarea name="cst_full_address" class="form-control" placeholder="Masukkan Alamat Lengkap" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_addr_state">Provinsi</label>
                                        <input type="text" name="cst_addr_state" class="form-control" placeholder="Masukkan Provinsi" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_addr_district">Kabupaten/Kota</label>
                                        <input type="text" name="cst_addr_district" class="form-control" placeholder="Masukkan Kabupaten/Kota" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_addr_village">Kecamatan/Kelurahan</label>
                                        <input type="text" name="cst_addr_village" class="form-control" placeholder="Masukkan Kecamatan/Kelurahan" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cst_pos_code">Kode Pos</label>
                                        <input type="text" name="cst_pos_code" class="form-control" placeholder="Masukkan Kode Pos" required>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal Add Data -->

            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <!-- Custom JS -->
    <script type="text/javascript">
        $(document).ready(function() {

            // JS fetch list data
            var table = $('#List-Data').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?= $config['host']; ?>/api/admin/customer?method=READ_LIST",
                "columnDefs": [{
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<span class='text-nowrap'><button class='btn btn-info btn-sm tblEdit'><i class='fa fa-edit'></i></button> <button class='btn btn-danger btn-sm tblDelete'><i class='fa fa-trash'></i></button></span>"
                    },
                    {
                        "targets": [5, 6, 7],
                        "className": "d-none"
                    },
                    {
                        "targets": [1, 2, 3, 4, 5, 6, 7],
                        "className": "text-nowrap"
                    }
                ]
            });

            // JS create data
            $("form#Add-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalAdd').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS show data single -> show edit form
            $('#List-Data tbody').on('click', '.tblEdit', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/customer?method=READ_SINGLE",
                    data: {
                        id_customer: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#modalEdit').modal('show');
                        $('#val_id_data').val(result.data.id_customer);
                        $('#val_cst_full_name').val(result.data.cst_full_name);
                        $('#val_cst_email').val(result.data.cst_email);
                        $('#val_cst_phone').val(result.data.cst_phone);
                        $('#val_cst_full_address').val(result.data.cst_full_address);
                        $('#val_cst_addr_state').val(result.data.cst_addr_state);
                        $('#val_cst_addr_district').val(result.data.cst_addr_district);
                        $('#val_cst_addr_village').val(result.data.cst_addr_village);
                        $('#val_cst_pos_code').val(result.data.cst_pos_code);
                    }
                });
            });

            // Tampilkan Form Ubah Data ? KEMEM
            $('#List-Data tbody').on('click', '.tblView', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/v1/admin/getApiParamV2",
                    data: {
                        id: id_data
                    },
                    dataType: "HTML",
                    success: function(result) {
                        $('#modalView').modal('show');
                        $('#Api-Param').html(result);
                    }
                });
            });

            // JS update data
            $("form#Edit-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalEdit').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS delete data
            $('#List-Data tbody').on('click', '.tblDelete', function() {
                var data = table.row($(this).parents('tr')).data();
                //var nama_tools = data[1];
                var id_data = data[0];
                swal({
                        title: "Anda yakin?",
                        text: "Anda tidak bisa mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, hapus data!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/admin/customer?method=DELETE",
                                data: {
                                    id_customer: id_data
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    if (hasil.status) {
                                        swal("Berhasil", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Dibatalkan", "Data anda aman :)", "error");
                        }
                    })
            });

        });
    </script>
</body>

</html>