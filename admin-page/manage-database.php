<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Kelola Database';
$allow_position = array('DEVELOPER', 'PEMILIK', 'MANAGER');
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
} else if (in_array(strtoupper($_SESSION['emp_job_name']), $allow_position) == false) {
    header('Location:' . $config['host_admin']);
    exit();
}
// --
// --
if (isset($_POST['aksi'])) {
    if ($_POST['aksi'] == 'BACKUP') {
        $tables = array();
        $result = mysqli_query($conn, "SHOW TABLES");
        while ($row = mysqli_fetch_row($result)) {
            $tables[] = $row[0];
        }
        $return = '';
        foreach ($tables as $table) {
            $result         = mysqli_query($conn, "SELECT * FROM " . $table);
            $num_fields     = mysqli_num_fields($result);
            $return        .= 'DROP TABLE ' . $table . ';';
            $row2           = mysqli_fetch_row(mysqli_query($conn, 'SHOW CREATE TABLE ' . $table));
            $return        .= "\n\n" . $row2[1] . ";\n\n";
            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysqli_fetch_row($result)) {
                    $return .= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        if (isset($row[$j])) {
                            $return .= '"' . $row[$j] . '"';
                        } else {
                            $return .= '""';
                        }
                        if ($j < $num_fields - 1) {
                            $return .= ',';
                        }
                    }
                    $return .= ");\n";
                }
            }
            $return .= "\n\n\n";
        }
        $handle = fopen('./backup/pl-warteg-' . date('d-m-y') . '.sql', 'w+');
        fwrite($handle, $return);
        fclose($handle);
        $array['result']    = 1;
        $array['content']   = '<div class="alert alert-success">Succces to backup database</div>';
        print_r(json_encode($array));
        exit();
    } elseif ($_POST['aksi'] == 'OPTIMIZE') {
        $query = mysqli_query($conn, "OPTIMIZE TABLE");
        if ($query) {
            $array['result']    = 1;
            $array['content']   = '<div class="alert alert-success">Succces to optimize database</div>';
        } else {
            $array['result']    = 0;
            $array['content']   = '<div class="alert alert-danger">Failed to optimize database</div>';
        }
        print_r(json_encode($array));
        exit();
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include('../inc/admin-page/admin-header.phtml'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Kelola Database</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="result_submit"></div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header bg-primary">
                                    <h3 class="card-title"><i class="fas fa-wrench"></i> Optimize Database</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <form action="" method="POST" id="Optimize-Form">
                                        <input type="hidden" name="aksi" value="OPTIMIZE">
                                        <div class="form-group">
                                            <button class="btn btn-primary" id="button_id" type="submit">Optimize</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header bg-primary">
                                    <h3 class="card-title"><i class="fas fa-file-export"></i> Backup Database</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <form action="" method="POST" id="Backup-Form">
                                        <input type="hidden" name="aksi" value="BACKUP">
                                        <div class="form-group">
                                            <button class="btn btn-primary" id="button_id2" type="submit">Backup</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->

            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>
    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form#Optimize-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('Optimize');
                        if (hasil.result) {
                            $("#result_submit").html('' + hasil.content + '');
                            status();
                        } else
                            $("#result_submit").html('' + hasil.content + '');
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('Optimize');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            $("form#Backup-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id2").html('Backup');
                        if (hasil.result) {
                            $("#result_submit").html('' + hasil.content + '');
                            status();
                        } else
                            $("#result_submit").html('' + hasil.content + '');
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id2").html('Backup');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id2").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

        });
    </script>
</body>

</html>