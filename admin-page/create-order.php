<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Buat Pesanan';
$allow_position = array('DEVELOPER', 'PEMILIK', 'MANAGER', 'KASIR');
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
} else if (in_array(strtoupper($_SESSION['emp_job_name']), $allow_position) == false) {
    header('Location:' . $config['host_admin']);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
    <!-- Custom Admin CSS -->
    <link href="<?= $config['host']; ?>/assets/css/custom-admin.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php include('../inc/admin-page/admin-header.phtml'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Buat Pesanan Baru</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">

                        <!-- Select Menu Card -->
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fa fa-list"></i> Data Menu</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="maximize">
                                            <i class="fas fa-expand"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body" style="display: block;">
                                    <div class="row mb-4">
                                        <!-- Select Category Menu -->
                                        <div class="col-md-6 mb-2">
                                            <select name="menu_category" id="menu_category" class="form-control" required>
                                                <option value="ALL" id="val_menu_category">-- Semua Kategori --</option>
                                                <?php
                                                $query = mysqli_query($conn, "SELECT * FROM `wrtg_menu_category`");
                                                while ($row = mysqli_fetch_assoc($query)) {
                                                    echo '<option value="' . $row['id_category'] . '">(' . $row['id_category'] . ') ' . $row['category_name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <!-- Search Box -->
                                        <div class="col-md-6 mb-2">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Cari Menu" id="menu_search">
                                                <span class="input-group-append">
                                                    <button id="menu_search_btn" type="button" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>

                                    <div class="row menu-list-box pt-4" id="menuBoxList">

                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- Preview Pesanan Card -->
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fa fa-shopping-cart"></i> Keranjang</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="maximize">
                                            <i class="fas fa-expand"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body">

                                    <!-- Form Pesanan -->
                                    <div>
                                        <!-- <div class="form-group row">
                                            <label for="no_invoice" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">No Bon <span style="color: red;">*</span></label>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                <input type="text" class="form-control" id="no_invoice" placeholder="Nomor Invoice">
                                            </div>
                                        </div> -->
                                        <div class="form-group row">
                                            <label for="no_invoice" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Pelanggan</label>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                <select id="ch_cust" name="ch_cust" class="form-control select2bs4 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                    <option value="">-- Pelanggan --</option>
                                                    <option value="102">-- Order via Kasir --</option>
                                                    <?php
                                                    $query = mysqli_query($conn, "SELECT * FROM `wrtg_customer`");
                                                    while ($row = mysqli_fetch_assoc($query)) {
                                                        echo '<option value="' . $row['id_customer'] . '">(' . $row['cst_phone'] . ') ' . $row['cst_full_name'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="ch_name" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Atas Nama <span style="color: red;">*</span></label>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                <input type="text" class="form-control" id="ch_name" placeholder="Pesanan Atas Nama">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- List Cart -->
                                    <div class="div">
                                        <span><i class="fa fa-shopping-cart"></i> Daftar Keranjang</span>
                                        <hr>
                                    </div>
                                    <div>
                                        <table class="table-cart" id="DataCartMain">
                                            <thead>
                                                <tr class="text-center">
                                                    <th style="width: 10px;"><strong>No</strong></th>
                                                    <th><strong>Nama Menu</strong></th>
                                                    <th><strong>Qty</strong></th>
                                                    <th><strong>Harga</strong></th>
                                                    <th><strong>#</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody id="DataCart">

                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- Box HR -->
                                    <div>
                                        <hr>
                                    </div>

                                    <!-- Box Checkout -->
                                    <div>
                                        <div class="form-group row">
                                            <label for="ch_status" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Status <span style="color: red;">*</span></label>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                <select id="ch_status" name="ch_status" class="form-control">
                                                    <option value="">-- Pilih Status Awal --</option>
                                                    <option value="PENDING" selected>Pending</option>
                                                    <option value="PROCESS">Process</option>
                                                    <option value="COMPLETED">Completed</option>
                                                    <option value="CANCELED">Canceled</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="ch_type" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Tipe Pesanan <span style="color: red;">*</span></label>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                <select id="ch_type" name="ch_type" class="form-control">
                                                    <option value="">-- Pilih Tipe Pesanan --</option>
                                                    <option value="TAKEAWAY">Takeaway</option>
                                                    <option value="DELIVERY">Delivery</option>
                                                    <option value="DINEIN">Dine In</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="ch_subtotal" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Total Bayar</label>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Rp. </span>
                                                    </div>
                                                    <input id="ch_subtotal" name="ch_subtotal" class="form-control" type="text" placeholder="Total Bayar" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="ch_discount" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Diskon</label>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Rp. </span>
                                                    </div>
                                                    <input id="ch_discount" name="ch_discount" class="form-control" type="text" placeholder="Total Diskon" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="ch_tax" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Pajak</label>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Rp. </span>
                                                    </div>
                                                    <input id="ch_tax" name="ch_tax" class="form-control" type="text" placeholder="Total Pajak" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="ch_total" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Total</label>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Rp. </span>
                                                    </div>
                                                    <input id="ch_total" name="ch_total" class="form-control" type="text" placeholder="Total" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="div" id="PaymentChildForm">
                                            <div class="form-group row">
                                                <label for="ch_payment" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Bayar</label>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Rp. </span>
                                                        </div>
                                                        <input id="ch_payment" name="ch_payment" class="form-control" type="text" placeholder="Bayar">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ch_payback" class="col-lg-3 col-md-3 col-sm-3 col-4 col-form-label">Kembali</label>
                                                <div class="col-lg-9 col-md-9 col-sm-9 col-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Rp. </span>
                                                        </div>
                                                        <input id="ch_payback" name="ch_payback" class="form-control" type="text" placeholder="Kembali" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary" id="btn_order" type="submit"> Buat Pesanan</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <!-- Custom JS -->
    <script type="text/javascript">
        //Initialize Select2 Elements
        $('.select2').select2();

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });

        function menuSearch() {
            alert('OK!');
        }

        function loadMenuList(category, search) {
            let url = '<?= $config['host']; ?>/api/admin/create-order?method=READ_MENU';

            if (category && search) {
                // alert('DOUBLE OK!');
                url = '<?= $config['host']; ?>/api/admin/create-order?method=READ_MENU&category=' + category + '&search='.search;
            } else if (category) {
                // alert('SINGLE CATEGORY!');
                url = '<?= $config['host']; ?>/api/admin/create-order?method=READ_MENU&category=' + category;
            } else if (search) {
                // alert('SINGLE SEARCH!');
                url = '<?= $config['host']; ?>/api/admin/create-order?method=READ_MENU&search=' + search;
            }

            $.ajax({
                type: "POST",
                url: url,
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.data.length > 0) {
                        $.each(response.data, function(i, item) {
                            $('#menuBoxList').append(item.html_display);
                        });
                    } else {

                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                },
                beforeSend(a, b) {
                    $('#menuBoxList').html('');
                    $.LoadingOverlay('show');
                }
            });
        }

        function loadMenu(idMenu) {
            let url = '<?= $config['host']; ?>/api/admin/create-order?method=READ_MENU_SINGLE&id_menu=' + idMenu;
            $.ajax({
                type: "POST",
                url: url,
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {

                    } else {

                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function loadCart() {
            let url = '<?= $config['host']; ?>/api/admin/create-order?method=READ_CART';
            $.ajax({
                type: "POST",
                url: url,
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');

                    let subTotal = 0;
                    let discountAmount = 0;
                    let taxAmount = 0;
                    let totalAmount = 0;
                    $.each(response.data, function(i, val) {
                        htmlData = '<tr>' +
                            '<td class="text-nowrap text-center"><strong>' + (i + 1) + '</strong></td>' +
                            '<td class="text-nowrap">' + val.menu_name + '</td>' +
                            '<td class="text-nowrap text-center ">' + val.qty + '</td>' +
                            '<td class="text-nowrap text-right">' + formatRupiah(val.menu_price, 'Rp. ') + '</td>' +
                            '<td class="text-nowrap text-center"><button class="btn btn-danger btn-sm tblDelete" onclick="deleteCart(' + val.id_menu + ');"><i class="fa fa-trash"></i></button></td>' +
                            '</tr>'
                        $('#DataCart').append(htmlData);
                        subTotal += (val.qty * val.menu_price);
                    });

                    taxPercentage = 10;
                    taxAmount = subTotal * taxPercentage / 100;
                    totalAmount = (subTotal - discountAmount) + taxAmount;

                    $('#ch_subtotal').val(formatRupiah(subTotal.toString()));
                    $('#ch_discount').val(formatRupiah(discountAmount.toString()));
                    $('#ch_tax').val(formatRupiah(taxAmount.toString()));
                    $('#ch_total').val(formatRupiah(totalAmount.toString()));

                    if (response.status == 1) {} else {}
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                    $('#DataCart').html('');
                }
            });
        }

        function deleteCart(idMenu) {
            let url = '<?= $config['host']; ?>/api/admin/create-order?method=DELETE_CART&id_menu=' + idMenu;
            $.ajax({
                type: "POST",
                url: url,
                data: {},
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        loadCart();
                    } else {

                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                    swal("Error", response.content, "error");
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function checkoutNow() {
            let ord_customer = $('#ch_cust').val();
            let ord_name = $('#ch_name').val();
            let ord_status = $('#ch_status').val();
            let ord_type = $('#ch_type').val();
            let ord_pay = $('#ch_payment').val();


            let data = {
                ord_customer,
                ord_name,
                ord_status,
                ord_type,
                ord_pay
            };

            let url = '<?= $config['host']; ?>/api/admin/create-order?method=CREATE_ORDER';
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "JSON",
                success: function(response) {
                    $.LoadingOverlay('hide');
                    if (response.status == 1) {
                        loadCart();
                        $('#ch_cust').val('');
                        $('#ch_name').val('');
                        $('#ch_status').val('');
                        $('#ch_type').val('');
                        swal("Berhasil", response.content, "success");
                    } else {
                        swal("Error", response.content, "error");
                    }
                },
                error: function(a, b, c) {
                    $.LoadingOverlay('hide');
                },
                beforeSend(a, b) {
                    $.LoadingOverlay('show');
                }
            });
        }

        function parseIntRupiah(amount) {
            var amount = amount;
            amount = amount.replace(".", "");
            amount = parseInt(amount);

            return amount;
        }

        $(document).ready(function() {

            loadMenuList();
            loadCart();

            // Keyup
            $('#ch_payment').keyup(function() {
                payback = parseIntRupiah($('#ch_payment').val()) - parseIntRupiah($('#ch_total').val());
                console.log(payback);
                $('#ch_payment').val(formatRupiah(this.value));
                $('#ch_payback').val(formatRupiah(payback.toString()));
            });

            // Make order !!!
            $('#btn_order').click(function() {
                checkoutNow();
            });

            // Search menu by keyword
            $('#menu_search_btn').click(function() {
                let keyword = $('#menu_search').val();
                loadMenuList(null, keyword);
            });

            // Search menu by category
            $('#menu_category').change(function() {
                let category = $(this).val();
                if (category != 'ALL') {
                    loadMenuList($(this).val());
                } else {
                    loadMenuList();
                }
            });

            // Add to cart
            $('#menuBoxList').on('click', '.table-menu', function(e) {
                let idMenu = e.currentTarget.id;
                loadMenu(idMenu);
                loadCart();
            });

        });
    </script>
</body>

</html>