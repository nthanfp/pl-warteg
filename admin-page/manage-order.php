<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Kelola Pesanan';
$allow_position = array('DEVELOPER', 'PEMILIK', 'MANAGER', 'KASIR');
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
} else if (in_array(strtoupper($_SESSION['emp_job_name']), $allow_position) == false) {
    header('Location:' . $config['host_admin']);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php include('../inc/admin-page/admin-header.phtml'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Kelola Data Pesanan</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">

                    <?php
                    $total_order            = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `wrtg_order`"));
                    $total_order_pending    = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `wrtg_order` WHERE `order_status`='PENDING'"));
                    $total_order_process    = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `wrtg_order` WHERE `order_status`='PROCESS'"));
                    $total_order_completed  = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `wrtg_order` WHERE `order_status`='COMPLETED'"));
                    $total_order_canceled   = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `wrtg_order` WHERE `order_status`='CANCELED'"));

                    $percentage_pending     = round(($total_order_pending / $total_order) * 100);
                    $percentage_process     = round(($total_order_process / $total_order) * 100);
                    $percentage_completed   = round(($total_order_completed / $total_order) * 100);
                    $percentage_canceled    = round(($total_order_canceled / $total_order) * 100);
                    ?>

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box bg-success">
                                <span class="info-box-icon"><i class="fas fa-check-square"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Pesanan Selesai</span>
                                    <span class="info-box-number"><?= $total_order_completed; ?></span>
                                    <div class="progress">
                                        <div class="progress-bar" style="width:  <?= $percentage_completed; ?>%"></div>
                                    </div>
                                    <span class="progress-description">
                                        <?= $percentage_completed; ?>% Dari Total Order
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box bg-info">
                                <span class="info-box-icon"><i class="fas fa-random"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Pesanan Diproses</span>
                                    <span class="info-box-number"><?= $total_order_process; ?></span>
                                    <div class="progress">
                                        <div class="progress-bar" style="width:  <?= $percentage_process; ?>%"></div>
                                    </div>
                                    <span class="progress-description">
                                        <?= $percentage_process; ?>% Dari Total Order
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box bg-warning">
                                <span class="info-box-icon"><i class="fas fa-user-clock"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Pesanan Menunggu</span>
                                    <span class="info-box-number"><?= $total_order_pending; ?></span>
                                    <div class="progress">
                                        <div class="progress-bar" style="width: <?= $percentage_pending; ?>%"></div>
                                    </div>
                                    <span class="progress-description">
                                        <?= $percentage_pending; ?>% Dari Total Order
                                    </span>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box bg-danger">
                                <span class="info-box-icon"><i class="fas fa-window-close"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Pesanan Batal</span>
                                    <span class="info-box-number"><?= $total_order_canceled; ?></span>
                                    <div class="progress">
                                        <div class="progress-bar" style="width: <?= $percentage_pending; ?>%"></div>
                                    </div>
                                    <span class="progress-description">
                                        <?= $percentage_pending; ?>% Dari Total Order
                                    </span>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-database"></i> Data Pesanan</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <a class="btn btn-info" href="<?= $config['host_admin']; ?>/create-order" role="button"><i class="fas fa-cart-plus"></i> Buat Pesanan Baru</a>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="List-Data" class="display table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Waktu Pemesanan</th>
                                                    <th class="text-nowrap">Pelanggan</th>
                                                    <th class="text-nowrap">Atas Nama</th>
                                                    <th class="text-nowrap">Total</th>
                                                    <th class="text-nowrap">Tipe Pesanan</th>
                                                    <th class="text-nowrap">Status Pesanan</th>
                                                    <th class="text-nowrap">Status Pembayaran</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Waktu Pemesanan</th>
                                                    <th class="text-nowrap">Pelanggan</th>
                                                    <th class="text-nowrap">Atas Nama</th>
                                                    <th class="text-nowrap">Total</th>
                                                    <th class="text-nowrap">Tipe Pesanan</th>
                                                    <th class="text-nowrap">Status Pesanan</th>
                                                    <th class="text-nowrap">Status Pembayaran</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.container-fluid -->

                <!-- Start: Modal Edit Data -->
                <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Update Pesanan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="card card-primary collapsed-card">
                                    <div class="card-header">
                                        <h3 class="card-title">Update Status Pesanan</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body" style="display: none;">
                                        <form id="Edit-Data-Form" action="<?= $config['host']; ?>/api/admin/order?method=UPDATE_STATUS" method="POST">
                                            <div class="form-group">
                                                <label>ID Pesanan</label>
                                                <input class="form-control" id="val_id_order" name="id_order" placeholder="ID Pesanan" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Status Pesanan</label>
                                                <select name="status" class="form-control">
                                                    <option value="" id="val_status">-- Pilih Status --</option>
                                                    <option value="PENDING">Pending</option>
                                                    <option value="PROCESS">Process</option>
                                                    <option value="COMPLETED">Completed</option>
                                                    <option value="CANCELED">Canceled</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" id="button_id_3" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="card card-primary collapsed-card">
                                    <div class="card-header">
                                        <h3 class="card-title">Update Status Pembayaran</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body" style="display: none;">
                                        <form id="Edit-Data-Form" action="<?= $config['host']; ?>/api/admin/order?method=UPDATE_PAYMENT" method="POST">
                                            <div class="form-group">
                                                <label>ID Pesanan</label>
                                                <input class="form-control" id="val_id_order_2" name="id_order" placeholder="ID Pesanan" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Status Pembayaran</label>
                                                <select name="payment_status" class="form-control">
                                                    <option value="" id="val_payment_status">-- Pilih Status --</option>
                                                    <option value="Pending">Menunggu Pembayaran</option>
                                                    <option value="Verification">Dalam Verifikasi</option>
                                                    <option value="Confirmed">Pembayaran Terverifikasi</option>
                                                    <option value="Canceled">Dibatalkan</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" id="button_id_3" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal Edit Data -->

                <!-- Start: Modal View Data -->
                <div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="modalView" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Lihat Pesanan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="div table-responsive">
                                    <table class="table table-bordered">
                                        <tbody id="Order-Section-1">
                                            <tr>
                                                <th>No Order</th>
                                                <th width="5">:</th>
                                                <td id="x_id_order"> - </td>
                                            </tr>
                                            <tr>
                                                <th>Akun Pelanggan</th>
                                                <th width="5">:</th>
                                                <td id="x_id_customer"> - </td>
                                            </tr>
                                            <tr>
                                                <th>Atas Nama</th>
                                                <th width="5">:</th>
                                                <td id="x_order_name"> - </td>
                                            </tr>
                                            <tr>
                                                <th>Status Pesanan</th>
                                                <th width="5">:</th>
                                                <td id="x_order_status"> - </td>
                                            </tr>
                                            <tr>
                                                <th>Tipe Pesanan</th>
                                                <th width="5">:</th>
                                                <td id="x_order_type"> - </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Menu</th>
                                                <th>Qty</th>
                                                <th>Harga</th>
                                                <th>Sub Total</th>
                                            </tr>
                                        </thead>
                                        <tbody id="Data-Detail-Order">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="div table-responsive">
                                    <table class="table table-bordered">
                                        <tbody id="Order-Section-1">
                                            <tr>
                                                <th>Sub Total</th>
                                                <th width="5">:</th>
                                                <td id="y_subtotal"> - </td>
                                            </tr>
                                            <tr>
                                                <th>Diskon</th>
                                                <th width="5">:</th>
                                                <td id="y_discount"> - </td>
                                            </tr>
                                            <tr>
                                                <th>Pajak</th>
                                                <th width="5">:</th>
                                                <td id="y_tax"> - </td>
                                            </tr>
                                            <tr>
                                                <th>Total Pesanan</th>
                                                <th width="5">:</th>
                                                <td id="y_total"> - </td>
                                            </tr>
                                            <tr>
                                                <th>Jumlah Pembayaran</th>
                                                <th width="5">:</th>
                                                <td id="y_payment"> - </td>
                                            </tr>
                                            <tr>
                                                <th>Kembalian (Jika Ada)</th>
                                                <th width="5">:</th>
                                                <td id="y_payback"> - </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal View Data -->

            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <!-- Custom JS -->
    <script type="text/javascript">
        $(document).ready(function() {

            // JS fetch list data
            var table = $('#List-Data').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?= $config['host']; ?>/api/admin/order?method=READ_LIST",
                "order": [
                    [0, 'desc'],
                    [1, 'desc']
                ],
                "columnDefs": [{
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<span class='text-nowrap'><button class='btn btn-warning btn-sm tblEdit'><i class='fa fa-edit'></i></button> <button class='btn btn-success btn-sm tblView'><i class='fa fa-eye'></i></button></span>"
                    },
                    {
                        "targets": 0,
                        // "className": "d-none"
                    },
                    {
                        "targets": [1, 2, 3, 4, 5, 6],
                        "className": "text-nowrap"
                    }
                ]
            });

            // JS create data
            $("form#Add-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalAdd').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Simpan');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS show data single -> show view form
            $('#List-Data tbody').on('click', '.tblView', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];

                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/order?method=READ_SINGLE",
                    data: {
                        id_order: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#modalView').modal('show');
                        $('#x_id_order').html('#' + result.data.id_order);
                        $('#x_id_customer').html(result.data.order_account);
                        $('#x_order_name').html(result.data.order_name);
                        $('#x_order_type').html(result.data.order_type);
                        $('#x_order_status').html(result.data.order_status);
                        $('#y_subtotal').html(formatRupiah(result.data.order_subtotal.toString(), 'Rp. '));
                        $('#y_discount').html(formatRupiah(result.data.order_discount.toString(), 'Rp. '));
                        $('#y_tax').html(formatRupiah(result.data.order_tax.toString(), 'Rp. '));
                        $('#y_total').html(formatRupiah(result.data.order_total.toString(), 'Rp. '));
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/order?method=READ_CHILD",
                    data: {
                        id_order: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $.each(result.data, function(i, val) {
                            htmlData = '<tr>' +
                                '<td class="text-nowrap text-center" width="8"><strong>' + (i + 1) + '</strong></td>' +
                                '<td class="text-nowrap">' + val.menu_name + '</td>' +
                                '<td class="text-nowrap text-center ">' + val.qty + '</td>' +
                                '<td class="text-nowrap text-left">' + formatRupiah(val.menu_price.toString(), 'Rp. ') + '</td>' +
                                '<td class="text-nowrap text-left">' + formatRupiah((val.menu_price * val.qty).toString(), 'Rp. ') + '</td>' +
                                '</tr>'
                            $('#Data-Detail-Order').append(htmlData);
                        });
                    },
                    beforeSend: function() {
                        $('#Data-Detail-Order').html('');
                    }
                });
            });

            // JS show data single -> show edit form
            $('#List-Data tbody').on('click', '.tblEdit', function() {

                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];

                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/order?method=READ_SINGLE",
                    data: {
                        id_order: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#modalEdit').modal('show');
                        $('#val_id_order').val(result.data.id_order);
                        $('#val_status').val(result.data.order_status);
                        $('#val_status').html('-- Pilih Status (' + result.data.order_status + ') --');
                        $('#val_id_order_2').val(result.data.id_order);
                        $('#val_payment_status').val(result.data.order_payment_status);
                        $('#val_payment_status').html('-- Pilih Status (' + result.data.order_payment_status + ') --');
                    }
                });
            });

            // JS update data
            $("form#Edit-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalEdit').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

        });
    </script>
</body>

</html>