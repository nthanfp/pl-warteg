<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Insights';
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
}

$id_emp     = $_SESSION['emp_id'];
$employee   = mysqli_query($conn, "SELECT *
FROM `wrtg_employee` `w`
INNER JOIN `wrtg_job` `j`
    ON (`w`.`emp_job_id`=`j`.`id_job`)
WHERE `id_employee`='$id_emp'");
$emp = mysqli_fetch_array($employee);

function toJsArr($arr)
{
    $arr = array_reverse($arr);
    $str = "[";
    for ($i = 0; $i < count($arr); $i++) {
        $str .= '"' . $arr[$i] . '", ';
    }
    $str .= "]";

    return $str;
}

print_r($dtChart);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include('../inc/admin-page/admin-header.phtml'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">

                        <div class="col-lg-6">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-chart-line"></i> Menu Populer</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="10">#</th>
                                                    <th>Menu</th>
                                                    <th>Terjual</th>
                                                </tr>
                                            <tbody>
                                                <?php
                                                $i = 0;
                                                $query = "SELECT `menu_name`, SUM(qty) AS `sold`
                                                FROM `wrtg_order_detail` `d`
                                                JOIN `wrtg_menu` `m`
                                                    USING(`id_menu`)
                                                GROUP BY `menu_name`
                                                ORDER BY `sold` DESC
                                                LIMIT 5";
                                                $query = mysqli_query($conn, $query);
                                                while ($row = mysqli_fetch_assoc($query)) {
                                                    $i++;
                                                ?>
                                                    <tr>
                                                        <td width="10"><?= $i; ?></td>
                                                        <td><?= $row['menu_name']; ?></td>
                                                        <td><?= $row['sold']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-chart-line"></i> Top Konsumen</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="10">#</th>
                                                    <th>Nama</th>
                                                    <th>Jumlah Pesanan</th>
                                                    <th>Total Nominal</th>
                                                </tr>
                                            <tbody>
                                                <?php
                                                $i = 0;
                                                $query = "SELECT `cst_full_name`, `cst_phone`, COUNT(id_order) AS `order_count`, SUM(`order_total`) AS `order_sum`
                                                FROM `wrtg_order` `o`
                                                JOIN `wrtg_customer` `c`
                                                    USING(`id_customer`)
                                                GROUP BY `id_customer`
                                                ORDER BY `order_count` DESC
                                                LIMIT 5";
                                                $query = mysqli_query($conn, $query);
                                                while ($row = mysqli_fetch_assoc($query)) {
                                                    $i++;
                                                ?>
                                                    <tr>
                                                        <td width="10"><?= $i; ?></td>
                                                        <td><?= $row['cst_full_name']; ?></td>
                                                        <td><?= $row['order_count']; ?></td>
                                                        <td><?= rupiah($row['order_sum']); ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-chart-line"></i> Grafik Transaksi</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="chart">
                                        <canvas id="chart_1"></canvas>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-chart-line"></i> Grafik Pelanggan Baru</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="chart">
                                        <canvas id="chart_2"></canvas>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                        </div>

                        <div class="col-lg-12">
                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- Chart -->
    <?php
    $dtChart['labels']      = array();
    $dtChart['datasets']    = array();
    $dtChart['labels'][0]   = date('d M Y');
    $dtChart['datasets'][0] = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(wrtg_order.order_time, '%d %M %Y') AS `date_format` FROM `wrtg_order` WHERE FROM_UNIXTIME(`wrtg_order`.`order_time`, '%d %M %Y')='" . $dtChart['labels'][0] . "'"));
    for ($i = 1; $i < 7; $i++) {
        array_push($dtChart['labels'], date("d M Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y"))));
        array_push($dtChart['datasets'], mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(wrtg_order.order_time, '%d %M %Y') AS `date_format` FROM `wrtg_order` WHERE FROM_UNIXTIME(`wrtg_order`.`order_time`, '%d %M %Y')='" . $dtChart['labels'][$i] . "'")));
    }

    $dtChart2['labels']      = array();
    $dtChart2['datasets']    = array();
    $dtChart2['labels'][0]   = date('d M Y');
    $dtChart2['datasets'][0] = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(`wrtg_customer`.`cst_register_date`, '%d %M %Y') AS `date_format` FROM `wrtg_customer` WHERE FROM_UNIXTIME(`wrtg_customer`.`cst_register_date`, '%d %M %Y')='" . $dtChart2['labels'][0] . "'"));
    for ($i = 1; $i < 7; $i++) {
        array_push($dtChart2['labels'], date("d M Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y"))));
        array_push($dtChart2['datasets'], mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(`wrtg_customer`.`cst_register_date`, '%d %M %Y') AS `date_format` FROM `wrtg_customer` WHERE FROM_UNIXTIME(`wrtg_customer`.`cst_register_date`, '%d %M %Y')='" . $dtChart['labels'][$i] . "'")));
    }
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var chartData = {
                labels: <?= toJsArr($dtChart['labels']); ?>,
                datasets: [{
                    data: <?= toJsArr($dtChart['datasets']); ?>,
                    borderColor: '#36A2EB',
                    backgroundColor: '#9BD0F5',
                }]
            };

            var chartData2 = {
                labels: <?= toJsArr($dtChart2['labels']); ?>,
                datasets: [{
                    data: <?= toJsArr($dtChart2['datasets']); ?>,
                    borderColor: '#36A2EB',
                    backgroundColor: '#9BD0F5',
                }]
            };

            var chart_1 = document.getElementById("chart_1");
            if (chart_1) {
                new Chart(chart_1, {
                    type: 'line',
                    data: chartData,
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        legend: {
                            display: false
                        }
                    }
                });
            }

            var chart_2 = document.getElementById("chart_2");
            if (chart_2) {
                new Chart(chart_2, {
                    type: 'line',
                    data: chartData2,
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        legend: {
                            display: false
                        }
                    }
                });
            }
        });
    </script>
</body>

</html>