<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Beranda';
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
}

$id_emp     = $_SESSION['emp_id'];
$employee   = mysqli_query($conn, "SELECT *
FROM `wrtg_employee` `w`
INNER JOIN `wrtg_job` `j`
    ON (`w`.`emp_job_id`=`j`.`id_job`)
WHERE `id_employee`='$id_emp'");
$emp = mysqli_fetch_array($employee);

$jam = date('H');
if ($jam >= '05' && $jam <= '10') {
    $ucapan = 'Selamat pagi, Semangat buat hari ini!';
} else if ($jam >= '11' && $jam <= '14') {
    $ucapan = 'Selamat siang, Jangan lelah untuk melayani pelanggan!';
} else if ($jam >= '15' && $jam <= '17') {
    $ucapan = 'Selamat sore, Jangan lupa minum air putih dan beristirahat sejenak!';
} else {
    $ucapan = 'Selamat malam, Selamat beristirahat, You\'re best!';
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include('../inc/admin-page/admin-header.phtml'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <?php
            $total_cust     = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `wrtg_customer`"));
            $total_menu     = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `wrtg_menu`"));
            $total_order    = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `wrtg_order`"));
            $total_material = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `wrtg_materials`"));
            ?>
            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="callout callout-info">
                                <h5>Halo, <?= $emp['emp_full_name']; ?> <i>(<?= $emp['job_name']; ?>)</i></h5>
                                <p> <?= $ucapan; ?></p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-teal">
                                <div class="inner">
                                    <h3><?= $total_order; ?></h3>
                                    <p>Jumlah Pesanan</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-shopping-basket"></i>
                                </div>
                                <a href="<?= $config['host_admin']; ?>/manage-order" class="small-box-footer">Lihat Detail <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3><?= $total_cust; ?></h3>
                                    <p>Jumlah Pelanggan</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                <a href="<?= $config['host_admin']; ?>/manage-customer" class="small-box-footer">Lihat Detail <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3><?= $total_menu; ?></h3>
                                    <p>Jumlah Menu</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-clipboard-list"></i>
                                </div>
                                <a href="<?= $config['host_admin']; ?>/manage-menu" class="small-box-footer">Lihat Detail <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3><?= $total_material; ?></h3>
                                    <p>Jumlah Bahan Baku</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-pallet"></i>
                                </div>
                                <a href="<?= $config['host_admin']; ?>/manage-materials" class="small-box-footer">Lihat Detail <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-primary collapsed-card">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-chart-line"></i> Grafik Transaksi</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                        </button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <?php
                                    $today          = date('d-m-Y');
                                    $harimundur1    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));
                                    $harimundur2    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y")));
                                    $harimundur3    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d") - 3, date("Y")));
                                    $harimundur4    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d") - 4, date("Y")));
                                    $harimundur5    = date("d-m-Y", mktime(0, 0, 0, date("m"), date("d") - 5, date("Y")));
                                    $hitungtoday    = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y') AS `date_format` FROM `wrtg_order` WHERE FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y')='$today'"));
                                    $hitungmundur1  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y') AS `date_format` FROM `wrtg_order` WHERE FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y')='$harimundur1'"));
                                    $hitungmundur2  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y') AS `date_format` FROM `wrtg_order` WHERE FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y')='$harimundur2'"));
                                    $hitungmundur3  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y') AS `date_format` FROM `wrtg_order` WHERE FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y')='$harimundur3'"));
                                    $hitungmundur4  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y') AS `date_format` FROM `wrtg_order` WHERE FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y')='$harimundur4'"));
                                    $hitungmundur5  = mysqli_num_rows(mysqli_query($conn, "SELECT FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y') AS `date_format` FROM `wrtg_order` WHERE FROM_UNIXTIME(wrtg_order.order_time, '%d-%m-%Y')='$harimundur5'"));
                                    ?>
                                    <div class="chart">
                                        <canvas id="chLine"></canvas>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>
    <script type="text/javascript">
        var chartData = {
            labels: ["<?= $harimundur5; ?>", "<?= $harimundur4; ?>", "<?= $harimundur3; ?>", "<?= $harimundur2; ?>", "<?= $harimundur1; ?>", "<?= $today; ?>"],
            datasets: [{
                data: [<?= $hitungmundur5; ?>, <?= $hitungmundur4; ?>, <?= $hitungmundur3; ?>, <?= $hitungmundur2; ?>, <?= $hitungmundur1; ?>, <?= $hitungtoday; ?>],
                borderColor: '#36A2EB',
                backgroundColor: '#9BD0F5',
            }]
        };

        var chLine = document.getElementById("chLine");
        if (chLine) {
            new Chart(chLine, {
                type: 'line',
                data: chartData,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: false
                            }
                        }]
                    },
                    legend: {
                        display: false
                    }
                }
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form#Edit-Config-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.result) {
                            $("#result_submit").html('<div class="alert alert-success">' + hasil.content + '</div>');
                            status();
                        } else
                            $("#result_submit").html('<div class="alert alert-danger">' + hasil.content + '</div>');
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form#Edit-ReChapta-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                        if (hasil.result) {
                            $("#result_submit_2").html('<div class="alert alert-success">' + hasil.content + '</div>');
                            status();
                        } else
                            $("#result_submit_2").html('<div class="alert alert-danger">' + hasil.content + '</div>');
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                        $("#result_submit_2").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id_2").html('Loading..');
                        $("#result_submit_2").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });
        });
    </script>
</body>

</html>