<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['name'] . ' - Kelola Variasi';
$allow_position = array('DEVELOPER', 'PEMILIK', 'MANAGER', 'KOKI');
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
} else if (in_array(strtoupper($_SESSION['emp_job_name']), $allow_position) == false) {
    header('Location:' . $config['host_admin']);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php include('../inc/admin-page/admin-header.phtml'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Kelola Data Grup Varian & Master Data Varian</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Daftar Data Grup Varian</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <a class="btn btn-info" id="btnTambah" href="#" data-toggle="modal" data-target="#modalAdd" role="button"><i class="fas fa-plus"></i> Tambah Grup Varian Baru</a>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="List-Data" class="display table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Nama Grup Varian</th>
                                                    <th class="text-nowrap">Deskripsi</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Nama Grup Varian</th>
                                                    <th class="text-nowrap">Deskripsi</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.container-fluid -->

                <!-- Start: Modal Edit Data -->
                <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit User</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="<?= $config['host']; ?>/api/admin/variant-group?method=UPDATE" id="Edit-Data-Form">
                                    <input type="hidden" id="val_id_data" name="id_data">
                                    <div class="form-group">
                                        <label for="group_name">Nama Grup</label>
                                        <input type="text" class="form-control" id="val_group_name" name="group_name" placeholder="Masukkan Nama Grup">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Deskripsi</label>
                                        <textarea class="form-control" id="val_description" name="description" placeholder="Masukkan Deskripsi"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_3" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal Edit Data -->

                <!-- Start: Modal Add Data -->
                <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Tambah Grup Varian</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="<?= $config['host']; ?>/api/admin/variant-group?method=CREATE" id="Add-Data-Form">
                                    <div class="form-group">
                                        <label for="group_name">Nama Grup</label>
                                        <input type="text" class="form-control" id="group_name" name="group_name" placeholder="Masukkan Nama Grup">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Deskripsi</label>
                                        <textarea class="form-control" id="description" name="description" placeholder="Masukkan Deskripsi"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal Add Data -->

                <!-- Start: Modal View Child Data -->
                <div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="modalView" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Data Varian</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="table-responsive">
                                    <table class="table borderless">
                                        <tbody>
                                            <form method="POST" action="<?= $config['host']; ?>/api/admin/variant?method=CREATE" id="Add-Data-Child-Form">
                                                <tr>
                                                    <input type="hidden" id="val_id_variant_group" name="id_variant_group" value="">
                                                    <td class="text-nowrap">
                                                        <label for="variant_name">Nama Varian</label>
                                                        <input type="text" class="form-control" name="variant_name" placeholder="Nama Varian" required>
                                                    </td>
                                                    <td class="text-nowrap">
                                                        <label for="variant_name">Deskripsi Varian</label>
                                                        <input type="text" class="form-control" name="variant_description" placeholder="Deskripsi Varian" required>
                                                    </td>
                                                    <td class="text-nowrap">
                                                        <label for="variant_name">Harga Varian</label>
                                                        <input type="number" min="0" class="form-control" name="variant_price" placeholder="Harga Varian" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-nowrap" colspan="3">
                                                        <button class="btn btn-primary" id="button_id_4" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                                        <button class="btn btn-danger" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                                    </td>
                                                </tr>
                                            </form>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered" id="editableTable">
                                        <thead>
                                            <tr>
                                                <th class="text-nowrap">Aksi</th>
                                                <th class="text-nowrap">ID Varian</th>
                                                <th class="text-nowrap d-none">ID Grup</th>
                                                <th class="text-nowrap">Nama Varian</th>
                                                <th class="text-nowrap">Deskripsi Varian</th>
                                                <th class="text-nowrap">Harga Varian</th>
                                            </tr>
                                        </thead>
                                        <tbody id="table-data">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal View Child Data -->

            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <script type="text/javascript" src="../assets/js/bootstable.js"></script>


    <!-- Custom JS -->
    <script type="text/javascript">
        $(document).ready(function() {

            // JS fetch list data
            var table = $('#List-Data').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?= $config['host']; ?>/api/admin/variant-group?method=READ_LIST",
                "columnDefs": [{
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<span class='text-nowrap'><button class='btn btn-success btn-sm tblView'><i class='fa fa-eye'></i></button> <button class='btn btn-info btn-sm tblEdit'><i class='fa fa-edit'></i></button> <button class='btn btn-danger btn-sm tblDelete'><i class='fa fa-trash'></i></button></span>"
                    },
                    {
                        "targets": 0,
                        "className": "d-none"
                    },
                    {
                        "targets": [1, 2, 3],
                        "className": "text-nowrap"
                    }
                ]
            });

            // JS create data
            $("form#Add-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalAdd').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Simpan');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS show data single -> show edit form
            $('#List-Data tbody').on('click', '.tblEdit', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/variant-group?method=READ_SINGLE",
                    data: {
                        id_variant_group: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        console.log('OK!')
                        $('#modalEdit').modal('show');
                        $('#val_id_data').val(result.data.id_variant_group);
                        $('#val_group_name').val(result.data.group_name);
                        $('#val_description').val(result.data.description);
                    }
                });
            });

            // JS update data
            $("form#Edit-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalEdit').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS delete data
            $('#List-Data tbody').on('click', '.tblDelete', function() {
                var data = table.row($(this).parents('tr')).data();
                //var nama_tools = data[1];
                var id_data = data[0];
                swal({
                        title: "Anda yakin?",
                        text: "Anda tidak bisa mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, hapus data!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/admin/variant-group?method=DELETE",
                                data: {
                                    id_variant_group: id_data
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    if (hasil.status) {
                                        swal("Berhasil", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Dibatalkan", "Data anda aman :)", "error");
                        }
                    })
            });

            // JS (CHILD) create data
            $("form#Add-Data-Child-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                var id_data = $("#val_id_variant_group").val();
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_4").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            table.ajax.reload(null, false);
                            console.log(id_data);
                            loadChildData(id_data);
                            swal("Success!", "" + hasil.content + "", "success");
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_4").html('<i class="fa fa-save"></i> Simpan');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id_4").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS (CHILD) update data
            $(document).on("click", "#editableTable button.edtUpdate", function() {
                let tr = $(this).closest('tr');
                let id_variant = tr.find('td:eq(1) input').val();
                let id_variant_group = tr.find('td:eq(2) input').val();
                let variant_name = tr.find('td:eq(3) input').val();
                let variant_description = tr.find('td:eq(4) input').val();
                let variant_price = tr.find('td:eq(5) input').val();
                // alert('Table 1: ' + id_variant + '\n' + variant_name + '\n' + variant_description + '\n' + variant_price);
                $.ajax({
                    url: "<?= $config['host']; ?>/api/admin/variant?method=UPDATE",
                    data: {
                        id_data: id_variant,
                        id_variant_group,
                        variant_name,
                        variant_description,
                        variant_price
                    },
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        loadChildData(id_variant_group);
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                        } else {
                            swal("Failed!", "" + hasil.content + "", "error");
                        }
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("button").attr("disabled", "disabled");
                    }
                });
            });

            // JS (CHILD) delete data
            $(document).on("click", "#editableTable button.edtDelete", function() {
                let tr = $(this).closest('tr');
                let id_variant = tr.find('td:eq(1) input').val();
                let id_variant_group = tr.find('td:eq(2) input').val();

                swal({
                        title: "Anda yakin?",
                        text: "Anda tidak bisa mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, hapus data!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/admin/variant?method=DELETE",
                                data: {
                                    id_variant: id_variant
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    if (hasil.status) {
                                        swal("Berhasil", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                        loadChildData(id_variant_group);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Dibatalkan", "Data anda aman :)", "error");
                        }
                    })
            });

            // JS (CHILD) (function show data)
            function loadChildData(id_data) {
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/variant-group?method=READ_CHILD",
                    data: {
                        id_variant_group: id_data
                    },
                    dataType: "JSON",
                    success: function(response) {
                        $('#modalView').modal('show');
                        $('#val_id_variant_group').val(id_data);
                        if (response.data.length > 0) {
                            $.each(response.data, function(i, item) {
                                $('#table-data').append(
                                    '<tr>' +
                                    '<td class="text-nowrap"><span class="text-nowrap"><button class="btn btn-primary btn-sm edtUpdate"><i class="fa fa-save"></i></button> <button class="btn btn-danger btn-sm edtDelete"><i class="fa fa-trash"></i></button></span></td>' +
                                    '<td class="text-nowrap w-75"><input type="text" class="form-control w-75" name="val_id_variant" value="' + item.id_variant + '" disabled></td>' +
                                    '<td class="text-nowrap d-none"><input type="text" class="form-control w-auto" name="val_id_variant" value="' + item.id_variant_group + '" disabled></td>' +
                                    '<td class="text-nowrap"><input type="text" class="form-control w-auto" name="val_variant_name" value="' + item.variant_name + '"></td>' +
                                    '<td class="text-nowrap"><input type="text" class="form-control w-auto" name="val_variant_description" value="' + item.variant_description + '"></td>' +
                                    '<td class="text-nowrap"><input type="text" class="form-control" name=" val_variant_price" value="' + item.variant_price + '"></td>' +
                                    '</tr>'
                                );
                            });
                        } else {
                            $('#table-data').append(
                                '<tr>' +
                                '<td class="text-nowrap text-center" colspan="5">Belum ada data varian</td>' +
                                '</tr>'
                            );
                        }
                    },
                    error: function(a, b, c) {
                        console.log('NOT OK!')
                        swal("Error", "" + hasil.content + "", "error");
                    },
                    beforeSend(a, b) {
                        $('#table-data').html('');
                    }
                });
            }

            // JS (CHILD) show data list
            $('#List-Data tbody').on('click', '.tblView', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                loadChildData(id_data);
            });

        });
    </script>
</body>

</html>