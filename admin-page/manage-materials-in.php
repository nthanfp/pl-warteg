<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['name'] . ' - Catat Bahan Baku Masuk';
$allow_position = array('DEVELOPER', 'PEMILIK', 'MANAGER', 'KOKI', 'PELAYAN');
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
} else if (in_array(strtoupper($_SESSION['emp_job_name']), $allow_position) == false) {
    header('Location:' . $config['host_admin']);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php include('../inc/admin-page/admin-header.phtml'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Kelola Data Masuk Bahan Baku</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">

                        <!-- Form Add -->
                        <div class="col-lg-4">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-plus"></i> Tambah Data Masuk Bahan Baku</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <form method="POST" action="<?= $config['host']; ?>/api/admin/materials-log?method=CREATE_IN" id="Add-Data-Form">

                                        <div class="form-group">
                                            <label for="id_material">Nama Bahan Baku <span style="color: red;">*</span></label>
                                            <select name="id_material" id="id_material" class="form-control select2bs4 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                                <option value="" unit="{SATUAN}">-- Pilih Bahan Baku --</option>
                                                <?php
                                                $query = mysqli_query($conn, "SELECT * FROM `wrtg_materials` ORDER BY `material_name` ASC");
                                                while ($row = mysqli_fetch_assoc($query)) {
                                                    echo '<option value="' . $row['id_material'] . '" unit="' . $row['material_unit'] . '">(' . $row['id_material'] . ') ' . $row['material_name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="log_qty">Kuantitas/Banyaknya <span style="color: red;">*</span></label>
                                            <div class="input-group mb-3">
                                                <input type="text" name="log_qty" class="form-control" placeholder="Kuantitas">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="log_unit">{SATUAN}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="log_price">Harga Beli <span style="color: red;">*</span></label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Rp. </span>
                                                </div>
                                                <input id="log_price" name="log_price" class="form-control" type="text" placeholder="Harga Beli">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="log_notes">Catatan</label>
                                            <textarea class="form-control" name="log_notes" placeholder="Catatan"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                            <button class="btn btn-danger" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                        </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>

                        <!-- Table Data -->
                        <div class="col-lg-8">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-database"></i> Daftar Data Bahan Baku Masuk</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <table id="List-Data" class="display table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Waktu Dibuat</th>
                                                    <th class="text-nowrap">Nama Bahan</th>
                                                    <th class="text-nowrap">Satuan</th>
                                                    <th class="text-nowrap">Banyaknya</th>
                                                    <th class="text-nowrap">Total Harga</th>
                                                    <th>Catatan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Waktu Dibuat</th>
                                                    <th class="text-nowrap">Nama Bahan</th>
                                                    <th class="text-nowrap">Satuan</th>
                                                    <th class="text-nowrap">Banyaknya</th>
                                                    <th class="text-nowrap">Total Harga</th>
                                                    <th>Catatan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- /.row -->

                    </div>
                </div>
                <!-- /.container-fluid -->


            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <!-- Custom JS -->
    <script type="text/javascript">
        //Initialize Select2 Elements
        $('.select2').select2();

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });

        $(document).ready(function() {

            $('#id_material').change(function() {
                var unit = $('#id_material option:selected').attr('unit');
                $('#log_unit').html(unit);
            });

            $('#log_price').keyup(function() {
                $('#log_price').val(formatRupiah(this.value));
            });

            // JS fetch list data
            var table = $('#List-Data').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?= $config['host']; ?>/api/admin/materials-log?method=READ_LIST_IN",
                "order": [
                    [1, 'desc'],
                    [0, 'desc']
                ],
                "columnDefs": [{
                        "targets": -1,
                        "data": null,
                        "defaultContent": "-"
                    },
                    {
                        "targets": [3],
                        "className": "d-none"
                    },
                    {
                        "targets": [1, 2, 3, 4, 6],
                        "className": "text-nowrap"
                    }
                ]
            });

            // JS create data
            $("form#Add-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalAdd').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Simpan');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS show data single -> show edit form
            $('#List-Data tbody').on('click', '.tblEdit', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/materials?method=READ_SINGLE",
                    data: {
                        id_material: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#modalEdit').modal('show');
                        $('#val_id_data').val(result.data.id_material);
                        $('#val_material_name').val(result.data.material_name);
                        $('#val_material_unit').val(result.data.material_unit);
                        $('#val_material_stock').val(result.data.material_stock);
                    }
                });
            });

            // JS update data
            $("form#Edit-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalEdit').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS delete data
            $('#List-Data tbody').on('click', '.tblDelete', function() {
                var data = table.row($(this).parents('tr')).data();
                //var nama_tools = data[1];
                var id_data = data[0];
                swal({
                        title: "Anda yakin?",
                        text: "Anda tidak bisa mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, hapus data!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/admin/materials?method=DELETE",
                                data: {
                                    id_material: id_data
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    if (hasil.status) {
                                        swal("Berhasil", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Dibatalkan", "Data anda aman :)", "error");
                        }
                    })
            });

        });
    </script>
</body>

</html>