<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Kelola Kupon';
$allow_position = array('DEVELOPER', 'PEMILIK', 'MANAGER', 'KOKI');
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
} else if (in_array(strtoupper($_SESSION['emp_job_name']), $allow_position) == false) {
    header('Location:' . $config['host_admin']);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php include('../inc/admin-page/admin-header.phtml'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Kelola Data Kupon</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-database"></i> Data Kupon</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <a class="btn btn-info" id="btnTambah" href="#" data-toggle="modal" data-target="#modalAdd" role="button"><i class="fas fa-plus"></i> Tambah Kupon Baru</a>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="List-Data" class="display table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Gambar</th>
                                                    <th class="text-nowrap">Nama Kupon</th>
                                                    <th class="text-nowrap">Kode Kupon</th>
                                                    <th class="text-nowrap">Deskripsi</th>
                                                    <th class="text-nowrap">Tipe</th>
                                                    <th class="text-nowrap">Nominal</th>
                                                    <th class="text-nowrap">Max Nominal</th>
                                                    <th class="text-nowrap">Min Pesan</th>
                                                    <th class="text-nowrap">Max Penggunaan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Gambar</th>
                                                    <th class="text-nowrap">Nama Kupon</th>
                                                    <th class="text-nowrap">Kode Kupon</th>
                                                    <th class="text-nowrap">Deskripsi</th>
                                                    <th class="text-nowrap">Tipe</th>
                                                    <th class="text-nowrap">Nominal</th>
                                                    <th class="text-nowrap">Max Nominal</th>
                                                    <th class="text-nowrap">Min Pesan</th>
                                                    <th class="text-nowrap">Max Penggunaan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.container-fluid -->

                <!-- Start: Modal Edit Data -->
                <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Kupon</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <!-- Form Upload Gambar: Edit -->
                                <form id="form-upload-2" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Pilih Gambar</label>
                                        <input type="file" class="form-control-file" name="image" id="file_gambar_2">
                                    </div>
                                </form>
                                <!-- End Form Upload Gambar: Edit -->

                                <form method="POST" action="<?= $config['host']; ?>/api/admin/voucher?method=UPDATE" id="Edit-Data-Form">
                                    <input type="hidden" id="val_id_data" name="id_data">
                                    <!-- Gambar -->
                                    <div id="image_voc_box_edit">
                                        <div class="form-group">
                                            <label>Gambar</label><br />
                                            <img id="image_voc_thumb_edit" class="img-fluid" alt="Responsive image">
                                        </div>
                                        <input type="hidden" id="edit_voc_image" name="voc_image" value="">
                                    </div>
                                    <!-- End Gambar -->
                                    <div class="form-group">
                                        <label for="val_voc_name">Nama Kupon <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" name="voc_name" placeholder="Nama Kupon" id="val_voc_name">
                                    </div>
                                    <div class="form-group">
                                        <label for="val_voc_code">Kode Kupon <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" name="voc_code" placeholder="Kode Kupon" id="val_voc_code">
                                    </div>
                                    <div class="form-group">
                                        <label for="val_voc_description">Deskripsi <span style="color: red;">*</span></label>
                                        <textarea class="form-control" name="voc_description" placeholder="Deskripsi Kupon" id="val_voc_description"></textarea>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="val_voc_type">Tipe Kupon <span style="color: red;">*</span></label>
                                            <select class="form-control" name="voc_type">
                                                <option value="" id="val_voc_type">-- Pilih Tipe Kupon --</option>
                                                <option value="Percentage">Presentase Diskon</option>
                                                <option value="Fixed">Nominal Tetap</option>
                                            </select>
                                        </div>
                                        <div class="col-6">
                                            <label for="val_voc_amount">Nominal Kupon <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_amount" placeholder="Nominal Kupon" id="val_voc_amount">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="val_voc_discount_max">Maks Potongan <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_discount_max" placeholder="Maks Potongan Kupon" id="val_voc_discount_max">
                                        </div>
                                        <div class="col-6">
                                            <label for="val_voc_min_spend">Min Pemesanan <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_min_spend" placeholder="Min Pemesanan" id="val_voc_min_spend">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="val_voc_max_used">Maks Penggunaan <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_max_used" placeholder="Maksimal Penggunaan" id="val_voc_max_used">
                                        </div>
                                        <div class="col-6">
                                            <label for="val_voc_max_used_daily">Maks (Hari) <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_max_used_daily" placeholder="Maks Penggunaan (Hari)" id="val_voc_max_used_daily">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="val_voc_start_at">Mulai Digunakan <span style="color: red;">*</span></label>
                                            <input type="date" class="form-control" name="voc_start_at" id="val_voc_start_at" placeholder="Mulai Digunakan">
                                        </div>
                                        <div class="col-6">
                                            <label for="val_voc_expired_at">Kadaluarsa <span style="color: red;">*</span></label>
                                            <input type="date" class="form-control" name="voc_expired_at" id="val_voc_expired_at" placeholder="Kadaluarsa">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_3" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal Edit Data -->

                <!-- Start: Modal Add Data -->
                <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Tambah Kupon</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <!-- Form Upload Gambar -->
                                <form id="form-upload-2" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Pilih Gambar</label>
                                        <input type="file" class="form-control-file" name="image" id="file_gambar">
                                    </div>
                                </form>
                                <!-- End Form Upload Gambar -->

                                <form method="POST" action="<?= $config['host']; ?>/api/admin/voucher?method=CREATE" id="Add-Data-Form">
                                    <!-- Gambar -->
                                    <div id="image_voc_box">
                                        <div class="form-group">
                                            <label>Gambar</label><br />
                                            <img id="image_voc_thumb" class="img-fluid" alt="Responsive image">
                                        </div>
                                        <input type="hidden" id="add_voc_image" name="voc_image" value="">
                                    </div>
                                    <!-- End Gambar -->
                                    <div class="form-group">
                                        <label for="voc_name">Nama Kupon <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" name="voc_name" placeholder="Nama Kupon">
                                    </div>
                                    <div class="form-group">
                                        <label for="voc_code">Kode Kupon <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" name="voc_code" placeholder="Kode Kupon">
                                    </div>
                                    <div class="form-group">
                                        <label for="voc_description">Deskripsi <span style="color: red;">*</span></label>
                                        <textarea class="form-control" name="voc_description" placeholder="Deskripsi Kupon"></textarea>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="voc_type">Tipe Kupon <span style="color: red;">*</span></label>
                                            <select class="form-control" name="voc_type">
                                                <option value="">-- Pilih Tipe Kupon --</option>
                                                <option value="Discount">Presentase Diskon</option>
                                                <option value="Fixed">Nominal Tetap</option>
                                            </select>
                                        </div>
                                        <div class="col-6">
                                            <label for="voc_amount">Nominal Kupon <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_amount" placeholder="Nominal Kupon">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="voc_discount_max">Maks Potongan <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_discount_max" placeholder="Maks Potongan Kupon">
                                        </div>
                                        <div class="col-6">
                                            <label for="voc_min_spend">Min Pemesanan <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_min_spend" placeholder="Min Pemesanan">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="voc_max_used">Maks Penggunaan <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_max_used" placeholder="Maksimal Penggunaan">
                                        </div>
                                        <div class="col-6">
                                            <label for="voc_max_used_daily">Maks (Hari) <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" name="voc_max_used_daily" placeholder="Maks Penggunaan (Hari)">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="voc_start_at">Mulai Digunakan <span style="color: red;">*</span></label>
                                            <input type="date" class="form-control" name="voc_start_at" placeholder="Mulai Digunakan">
                                        </div>
                                        <div class="col-6">
                                            <label for="voc_expired_at">Kadaluarsa <span style="color: red;">*</span></label>
                                            <input type="date" class="form-control" name="voc_expired_at" placeholder="Kadaluarsa">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal Add Data -->

            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <!-- Datepicker -->
    <!--  -->

    <!-- Custom JS -->
    <script type="text/javascript">
        $(document).ready(function() {

            $('#image_voc_box').hide();

            // Upload gambar: Add
            $('#file_gambar').change(function() {
                var file_data = $('#file_gambar').prop('files')[0];
                var form_data = new FormData();
                form_data.append('image', file_data);
                $.ajax({
                    url: '<?= $config['host']; ?>/api/v1/upload-images',
                    type: 'POST',
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: false,
                    dataType: 'JSON',
                    success: function(hasil) {
                        // tampilkan nama file yang diunggah jika berhasil
                        if (hasil.status == 1) {
                            let file_path = hasil.data.file_path;
                            let file_name = hasil.data.file_name;
                            swal('Berhasil!', hasil.content, 'success');
                            $('#image_voc_thumb').attr('src', '<?= $config['host']; ?>/' + file_path);
                            $('#image_voc_box').show(1000);
                            $('#add_voc_image').val(file_path);
                        } else {
                            swal("Failed!", hasil.content, "error");
                        }
                    },
                    error: function(xhr, status, error) {
                        // tampilkan pesan error jika terjadi kesalahan
                        alert('Terjadi kesalahan: ' + error);
                    }
                });
            });

            // Upload gambar: Edit
            $('#file_gambar_2').change(function() {
                var file_data = $('#file_gambar_2').prop('files')[0];
                var form_data = new FormData();
                form_data.append('image', file_data);
                $.ajax({
                    url: '<?= $config['host']; ?>/api/v1/upload-images',
                    type: 'POST',
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: false,
                    dataType: 'JSON',
                    success: function(hasil) {
                        // tampilkan nama file yang diunggah jika berhasil
                        if (hasil.status == 1) {
                            let file_path = hasil.data.file_path;
                            let file_name = hasil.data.file_name;
                            swal('Berhasil!', hasil.content, 'success');
                            $('#image_voc_thumb_edit').attr('src', '<?= $config['host']; ?>/' + file_path);
                            $('#edit_voc_image').val(file_path);
                        } else {
                            swal("Failed!", hasil.content, "error");
                        }
                    },
                    error: function(xhr, status, error) {
                        // tampilkan pesan error jika terjadi kesalahan
                        alert('Terjadi kesalahan: ' + error);
                    }
                });
            });


            // JS fetch list data
            var table = $('#List-Data').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?= $config['host']; ?>/api/admin/voucher?method=READ_LIST",
                "columnDefs": [{
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<span class='text-nowrap'><button class='btn btn-info btn-sm tblEdit'><i class='fa fa-edit'></i></button> <button class='btn btn-danger btn-sm tblDelete'><i class='fa fa-trash'></i></button></span>"
                    },
                    {
                        "targets": 0,
                        // "className": "d-none"
                    },
                    {
                        "targets": [1, 2, 3, 4, 5, 6, 7, 8, 9],
                        "className": "text-nowrap"
                    }
                ]
            });

            // JS create data
            $("form#Add-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalAdd').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Simpan');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS show data single -> show edit form
            $('#List-Data tbody').on('click', '.tblEdit', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/voucher?method=READ_SINGLE",
                    data: {
                        id_voucher: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#modalEdit').modal('show');
                        $('#val_id_data').val(result.data.id_voucher);
                        $('#val_voc_name').val(result.data.voc_name);
                        $('#val_voc_code').val(result.data.voc_code);
                        $('#val_voc_description').val(result.data.voc_description);
                        $('#val_voc_type').val(result.data.voc_type);
                        $('#val_voc_amount').val(result.data.voc_amount);
                        $('#val_voc_discount_max').val(result.data.voc_discount_max);
                        $('#val_voc_min_spend').val(result.data.voc_min_spend);
                        $('#val_voc_max_used').val(result.data.voc_max_used);
                        $('#val_voc_max_used_daily').val(result.data.voc_max_used_daily);
                        $('#val_voc_start_at').val(result.data.voc_start_at);
                        $('#val_voc_expired_at').val(result.data.voc_expired_at);
                        $('#image_voc_thumb_edit').attr('src', '<?= $config['host']; ?>/' + result.data.voc_image);
                    }
                });
            });

            // JS update data
            $("form#Edit-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalEdit').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS delete data
            $('#List-Data tbody').on('click', '.tblDelete', function() {
                var data = table.row($(this).parents('tr')).data();
                //var nama_tools = data[1];
                var id_data = data[0];
                swal({
                        title: "Anda yakin?",
                        text: "Anda tidak bisa mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, hapus data!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/admin/voucher?method=DELETE",
                                data: {
                                    id_voucher: id_data
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    if (hasil.status) {
                                        swal("Berhasil", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Dibatalkan", "Data anda aman :)", "error");
                        }
                    })
            });

        });
    </script>
</body>

</html>