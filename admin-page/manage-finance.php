<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['name'] . ' - Kelola Keuangan';
$allow_position = array('DEVELOPER', 'PEMILIK', 'MANAGER', 'KOKI', 'PELAYAN');
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
} else if (in_array(strtoupper($_SESSION['emp_job_name']), $allow_position) == false) {
    header('Location:' . $config['host_admin']);
    exit();
}


function getTotalIn($wallet, $range, $range_str = 0, $range_end = 0)
{
    global $conn;

    switch ($range) {
        case 'ALL':
            $total_in = mysqli_query($conn, "SELECT SUM(amount_in) AS `total_in` FROM `wrtg_wallet_log` WHERE `id_wallet`='$wallet'");
            $total_in = mysqli_fetch_assoc($total_in)['total_in'];
            break;
        case 'CUSTOM':
            $total_in   = mysqli_query($conn, "SELECT SUM(`amount_in`) AS `total_in` FROM `wrtg_wallet_log` WHERE FROM_UNIXTIME(`created_at`, '%d-%m-%Y') BETWEEN '$range_str' AND '$range_end' AND `id_wallet`='$wallet'");
            $total_in = mysqli_fetch_assoc($total_in)['total_in'];
            break;
        default:
            $total_in = mysqli_query($conn, "SELECT SUM(amount_in) AS `total_in` FROM `wrtg_wallet_log` WHERE `id_wallet`='$wallet'");
            $total_in = mysqli_fetch_assoc($total_in)['total_in'];
            break;
    }

    return $total_in;
}

function getTotalOut($wallet, $range, $range_str = 0, $range_end = 0)
{
    global $conn;

    switch ($range) {
        case 'ALL':
            $total_out  = mysqli_query($conn, "SELECT SUM(amount_out) AS `total_out` FROM `wrtg_wallet_log` WHERE `id_wallet`='901'");
            $total_out  = mysqli_fetch_assoc($total_out)['total_out'];
            break;
        case 'CUSTOM':
            $total_out  = mysqli_query($conn, "SELECT SUM(`amount_out`) AS `total_out` FROM `wrtg_wallet_log` WHERE FROM_UNIXTIME(`created_at`, '%d-%m-%Y') BETWEEN '$range_str' AND '$range_end' AND `id_wallet`='$wallet'");
            $total_out  = mysqli_fetch_assoc($total_out)['total_out'];
            break;
        default:
            $total_out  = mysqli_query($conn, "SELECT SUM(amount_out) AS `total_out` FROM `wrtg_wallet_log` WHERE `id_wallet`='901'");
            $total_out  = mysqli_fetch_assoc($total_out)['total_out'];
            break;
    }

    return $total_out;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php include('../inc/admin-page/admin-header.phtml'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Kelola Keuangan</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">

                        <!-- Form Add -->
                        <div class="col-lg-4">
                            <?php
                            $query  = "SELECT * FROM `wrtg_wallet` WHERE `id_wallet`='901'";
                            $query  = mysqli_query($conn, $query);
                            $wallet = mysqli_fetch_assoc($query);
                            ?>
                            <div class="info-box bg-info">
                                <span class="info-box-icon"><i class="fas fa-wallet"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Dompet Utama</span>
                                    <h5 class="info-box-number"><?= rupiah($wallet['wl_balance']); ?></h5>
                                </div>
                            </div>
                            <div>
                                <h5>Semua Periode</h5>
                                <div class="info-box border border-success">
                                    <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Pendapatan</span>
                                        <span class="info-box-number"><?= rupiah(getTotalIn(901, 'ALL')); ?></span>
                                    </div>
                                </div>
                                <div class="info-box border border-danger">
                                    <span class="info-box-icon bg-danger"><i class="far fa-flag"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Total Pengeluaran</span>
                                        <span class="info-box-number"><?= rupiah(getTotalOut(901, 'ALL')); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Table Data -->
                        <div class="col-lg-8">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-file-invoice-dollar"></i> Data Laporan Keuangan</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label>Periode Awal</label>
                                                    <input type="date" class="form-control" name="range_start" id="range_start">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label>Periode Akhir</label>
                                                    <input type="date" class="form-control" name="range_end" id="range_end">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <button class="btn btn-primary btn-block" onclick="changeRange();"><i class="fas fa-funnel-dollar"></i> Filter Data</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="CSTM-Section">
                                            <div class="col-lg-12">
                                                <div class="alert alert-info" id="CSTM-Info">Menampilkan data secara keseluruhan</div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="info-box border border-success">
                                                    <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>
                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Total Pendapatan</span>
                                                        <span class="info-box-number" id="CSTM-Total-In"><?= rupiah(getTotalIn(901, 'ALL')); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="info-box border border-danger">
                                                    <span class="info-box-icon bg-danger"><i class="far fa-flag"></i></span>
                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Total Pengeluaran</span>
                                                        <span class="info-box-number" id="CSTM-Total-Out"><?= rupiah(getTotalOut(901, 'ALL')); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="List-Data" class="display table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Waktu Dibuat</th>
                                                    <th class="text-nowrap">Dana Masuk</th>
                                                    <th class="text-nowrap">Dana Keluar</th>
                                                    <th class="text-nowrap">Dana Total</th>
                                                    <th class="text-nowrap">Berita</th>
                                                    <th class="text-nowrap">Catatan</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Waktu Dibuat</th>
                                                    <th class="text-nowrap">Dana Masuk</th>
                                                    <th class="text-nowrap">Dana Keluar</th>
                                                    <th class="text-nowrap">Dana Total</th>
                                                    <th class="text-nowrap">Berita</th>
                                                    <th class="text-nowrap">Catatan</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- /.row -->

                    </div>
                </div>
                <!-- /.container-fluid -->


            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <!-- Custom JS -->
    <script type="text/javascript">
        function changeRange() {
            let range_str = $('#range_start').val();
            let range_end = $('#range_end').val();
            if ((!range_str) || (!range_end)) {
                swal('Failed!', 'Harap lengkapi Periode Awal & Periode Akhir!', 'error');
            } else {
                loadDatatables(range_str, range_end);
            }
        }

        function loadDatatables(range_str, range_end) {
            // JS fetch list data
            calculateFinance(range_str, range_end);
            $("#List-Data").dataTable().fnDestroy();
            var table = $('#List-Data').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?= $config['host']; ?>/api/admin/finance?method=READ_LIST&range_str=" + range_str + "&range_end=" + range_end,
                "order": [
                    [1, 'desc'],
                    [0, 'desc']
                ],
                "dom": 'Bfrtip',
                buttons: [
                    'pageLength',
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        download: 'open',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ],
                "columnDefs": [{
                        "targets": -1,
                        "data": null,
                        "defaultContent": "-"
                    },
                    {
                        "targets": [],
                        "className": "d-none"
                    },
                    {
                        "targets": [1, 2, 3, 4, 5, 6],
                        "className": "text-nowrap"
                    }
                ]
            });
        }

        function calculateFinance(range_str, range_end) {
            $.ajax({
                url: "<?= $config['host']; ?>/api/admin/finance?method=CALCULATE_FINANCE",
                data: {
                    range_str,
                    range_end
                },
                timeout: false,
                type: 'POST',
                dataType: 'JSON',
                success: function(hasil) {
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                    if (hasil.status) {
                        $('#CSTM-Section').show();
                        $('#CSTM-Total-In').html(hasil.data.total_in_rp);
                        $('#CSTM-Total-Out').html(hasil.data.total_out_rp);
                        if ((!range_str) || (!range_end)) {
                            $('#CSTM-Info').html('Menampilkan data periode keseluruhan');
                        } else {
                            $('#CSTM-Info').html('Menampilkan data dari periode ' + range_str + ' s/d ' + range_end);
                        }
                    } else {
                        swal("Failed!", "" + hasil.content + "", "error");
                    }
                },
                error: function(a, b, c) {
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                },
                beforeSend: function() {
                    $("input").attr("disabled", "disabled");
                    $("button").attr("disabled", "disabled");
                }
            });
        }

        $(document).ready(function() {

            $('#CSTM-Section').hide();
            loadDatatables();

            $('#id_material').change(function() {
                var unit = $('#id_material option:selected').attr('unit');
                $('#log_unit').html(unit);
            });

            $('#log_price').keyup(function() {
                $('#log_price').val(formatRupiah(this.value));
            });

            // JS create data
            $("form#Add-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalAdd').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Simpan');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS show data single -> show edit form
            $('#List-Data tbody').on('click', '.tblEdit', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/materials?method=READ_SINGLE",
                    data: {
                        id_material: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#modalEdit').modal('show');
                        $('#val_id_data').val(result.data.id_material);
                        $('#val_material_name').val(result.data.material_name);
                        $('#val_material_unit').val(result.data.material_unit);
                        $('#val_material_stock').val(result.data.material_stock);
                    }
                });
            });

            // JS update data
            $("form#Edit-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalEdit').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Simpan');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS delete data
            $('#List-Data tbody').on('click', '.tblDelete', function() {
                var data = table.row($(this).parents('tr')).data();
                //var nama_tools = data[1];
                var id_data = data[0];
                swal({
                        title: "Anda yakin?",
                        text: "Anda tidak bisa mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, hapus data!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/admin/materials?method=DELETE",
                                data: {
                                    id_material: id_data
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    if (hasil.status) {
                                        swal("Berhasil", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Dibatalkan", "Data anda aman :)", "error");
                        }
                    })
            });

        });
    </script>
</body>

</html>