<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Kelola Laporan';
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
}

$id_emp     = $_SESSION['emp_id'];
$employee   = mysqli_query($conn, "SELECT *
FROM `wrtg_employee` `w`
INNER JOIN `wrtg_job` `j`
    ON (`w`.`emp_job_id`=`j`.`id_job`)
WHERE `id_employee`='$id_emp'");
$emp = mysqli_fetch_array($employee);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include('../inc/admin-page/admin-header.phtml'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">

                        <div class="col-lg-4">
                            <div class="alert alert-info">Laporan bisa ditentukan berdasarkan kriteria waktu.</div>
                        </div>

                        <div class="col-lg-8">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-folder-open"></i> Kriteria Laporan</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <form action="<?= $config['host']; ?>/api/admin/reports" method="POST" id="Open-Report-Form">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label>Periode Awal</label>
                                                    <input type="date" class="form-control" name="range_start" id="range_start">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label>Periode Akhir</label>
                                                    <input type="date" class="form-control" name="range_end" id="range_end">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Format Laporan</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="report_format" id="report_format_1" value="D" checked>
                                                <label class="form-check-label" for="report_format_1">
                                                    Detail
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="report_format" id="report_format_2" value="R">
                                                <label class="form-check-label" for="report_format_2">
                                                    Rekapitulasi
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Jenis Laporan</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="report_type" id="report_type_1" value="1" checked>
                                                <label class="form-check-label" for="report_type_1">
                                                    Laporan Transaksi
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="report_type" id="report_type__2" value="2">
                                                <label class="form-check-label" for="report_type_2">
                                                    Laporan Transaksi per Pelanggan
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="report_type" id="report_type_3" value="3">
                                                <label class="form-check-label" for="report_type_3">
                                                    Laporan Transaksi per Menu
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary btn-block" id="button_id">Buka Laporan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("form#Open-Report-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('Buka Laporan');
                        if (hasil.status) {
                            swal("Success!", hasil.content, "success");
                            if (hasil.data.report_url) {
                                window.location.href = hasil.data.report_url;
                            }
                        } else {
                            swal("Failed!", hasil.content, "error");
                        }
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('Buka Laporan');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });
        });
    </script>
</body>

</html>