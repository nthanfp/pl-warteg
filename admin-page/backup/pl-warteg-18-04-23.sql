DROP TABLE wrtg_config;

CREATE TABLE `wrtg_config` (
  `id_config` int NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `value` varchar(225) DEFAULT NULL,
  `status` enum('Y','N') DEFAULT NULL,
  PRIMARY KEY (`id_config`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO wrtg_config VALUES("101","host","http://localhost/pl-warteg","Y");
INSERT INTO wrtg_config VALUES("102","name","Warteg Bahari","Y");
INSERT INTO wrtg_config VALUES("103","title","PL  Warteg","Y");
INSERT INTO wrtg_config VALUES("104","description","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et fermentum orci. Sed molestie velit in dui cursus suscipit. Phasellus convallis bibendum tristique","Y");
INSERT INTO wrtg_config VALUES("105","keyword","warteg, warung tegal, warung makan, wateg murah, warteg bandung, warteg enak","Y");
INSERT INTO wrtg_config VALUES("106","author","Iskandarsyah Tech","Y");
INSERT INTO wrtg_config VALUES("107","og_image","-","N");
INSERT INTO wrtg_config VALUES("108","favicon","-","N");



DROP TABLE wrtg_customer;

CREATE TABLE `wrtg_customer` (
  `id_customer` int unsigned NOT NULL AUTO_INCREMENT,
  `cst_full_name` varchar(255) NOT NULL,
  `cst_password` varchar(255) NOT NULL,
  `cst_phone` varchar(20) NOT NULL,
  `cst_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `cst_full_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `cst_addr_state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `cst_addr_district` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `cst_addr_village` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `cst_pos_code` varchar(225) DEFAULT NULL,
  `cst_register_date` varchar(50) DEFAULT NULL,
  `cst_last_login` varchar(50) DEFAULT NULL,
  `cst_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO wrtg_customer VALUES("101","Andi Pratama","62cc2d8b4bf2d8728120d052163a77df","081234567890","andi@example.com","Jl. Pajajaran No. 10","Jawa Barat","Bandung","Lengkong","40115","","","");
INSERT INTO wrtg_customer VALUES("102","Budi Setiawan","62cc2d8b4bf2d8728120d052163a77df","081345678901","budi@example.com","Jl. Merdeka No. 20","Jawa Barat","Bandung","Coblong","40111","","","");
INSERT INTO wrtg_customer VALUES("103","Citra Wijaya","62cc2d8b4bf2d8728120d052163a77df","081456789012","citra@example.com","Jl. Sunda No. 30","Jawa Barat","Bandung","Sukajadi","40161","","","");
INSERT INTO wrtg_customer VALUES("109","Joko W","hahalol123","081238761276","joko@gmail.com","Jl Melati 1 No 8","Jawa Barat","Bandung","Rancamanyar","40375","","","");



DROP TABLE wrtg_detail_order;

CREATE TABLE `wrtg_detail_order` (
  `id_detail_order` int unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int unsigned NOT NULL,
  `id_menu` int unsigned NOT NULL,
  `amount` int NOT NULL,
  `price` int NOT NULL,
  `total_price` int NOT NULL,
  PRIMARY KEY (`id_detail_order`),
  KEY `id_order` (`id_order`),
  KEY `id_menu` (`id_menu`),
  CONSTRAINT `wrtg_detail_order_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `wrtg_order` (`id_order`),
  CONSTRAINT `wrtg_detail_order_ibfk_2` FOREIGN KEY (`id_menu`) REFERENCES `wrtg_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;




DROP TABLE wrtg_employee;

CREATE TABLE `wrtg_employee` (
  `id_employee` int unsigned NOT NULL AUTO_INCREMENT,
  `emp_full_name` varchar(255) NOT NULL,
  `emp_email` varchar(255) NOT NULL,
  `emp_password` varchar(255) NOT NULL,
  `emp_phone` varchar(20) NOT NULL,
  `emp_job_id` int DEFAULT NULL,
  `emp_salary_day` int DEFAULT NULL,
  `emp_register_date` varchar(50) DEFAULT NULL,
  `emp_last_login` varchar(50) DEFAULT NULL,
  `emp_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_employee`),
  KEY `emp_job_id` (`emp_job_id`),
  CONSTRAINT `FK_wrtg_employee_wrtg_job` FOREIGN KEY (`emp_job_id`) REFERENCES `wrtg_job` (`id_job`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO wrtg_employee VALUES("101","John Doe","johndoe@example.com","0192023a7bbd73250516f069df18b500","081234567890","81","","","","");
INSERT INTO wrtg_employee VALUES("102","Jane Doe","janedoe@example.com","0192023a7bbd73250516f069df18b500","081234567891","82","","","","");
INSERT INTO wrtg_employee VALUES("103","Bob Smith","bobsmith@example.com","0192023a7bbd73250516f069df18b500","081234567892","84","","","","");



DROP TABLE wrtg_job;

CREATE TABLE `wrtg_job` (
  `id_job` int NOT NULL AUTO_INCREMENT,
  `job_name` varchar(50) DEFAULT NULL,
  `job_salary_day` int DEFAULT NULL,
  PRIMARY KEY (`id_job`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO wrtg_job VALUES("81","Pemilik","0");
INSERT INTO wrtg_job VALUES("82","Manajer","95000");
INSERT INTO wrtg_job VALUES("83","Chef","80000");
INSERT INTO wrtg_job VALUES("84","Pelayan","55000");



DROP TABLE wrtg_menu;

CREATE TABLE `wrtg_menu` (
  `id_menu` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_description` varchar(255) NOT NULL,
  `menu_images` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `menu_price` int NOT NULL,
  `menu_stock` int NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO wrtg_menu VALUES("101","Nasi Putih","Nasi putih hangat","","4000","1");
INSERT INTO wrtg_menu VALUES("102","Ayam Goreng","Ayam goreng krispi","","15000","0");
INSERT INTO wrtg_menu VALUES("103","Tahu Goreng","-","","5000","1");
INSERT INTO wrtg_menu VALUES("104","Tempe Goreng","-","","5000","1");



DROP TABLE wrtg_menu_detail;

CREATE TABLE `wrtg_menu_detail` (
  `id_menu` int unsigned NOT NULL,
  `id_variant_group` int NOT NULL,
  PRIMARY KEY (`id_menu`,`id_variant_group`),
  KEY `wrtg_menu_detail_ibfk_2` (`id_variant_group`),
  CONSTRAINT `wrtg_menu_detail_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `wrtg_menu` (`id_menu`),
  CONSTRAINT `wrtg_menu_detail_ibfk_2` FOREIGN KEY (`id_variant_group`) REFERENCES `wrtg_variant_group` (`id_variant_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;




DROP TABLE wrtg_order;

CREATE TABLE `wrtg_order` (
  `id_order` int unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` int unsigned NOT NULL,
  `order_date` datetime NOT NULL,
  `order_type` enum('Takeaway','Delivery','DineIn') NOT NULL,
  `order_status` enum('Pending','Process','Completed','Canceled') NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `id_customer` (`id_customer`),
  CONSTRAINT `wrtg_order_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `wrtg_customer` (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;




DROP TABLE wrtg_variant;

CREATE TABLE `wrtg_variant` (
  `id_variant` int unsigned NOT NULL AUTO_INCREMENT,
  `id_variant_group` int DEFAULT NULL,
  `variant_name` varchar(255) NOT NULL,
  `variant_description` varchar(255) NOT NULL,
  `variant_price` int NOT NULL,
  PRIMARY KEY (`id_variant`),
  KEY `fk_variant_group` (`id_variant_group`),
  CONSTRAINT `fk_variant_group` FOREIGN KEY (`id_variant_group`) REFERENCES `wrtg_variant_group` (`id_variant_group`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO wrtg_variant VALUES("101","101","Level Pedas 1","Tingkat kepedasan level 1","7000");
INSERT INTO wrtg_variant VALUES("102","101","Level Pedas 2","Tingkat kepedasan level 2","2500");
INSERT INTO wrtg_variant VALUES("103","101","Level Pedas 3","Tingkat kepedasan level 3","3000");
INSERT INTO wrtg_variant VALUES("104","101","Level Pedas 4","Tingkat kepedasan level 4","3500");
INSERT INTO wrtg_variant VALUES("105","101","Level Pedas 5","Tingkat kepedasan level 5","4500");
INSERT INTO wrtg_variant VALUES("106","102","Porsi Kecil","Ukuran porsi kecil","5000");
INSERT INTO wrtg_variant VALUES("107","102","Porsi Sedang","Ukuran porsi sedang","7000");
INSERT INTO wrtg_variant VALUES("108","102","Porsi Besar","Ukuran porsi besar","10000");
INSERT INTO wrtg_variant VALUES("120","1003","Kecil (S)","Nasi ukuran kecil","1000");
INSERT INTO wrtg_variant VALUES("121","1003","Sedang (M)","Nasi ukuran sedang","3000");
INSERT INTO wrtg_variant VALUES("122","1003","Besar (L)","Nasi ukuran besar","4000");



DROP TABLE wrtg_variant_group;

CREATE TABLE `wrtg_variant_group` (
  `id_variant_group` int NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  `description` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id_variant_group`)
) ENGINE=InnoDB AUTO_INCREMENT=1006 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO wrtg_variant_group VALUES("101","Level Pedas","Variant menu tingkat kepedasan");
INSERT INTO wrtg_variant_group VALUES("102","Ukuran Porsi","Variant menu ukuran porsi");
INSERT INTO wrtg_variant_group VALUES("1003","Porsi Nasi (Size)","Nothing");



