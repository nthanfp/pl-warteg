DROP TABLE wrtg_config;

CREATE TABLE `wrtg_config` (
  `id_config` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `value` varchar(225) DEFAULT NULL,
  `status` enum('Y','N') DEFAULT NULL,
  PRIMARY KEY (`id_config`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_config VALUES("101","host","http://localhost/pl-warteg","Y");
INSERT INTO wrtg_config VALUES("102","name","Warteg Bahari","Y");
INSERT INTO wrtg_config VALUES("103","title","Warteg Baharix - Admin Control","Y");
INSERT INTO wrtg_config VALUES("104","description","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et fermentum orci. Sed molestie velit in dui cursus suscipit. Phasellus convallis bibendum tristique","Y");
INSERT INTO wrtg_config VALUES("105","keyword","warteg, warung tegal, warung makan, wateg murah, warteg bandung, warteg enak","Y");
INSERT INTO wrtg_config VALUES("106","author","Iskandarsyah Tech","Y");
INSERT INTO wrtg_config VALUES("107","og_image","-","Y");
INSERT INTO wrtg_config VALUES("108","favicon","-","Y");



DROP TABLE wrtg_customer;

CREATE TABLE `wrtg_customer` (
  `id_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cst_full_name` varchar(255) NOT NULL,
  `cst_password` varchar(255) NOT NULL,
  `cst_phone` varchar(20) NOT NULL,
  `cst_email` varchar(255) NOT NULL,
  `cst_full_address` varchar(255) DEFAULT NULL,
  `cst_addr_state` varchar(255) DEFAULT NULL,
  `cst_addr_district` varchar(255) DEFAULT NULL,
  `cst_addr_village` varchar(255) DEFAULT NULL,
  `cst_pos_code` varchar(225) DEFAULT NULL,
  `cst_register_date` varchar(50) DEFAULT NULL,
  `cst_last_login` varchar(50) DEFAULT NULL,
  `cst_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_customer VALUES("102","Budi Setiawan","62cc2d8b4bf2d8728120d052163a77df","081345678901","budi@example.com","Jl. Merdeka No. 20","Jawa Barat","Bandung","Coblong","40111","","","");
INSERT INTO wrtg_customer VALUES("103","Citra Wijaya","62cc2d8b4bf2d8728120d052163a77df","081456789012","citra@example.com","Jl. Sunda No. 30","Jawa Barat","Bandung","Sukajadi","40161","","","");
INSERT INTO wrtg_customer VALUES("109","Joko W","hahalol123","081238761276","joko@gmail.com","Jl Melati 1 No 8","Jawa Barat","Bandung","Rancamanyar","40375","","","");



DROP TABLE wrtg_employee;

CREATE TABLE `wrtg_employee` (
  `id_employee` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `emp_full_name` varchar(255) NOT NULL,
  `emp_email` varchar(255) NOT NULL,
  `emp_password` varchar(255) NOT NULL,
  `emp_phone` varchar(20) NOT NULL,
  `emp_job_id` int(11) DEFAULT NULL,
  `emp_salary_day` int(11) DEFAULT NULL,
  `emp_register_date` varchar(50) DEFAULT NULL,
  `emp_last_login` varchar(50) DEFAULT NULL,
  `emp_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_employee`),
  KEY `emp_job_id` (`emp_job_id`),
  CONSTRAINT `FK_wrtg_employee_wrtg_job` FOREIGN KEY (`emp_job_id`) REFERENCES `wrtg_job` (`id_job`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_employee VALUES("101","John Doe","johndoe@example.com","0192023a7bbd73250516f069df18b500","081234567890","81","","","","");
INSERT INTO wrtg_employee VALUES("105","Abi","abi@iskandarsyah.co","0192023a7bbd73250516f069df18b500","081212345678","86","","1682700930","","");
INSERT INTO wrtg_employee VALUES("106","Nathan","nathan@iskandarsyah.co","0192023a7bbd73250516f069df18b500","081212345678","86","","1682700941","","");
INSERT INTO wrtg_employee VALUES("107","Andi","andi@iskandarsyah.co","0192023a7bbd73250516f069df18b500","081212345678","86","","1682700951","","");



DROP TABLE wrtg_images;

CREATE TABLE `wrtg_images` (
  `id_image` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `created_at` varchar(50) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_image`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_images VALUES("18","81a22d05fef9d528232e30ae4941dddc.png","uploads/images/81a22d05fef9d528232e30ae4941dddc.png","1682131275","1682131275");
INSERT INTO wrtg_images VALUES("19","8ced231d4ba703b10a0472a221055d29.png","uploads/images/8ced231d4ba703b10a0472a221055d29.png","1682131351","1682131351");
INSERT INTO wrtg_images VALUES("20","7e21bd5f9dd027a928386dcfa74faa02.jpg","uploads/images/7e21bd5f9dd027a928386dcfa74faa02.jpg","1682136010","1682136010");
INSERT INTO wrtg_images VALUES("21","7b12ea7cf6c62be7f2cb4c96ef61137a.jpeg","uploads/images/7b12ea7cf6c62be7f2cb4c96ef61137a.jpeg","1682136627","1682136627");
INSERT INTO wrtg_images VALUES("22","51aeabdc4c669c2abeccfcf93763d53d.jpeg","uploads/images/51aeabdc4c669c2abeccfcf93763d53d.jpeg","1682136644","1682136644");
INSERT INTO wrtg_images VALUES("23","25a7de391d84a983a0a7aff355c16719.jpeg","uploads/images/25a7de391d84a983a0a7aff355c16719.jpeg","1682136686","1682136686");
INSERT INTO wrtg_images VALUES("24","093ee7f237e72e80b326b5a7d05b365a.png","uploads/images/093ee7f237e72e80b326b5a7d05b365a.png","1682136788","1682136788");
INSERT INTO wrtg_images VALUES("25","1e6100c7308713f775a12406d79c9700.jpeg","uploads/images/1e6100c7308713f775a12406d79c9700.jpeg","1682136961","1682136961");
INSERT INTO wrtg_images VALUES("26","7199270da3a29c9daca8ca1e853a3284.png","uploads/images/7199270da3a29c9daca8ca1e853a3284.png","1682159815","1682159815");
INSERT INTO wrtg_images VALUES("27","48c281d8f10bc9ed3a9c0257f688a56c.png","uploads/images/48c281d8f10bc9ed3a9c0257f688a56c.png","1682159956","1682159956");
INSERT INTO wrtg_images VALUES("28","c7e656ba77b7e08141c1973a30065814.jpg","uploads/images/c7e656ba77b7e08141c1973a30065814.jpg","1682159967","1682159967");
INSERT INTO wrtg_images VALUES("29","3eb40a661a9314322807ce78c4b5768a.jpg","uploads/images/3eb40a661a9314322807ce78c4b5768a.jpg","1682561199","1682561199");



DROP TABLE wrtg_job;

CREATE TABLE `wrtg_job` (
  `id_job` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(50) DEFAULT NULL,
  `job_salary_day` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_job`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_job VALUES("81","Pemilik","0");
INSERT INTO wrtg_job VALUES("82","Manajer","95000");
INSERT INTO wrtg_job VALUES("83","Chef","80000");
INSERT INTO wrtg_job VALUES("84","Pelayan","55000");
INSERT INTO wrtg_job VALUES("86","Developer","0");



DROP TABLE wrtg_materials;

CREATE TABLE `wrtg_materials` (
  `id_material` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(255) DEFAULT NULL,
  `material_unit` varchar(50) DEFAULT NULL,
  `material_stock` int(11) DEFAULT NULL,
  `created_at` varchar(50) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_material`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_materials VALUES("156","Masako","Gram","500","","2023-04-28 11:20:56");



DROP TABLE wrtg_materials_logs;

CREATE TABLE `wrtg_materials_logs` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) DEFAULT NULL,
  `log_type` enum('IN','OUT') DEFAULT NULL,
  `log_qty` int(11) DEFAULT NULL,
  `log_notes` text DEFAULT NULL,
  `created_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;




DROP TABLE wrtg_menu;

CREATE TABLE `wrtg_menu` (
  `id_menu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_category` int(11) DEFAULT NULL,
  `menu_name` varchar(255) NOT NULL,
  `menu_description` varchar(255) NOT NULL,
  `menu_images` varchar(255) DEFAULT NULL,
  `menu_price` int(11) NOT NULL,
  `menu_stock` int(11) NOT NULL,
  PRIMARY KEY (`id_menu`),
  KEY `id_category` (`id_category`),
  CONSTRAINT `FK_wrtg_menu_wrtg_menu_category` FOREIGN KEY (`id_category`) REFERENCES `wrtg_menu_category` (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_menu VALUES("109","104","Nasi Putih","Nasi putih hangat","uploads/images/c7e656ba77b7e08141c1973a30065814.jpg","3000","99");
INSERT INTO wrtg_menu VALUES("110","104","Ayam Goreng","Ayam goreng dengan rempah khas","uploads/images/1e6100c7308713f775a12406d79c9700.jpeg","12000","10");



DROP TABLE wrtg_menu_category;

CREATE TABLE `wrtg_menu_category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_menu_category VALUES("104","Uncategorized");
INSERT INTO wrtg_menu_category VALUES("105","Aneka Snack");
INSERT INTO wrtg_menu_category VALUES("106","Paket Makan");
INSERT INTO wrtg_menu_category VALUES("107","Minuman");
INSERT INTO wrtg_menu_category VALUES("108","Aneka Sayur");
INSERT INTO wrtg_menu_category VALUES("109","Aneka Lauk");



DROP TABLE wrtg_menu_detail;

CREATE TABLE `wrtg_menu_detail` (
  `id_menu` int(10) unsigned NOT NULL,
  `id_variant_group` int(11) NOT NULL,
  PRIMARY KEY (`id_menu`,`id_variant_group`),
  KEY `wrtg_menu_detail_ibfk_2` (`id_variant_group`),
  CONSTRAINT `wrtg_menu_detail_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `wrtg_menu` (`id_menu`),
  CONSTRAINT `wrtg_menu_detail_ibfk_2` FOREIGN KEY (`id_variant_group`) REFERENCES `wrtg_variant_group` (`id_variant_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_menu_detail VALUES("109","1006");
INSERT INTO wrtg_menu_detail VALUES("110","1007");



DROP TABLE wrtg_order;

CREATE TABLE `wrtg_order` (
  `id_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) unsigned NOT NULL,
  `order_date` datetime NOT NULL,
  `order_type` enum('Takeaway','Delivery','DineIn') NOT NULL,
  `order_status` enum('Pending','Process','Completed','Canceled') NOT NULL,
  PRIMARY KEY (`id_order`),
  KEY `id_customer` (`id_customer`),
  CONSTRAINT `wrtg_order_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `wrtg_customer` (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;




DROP TABLE wrtg_order_detail;

CREATE TABLE `wrtg_order_detail` (
  `id_detail_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(10) unsigned NOT NULL,
  `id_menu` int(10) unsigned NOT NULL,
  `amount` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_order`),
  KEY `id_order` (`id_order`),
  KEY `id_menu` (`id_menu`),
  CONSTRAINT `wrtg_order_detail_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `wrtg_order` (`id_order`),
  CONSTRAINT `wrtg_order_detail_ibfk_2` FOREIGN KEY (`id_menu`) REFERENCES `wrtg_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;




DROP TABLE wrtg_unit;

CREATE TABLE `wrtg_unit` (
  `id_unit` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_unit`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_unit VALUES("168","Kilogram","2023-04-28 11:03:27","2023-04-28 11:03:27");
INSERT INTO wrtg_unit VALUES("169","Gram","2023-04-28 11:03:27","2023-04-28 11:03:27");
INSERT INTO wrtg_unit VALUES("170","Mililiter","2023-04-28 11:03:27","2023-04-28 11:03:27");
INSERT INTO wrtg_unit VALUES("171","Liter","2023-04-28 11:03:27","2023-04-28 11:03:27");
INSERT INTO wrtg_unit VALUES("172","Centimeter","2023-04-28 11:03:27","2023-04-28 11:03:27");
INSERT INTO wrtg_unit VALUES("173","Meter","2023-04-28 11:03:27","2023-04-28 11:03:27");
INSERT INTO wrtg_unit VALUES("174","Inci","2023-04-28 11:03:27","2023-04-28 11:03:27");
INSERT INTO wrtg_unit VALUES("175","Kaki","2023-04-28 11:03:27","2023-04-28 11:03:27");
INSERT INTO wrtg_unit VALUES("176","Box","2023-04-28 11:03:27","2023-04-28 11:03:27");
INSERT INTO wrtg_unit VALUES("177","Pak","2023-04-28 11:03:27","2023-04-28 11:03:27");



DROP TABLE wrtg_variant;

CREATE TABLE `wrtg_variant` (
  `id_variant` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_variant_group` int(11) DEFAULT NULL,
  `variant_name` varchar(255) NOT NULL,
  `variant_description` varchar(255) NOT NULL,
  `variant_price` int(11) NOT NULL,
  PRIMARY KEY (`id_variant`),
  KEY `fk_variant_group` (`id_variant_group`),
  CONSTRAINT `fk_variant_group` FOREIGN KEY (`id_variant_group`) REFERENCES `wrtg_variant_group` (`id_variant_group`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_variant VALUES("101","101","Level Pedas 1","Tingkat kepedasan level 1","7000");
INSERT INTO wrtg_variant VALUES("102","101","Level Pedas 2","Tingkat kepedasan level 2","2500");
INSERT INTO wrtg_variant VALUES("103","101","Level Pedas 3","Tingkat kepedasan level 3","3000");
INSERT INTO wrtg_variant VALUES("104","101","Level Pedas 4","Tingkat kepedasan level 4","3500");
INSERT INTO wrtg_variant VALUES("105","101","Level Pedas 5","Tingkat kepedasan level 5","4500");
INSERT INTO wrtg_variant VALUES("123","1006","Besar (L)","Nasi porsi besar","1000");
INSERT INTO wrtg_variant VALUES("124","1006","Ekstra Besar (XL)","Nasi porsi ekstra besar","2000");
INSERT INTO wrtg_variant VALUES("125","1007","Dada","-","0");
INSERT INTO wrtg_variant VALUES("126","1007","Paha","-","0");
INSERT INTO wrtg_variant VALUES("127","1009","Dingin","-","0");
INSERT INTO wrtg_variant VALUES("128","1009","Panas","-","0");



DROP TABLE wrtg_variant_group;

CREATE TABLE `wrtg_variant_group` (
  `id_variant_group` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  `description` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_variant_group`)
) ENGINE=InnoDB AUTO_INCREMENT=1010 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO wrtg_variant_group VALUES("101","Level Pedas","Variant menu tingkat kepedasan","","");
INSERT INTO wrtg_variant_group VALUES("1006","Tambah Porsi Nasi","Tambah ukuran porsi nasi","","");
INSERT INTO wrtg_variant_group VALUES("1007","Bagian Ayam","Bagian potongan ayam","","");
INSERT INTO wrtg_variant_group VALUES("1009","Minuman","-","","");



