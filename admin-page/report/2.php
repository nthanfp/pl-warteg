<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Laporan Transaksi per Pelanggan';
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
}

$id_emp     = $_SESSION['emp_id'];
$employee   = mysqli_query($conn, "SELECT *
FROM `wrtg_employee` `w`
INNER JOIN `wrtg_job` `j`
    ON (`w`.`emp_job_id`=`j`.`id_job`)
WHERE `id_employee`='$id_emp'");
$emp = mysqli_fetch_array($employee);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../../inc/admin-page/admin-head.phtml'); ?>
    <script type="text/javascript" src="https://raw.githubusercontent.com/Spyes/printTable.js/master/printTable.min.js"></script>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include('../../inc/admin-page/admin-header.phtml'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Laporan Transaksi per Pelanggan</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <a href="<?= $config['host_admin']; ?>/reports" class="btn btn-primary mb-3"><i class="fas fa-chevron-circle-left"></i> Kembali Ke Laporan</a>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Laporan Transaksi</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php
                            if ($_GET['report_format'] == 'D') {
                            ?>
                                <div class="alert alert-info">
                                    <?php
                                    if ((!empty($_GET['range_start'])) || (!empty($_GET['range_end']))) {
                                        echo 'Menampilakan laporan detail transaksi per pelanggan periode ' . $_GET['range_start'] . ' - ' . $_GET['range_end'];
                                    } else {
                                        echo 'Menampilakan laporan detail transaksi per pelanggan keseluruhan';
                                    }
                                    ?>
                                </div>
                                <div class="table-responsive">
                                    <table id="table_D" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID Pelanggan</th>
                                                <th>Nama Pelanggan</th>
                                                <th>Jumlah Transaksi</th>
                                                <th>Total Transaksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ((!empty($_GET['range_start'])) || (!empty($_GET['range_end']))) {
                                                $range_str = $_GET['range_start'];
                                                $range_end = $_GET['range_end'];
                                                $query = "SELECT `o`.`id_customer`, `cst_full_name`, `order_time`, `id_order`, SUM(`order_total`) AS `x_total`, COUNT(`id_order`) AS `x_order`
                                                FROM `wrtg_order` `o`
                                                INNER JOIN `wrtg_customer` `c`
                                                    ON (`o`.`id_customer`=`c`.`id_customer`)
                                                INNER JOIN `wrtg_payment` `p`
                                                    ON (`p`.`pay_code`=`o`.`order_payment_code`)
                                                WHERE FROM_UNIXTIME(`order_time`, '%Y-%m-%d') BETWEEN '$range_str' AND '$range_end'
                                                GROUP BY `o`.`id_customer`
                                                ORDER BY `c`.`cst_full_name`";
                                            } else {
                                                $query = "SELECT `o`.`id_customer`, `cst_full_name`, `order_time`, `id_order`, SUM(`order_total`) AS `x_total`, COUNT(`id_order`) AS `x_order`
                                                FROM `wrtg_order` `o`
                                                INNER JOIN `wrtg_customer` `c`
                                                    ON (`o`.`id_customer`=`c`.`id_customer`)
                                                INNER JOIN `wrtg_payment` `p`
                                                    ON (`p`.`pay_code`=`o`.`order_payment_code`)
                                                GROUP BY `o`.`id_customer`
                                                ORDER BY `c`.`cst_full_name`";
                                            }
                                            $query = mysqli_query($conn, $query);
                                            while ($row = mysqli_fetch_assoc($query)) {
                                            ?>
                                                <tr>
                                                    <td><?= $row['id_customer']; ?></td>
                                                    <td><?= $row['cst_full_name']; ?></td>
                                                    <td><?= $row['x_order']; ?></td>
                                                    <td><?= rupiah($row['x_total']); ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>ID Pelanggan</th>
                                                <th>Nama Pelanggan</th>
                                                <th>Jumlah Transaksi</th>
                                                <th>Total Transaksi</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <?php
                            } else if ($_GET['report_format'] == 'R') {
                            ?>
                                <div class="table-responsive">
                                    <table id="table_D" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID Pelanggan</th>
                                                <th>Nama Pelanggan</th>
                                                <th>Jumlah Transaksi</th>
                                                <th>Total Transaksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ((!empty($_GET['range_start'])) || (!empty($_GET['range_end']))) {
                                                $range_str = $_GET['range_start'];
                                                $range_end = $_GET['range_end'];
                                                $query = "SELECT `o`.`id_customer`, `cst_full_name`, `order_time`, `id_order`, SUM(`order_total`) AS `x_total`, COUNT(`id_order`) AS `x_order`
                                                FROM `wrtg_order` `o`
                                                INNER JOIN `wrtg_customer` `c`
                                                    ON (`o`.`id_customer`=`c`.`id_customer`)
                                                INNER JOIN `wrtg_payment` `p`
                                                    ON (`p`.`pay_code`=`o`.`order_payment_code`)
                                                WHERE FROM_UNIXTIME(`order_time`, '%Y-%m-%d') BETWEEN '$range_str' AND '$range_end'
                                                GROUP BY `o`.`id_customer`
                                                ORDER BY `c`.`cst_full_name`";
                                            } else {
                                                $query = "SELECT `o`.`id_customer`, `cst_full_name`, `order_time`, `id_order`, SUM(`order_total`) AS `x_total`, COUNT(`id_order`) AS `x_order`
                                                FROM `wrtg_order` `o`
                                                INNER JOIN `wrtg_customer` `c`
                                                    ON (`o`.`id_customer`=`c`.`id_customer`)
                                                INNER JOIN `wrtg_payment` `p`
                                                    ON (`p`.`pay_code`=`o`.`order_payment_code`)
                                                GROUP BY `o`.`id_customer`
                                                ORDER BY `c`.`cst_full_name`";
                                            }
                                            $query = mysqli_query($conn, $query);
                                            $sum_1 = 0;
                                            $sum_2 = 0;
                                            while ($row = mysqli_fetch_assoc($query)) {
                                                $sum_1 += $row['x_order'];
                                                $sum_2 += $row['x_total'];
                                            ?>
                                                <tr>
                                                    <td><?= $row['id_customer']; ?></td>
                                                    <td><?= $row['cst_full_name']; ?></td>
                                                    <td><?= $row['x_order']; ?></td>
                                                    <td><?= rupiah($row['x_total']); ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="2">Total</th>
                                                <th><?= $sum_1; ?></th>
                                                <th><?= rupiah($sum_2); ?></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <?php include('../../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <?php include('../../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $("#table_D").tableExport({
                position: 'top',
            });

        });
    </script>
</body>

</html>