<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../../lib/config.php');
$config['title'] = $config['admin_name'] . ' - Laporan Transaksi';
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
}

$id_emp     = $_SESSION['emp_id'];
$employee   = mysqli_query($conn, "SELECT *
FROM `wrtg_employee` `w`
INNER JOIN `wrtg_job` `j`
    ON (`w`.`emp_job_id`=`j`.`id_job`)
WHERE `id_employee`='$id_emp'");
$emp = mysqli_fetch_array($employee);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../../inc/admin-page/admin-head.phtml'); ?>
    <script type="text/javascript" src="https://raw.githubusercontent.com/Spyes/printTable.js/master/printTable.min.js"></script>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include('../../inc/admin-page/admin-header.phtml'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Laporan Transaksi</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <a href="<?= $config['host_admin']; ?>/reports" class="btn btn-primary mb-3"><i class="fas fa-chevron-circle-left"></i> Kembali Ke Laporan</a>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Laporan Transaksi</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php
                            if ($_GET['report_format'] == 'D') {
                            ?>
                                <div class="alert alert-info">
                                    <?php
                                    if ((!empty($_GET['range_start'])) || (!empty($_GET['range_end']))) {
                                        echo 'Menampilakan laporan detail transaksi periode ' . $_GET['range_start'] . ' - ' . $_GET['range_end'];
                                    } else {
                                        echo 'Menampilakan laporan detail transaksi keseluruhan';
                                    }
                                    ?>
                                </div>
                                <div class="table-responsive">
                                    <table id="table_D" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Waktu Transaksi</th>
                                                <th>Pelanggan</th>
                                                <th>Metode Pembayaran</th>
                                                <th>Status Pembayaran</th>
                                                <th>Nominal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ((!empty($_GET['range_start'])) || (!empty($_GET['range_end']))) {
                                                $range_str = $_GET['range_start'];
                                                $range_end = $_GET['range_end'];
                                                $query = "SELECT *
                                                FROM `wrtg_order` `o`
                                                INNER JOIN `wrtg_customer` `c`
                                                    ON (`o`.`id_customer`=`c`.`id_customer`)
                                                INNER JOIN `wrtg_payment` `p`
                                                    ON (`p`.`pay_code`=`o`.`order_payment_code`)
                                                WHERE FROM_UNIXTIME(`order_time`, '%Y-%m-%d') BETWEEN '$range_str' AND '$range_end'
                                                ORDER BY `order_time` DESC";
                                            } else {
                                                $query = "SELECT *
                                                FROM `wrtg_order` `o`
                                                INNER JOIN `wrtg_customer` `c`
                                                    ON (`o`.`id_customer`=`c`.`id_customer`)
                                                INNER JOIN `wrtg_payment` `p`
                                                    ON (`p`.`pay_code`=`o`.`order_payment_code`)
                                                ORDER BY `order_time` DESC";
                                            }
                                            $query = mysqli_query($conn, $query);
                                            while ($row = mysqli_fetch_assoc($query)) {
                                                $d = ucwords($row['order_payment_status']);
                                                if ($d == 'Pending') {
                                                    $pay_status = 'Menunggu Konfirmasi';
                                                } else if ($d == 'Verification') {
                                                    $pay_status = 'Dalam Verifikasi';
                                                } else if ($d == 'Confirmed') {
                                                    $pay_status = 'Terkonfirmasi';
                                                } else if ($d == 'Canceled') {
                                                    $pay_status = 'Dibatalkan';
                                                }
                                            ?>
                                                <tr>
                                                    <td><?= $row['id_order']; ?></td>
                                                    <td><?= date('Y-m-d H:i', $row['order_time']); ?></td>
                                                    <td><?= $row['cst_full_name']; ?></td>
                                                    <td><?= $row['pay_name']; ?></td>
                                                    <td><?= $pay_status; ?></td>
                                                    <td><?= rupiah($row['order_total']); ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                            <tr>
                                                <td>158</td>
                                                <td class=" text-nowrap">2023-05-24 09:29</td>
                                                <td class=" text-nowrap">Kasir Warteg</td>
                                                <td class=" text-nowrap">Bayar di Tempat</td>
                                                <td class=" text-nowrap">Terkonfirmasi</td>
                                                <td class=" text-nowrap">Rp 51.700</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Waktu Transaksi</th>
                                                <th>Pelanggan</th>
                                                <th>Metode Pembayaran</th>
                                                <th>Status Pembayaran</th>
                                                <th>Nominal</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <?php
                            } else if ($_GET['report_format'] == 'R') {
                            ?>
                                <div class="table-responsive">
                                    <table id="table_D" class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="text-nowrap">
                                                <th>ID</th>
                                                <th>Waktu Transaksi</th>
                                                <th>Pelanggan</th>
                                                <th>Metode Pembayaran</th>
                                                <th>Status Pembayaran</th>
                                                <th>Sub Total</th>
                                                <th>Diskon</th>
                                                <th>Pajak</th>
                                                <th>Total Transaksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ((!empty($_GET['range_start'])) || (!empty($_GET['range_end']))) {
                                                $range_str = $_GET['range_start'];
                                                $range_end = $_GET['range_end'];
                                                $query = "SELECT *
                                                FROM `wrtg_order` `o`
                                                INNER JOIN `wrtg_customer` `c`
                                                    ON (`o`.`id_customer`=`c`.`id_customer`)
                                                INNER JOIN `wrtg_payment` `p`
                                                    ON (`p`.`pay_code`=`o`.`order_payment_code`)
                                                WHERE FROM_UNIXTIME(`order_time`, '%Y-%m-%d') BETWEEN '$range_str' AND '$range_end'
                                                ORDER BY `order_time` DESC";
                                            } else {
                                                $query = "SELECT *
                                                FROM `wrtg_order` `o`
                                                INNER JOIN `wrtg_customer` `c`
                                                    ON (`o`.`id_customer`=`c`.`id_customer`)
                                                INNER JOIN `wrtg_payment` `p`
                                                    ON (`p`.`pay_code`=`o`.`order_payment_code`)
                                                ORDER BY `order_time` DESC";
                                            }
                                            $query = mysqli_query($conn, $query);
                                            $sum_1 = 0;
                                            $sum_2 = 0;
                                            $sum_3 = 0;
                                            $sum_4 = 0;
                                            while ($row = mysqli_fetch_assoc($query)) {
                                                $sum_1 += $row['order_subtotal'];
                                                $sum_2 += $row['order_discount'];
                                                $sum_3 += $row['order_total'];
                                                $sum_4 += $row['order_tax'];
                                                $d = ucwords($row['order_payment_status']);
                                                if ($d == 'Pending') {
                                                    $pay_status = 'Menunggu Konfirmasi';
                                                } else if ($d == 'Verification') {
                                                    $pay_status = 'Dalam Verifikasi';
                                                } else if ($d == 'Confirmed') {
                                                    $pay_status = 'Terkonfirmasi';
                                                } else if ($d == 'Canceled') {
                                                    $pay_status = 'Dibatalkan';
                                                }
                                            ?>
                                                <tr class="text-nowrap">
                                                    <td><?= $row['id_order']; ?></td>
                                                    <td><?= date('Y-m-d H:i', $row['order_time']); ?></td>
                                                    <td><?= $row['cst_full_name']; ?></td>
                                                    <td><?= $row['pay_name']; ?></td>
                                                    <td><?= $pay_status; ?></td>
                                                    <td><?= rupiah($row['order_subtotal']); ?></td>
                                                    <td><?= rupiah($row['order_discount']); ?></td>
                                                    <td><?= rupiah($row['order_tax']); ?></td>
                                                    <td><?= rupiah($row['order_total']); ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>

                                        </tbody>
                                        <tfoot>
                                            <tr class="text-nowrap">
                                                <th colspan="5">Total</th>
                                                <th><?= rupiah($sum_1); ?></th>
                                                <th><?= rupiah($sum_2); ?></th>
                                                <th><?= rupiah($sum_4); ?></th>
                                                <th><?= rupiah($sum_3); ?></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <?php include('../../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <?php include('../../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $("#table_D").tableExport({
                position: 'top',
            });

        });
    </script>
</body>

</html>