<?php
// Project UAS: PPPL
// --- Sistem Informasi Warteg
// --- Kelompok 3
// --- Author by: Nathanael F, M Abi, Andi I
// --- STMIK LIKMI
// --- Don't remove this copyright
// 2023
session_start();
require('../lib/config.php');
$config['title'] = $config['name'] . ' - Kelola Menu';
$allow_position = array('DEVELOPER', 'PEMILIK', 'MANAGER', 'KOKI');
if ($_SESSION['emp_status'] != 'login') {
    header('Location:' . $config['host'] . '/admin-page/login');
    exit();
} else if (in_array(strtoupper($_SESSION['emp_job_name']), $allow_position) == false) {
    header('Location:' . $config['host_admin']);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('../inc/admin-page/admin-head.phtml'); ?>
    <style>
        .img-max {
            max-width: 500px
        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php include('../inc/admin-page/admin-header.phtml'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#"><?= $config['name']; ?></a></li>
                                <li class="breadcrumb-item active">Kelola Data Menu</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">

                <!-- ------------------------------------------------  -->

                <!-- Menu Area -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-database"></i> Data Menu</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <a class="btn btn-info" id="btnTambah" href="#" data-toggle="modal" data-target="#modalAdd" role="button"><i class="fas fa-plus"></i> Tambah Menu Baru</a>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="List-Data" class="display table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Gambar</th>
                                                    <th class="text-nowrap">Nama Menu</th>
                                                    <th class="text-nowrap">Deskripsi</th>
                                                    <th class="text-nowrap">Stok</th>
                                                    <th class="text-nowrap">Harga Menu</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-nowrap">ID</th>
                                                    <th class="text-nowrap">Gambar</th>
                                                    <th class="text-nowrap">Nama Menu</th>
                                                    <th class="text-nowrap">Deskripsi</th>
                                                    <th class="text-nowrap">Stok</th>
                                                    <th class="text-nowrap">Harga Menu</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.container-fluid -->

                <!-- Start: Modal Edit Data -->
                <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Data</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <!-- Form Upload Gambar -->
                                <form id="form-upload-2" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Pilih Gambar</label>
                                        <input type="file" class="form-control-file" name="image" id="file_gambar_2">
                                    </div>
                                </form>
                                <!-- End Form Upload Gambar -->

                                <form method="POST" action="<?= $config['host']; ?>/api/admin/menu?method=UPDATE" id="Edit-Data-Form">
                                    <input type="hidden" id="val_id_data" name="id_data">

                                    <!-- Gambar -->
                                    <div id="image_menu_box_edit">
                                        <div class="form-group">
                                            <label>Gambar</label><br />
                                            <img id="image_menu_thumb_edit" class="img-fluid" alt="Responsive image">
                                        </div>
                                        <input type="hidden" id="edit_menu_images" name="menu_images" value="">
                                    </div>
                                    <!-- End Gambar -->

                                    <div class="form-group">
                                        <label for="menu_name">Nama Menu</label>
                                        <input type="text" class="form-control" id="val_menu_name" name="menu_name" placeholder="Masukkan nama menu">
                                    </div>
                                    <div class="form-group">
                                        <label for="menu_description">Deskripsi Menu</label>
                                        <textarea class="form-control" id="val_menu_description" name="menu_description" placeholder="Masukkan deskripsi menu"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="menu_category">Kategori Menu</label>
                                        <select name="menu_category" id="menu_category" class="form-control" required>
                                            <option value="" id="val_menu_category">-- Pilih Kategori --</option>
                                            <?php
                                            $query = mysqli_query($conn, "SELECT * FROM `wrtg_menu_category`");
                                            while ($row = mysqli_fetch_assoc($query)) {
                                                echo '<option value="' . $row['id_category'] . '">(' . $row['id_category'] . ') ' . $row['category_name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="menu_price">Harga Menu</label>
                                        <input type="number" class="form-control" id="val_menu_price" name="menu_price" placeholder="Masukkan harga menu">
                                    </div>
                                    <div class="form-group">
                                        <label for="menu_stock">Stok Menu</label>
                                        <input type="number" class="form-control" id="val_menu_stock" name="menu_stock" placeholder="Masukkan stok menu">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_3" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal Edit Data -->

                <!-- Start: Modal Add Data -->
                <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Tambah Varian</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <!-- Form Upload Gambar -->
                                <form id="form-upload" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Pilih Gambar</label>
                                        <input type="file" class="form-control-file" name="image" id="file_gambar">
                                    </div>
                                </form>
                                <!-- End Form Upload Gambar -->

                                <form method="POST" action="<?= $config['host']; ?>/api/admin/menu?method=CREATE" id="Add-Data-Form">
                                    <!-- Gambar -->
                                    <div id="image_menu_box">
                                        <div class="form-group">
                                            <label>Gambar</label><br />
                                            <img id="image_menu_thumb" class="img-fluid" alt="Responsive image">
                                        </div>
                                        <input type="hidden" id="add_menu_images" name="menu_images" value="">
                                    </div>
                                    <!-- End Gambar -->

                                    <div class="form-group">
                                        <label for="menu_name">Nama Menu</label>
                                        <input type="text" class="form-control" id="menu_name" name="menu_name" placeholder="Masukkan nama menu">
                                    </div>
                                    <div class="form-group">
                                        <label for="menu_description">Deskripsi Menu</label>
                                        <textarea class="form-control" id="menu_description" name="menu_description" placeholder="Masukkan deskripsi menu"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="menu_category">Kategori Menu</label>
                                        <select name="menu_category" id="menu_category" class="form-control" required>
                                            <option value="">-- Pilih Kategori --</option>
                                            <?php
                                            $query = mysqli_query($conn, "SELECT * FROM `wrtg_menu_category`");
                                            while ($row = mysqli_fetch_assoc($query)) {
                                                echo '<option value="' . $row['id_category'] . '">(' . $row['id_category'] . ') ' . $row['category_name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="menu_price">Harga Menu</label>
                                        <input type="number" class="form-control" id="menu_price" name="menu_price" placeholder="Masukkan harga menu">
                                    </div>
                                    <div class="form-group">
                                        <label for="menu_stock">Stok Menu</label>
                                        <input type="number" class="form-control" id="menu_stock" name="menu_stock" placeholder="Masukkan stok menu">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="button_id_2" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                        <button class="btn btn-danger float-right" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal Add Data -->

                <!-- Start: Modal View Child Data -->
                <div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="modalView" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Data Varian</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="table-responsive">
                                    <table class="table borderless">
                                        <tbody>
                                            <form method="POST" action="<?= $config['host']; ?>/api/admin/menu?method=CREATE_MENU_VARIAN" id="Add-Data-Child-Form">
                                                <tr>
                                                    <input type="hidden" id="val_id_menu" name="id_menu" value="">
                                                    <td class="text-nowrap">
                                                        <label for="variant_name">Varian</label>
                                                        <select name="id_variant_group" class="form-control" required>
                                                            <option value="">-- Pilih Varian --</option>
                                                            <?php
                                                            $query = mysqli_query($conn, "SELECT * FROM `wrtg_variant_group`");
                                                            while ($row = mysqli_fetch_assoc($query)) {
                                                                echo '<option value="' . $row['id_variant_group'] . '">(' . $row['id_variant_group'] . ') ' . $row['group_name'] . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-nowrap">
                                                        <button class="btn btn-primary" id="button_id_4" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                                        <button class="btn btn-danger" type="reset"><i class="fa fa-trash"></i> Reset</button>
                                                    </td>
                                                </tr>
                                            </form>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered" id="editableTable">
                                        <thead>
                                            <tr>
                                                <th class="text-nowrap">Aksi</th>
                                                <th class="text-nowrap d-none">ID Menu</th>
                                                <th class="text-nowrap">Nama Menu</th>
                                                <th class="text-nowrap">Nama Varian</th>
                                            </tr>
                                        </thead>
                                        <tbody id="table-data">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- End: Modal View Child Data -->
                <!-- End: Kategori Menu Area -->

            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php include('../inc/admin-page/admin-footer.phtml'); ?>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <?php include('../inc/admin-page/admin-foot.phtml'); ?>

    <!-- SweetAlert Plugin JS -->
    <script type="text/javascript" src="<?= $config['host']; ?>/assets/js/sweetalert.min.js"></script>

    <!-- Custom JS -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('#image_menu_box').hide();

            // ====================== Menu Data ======================

            // JS fetch list data
            var table = $('#List-Data').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?= $config['host']; ?>/api/admin/menu?method=READ_LIST",
                "columnDefs": [{
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<span class='text-nowrap'><button class='btn btn-success btn-sm tblView'><i class='fa fa-eye'></i></button> <button class='btn btn-info btn-sm tblEdit'><i class='fa fa-edit'></i></button> <button class='btn btn-danger btn-sm tblDelete'><i class='fa fa-trash'></i></button></span>"
                    },
                    {
                        "targets": 0,
                        // "className": "d-none"
                    },
                    {
                        "targets": [1, 2, 3],
                        "className": "text-nowrap"
                    }
                ]
            });

            // JS upload image
            $(document).ready(function() {
                // ketika gambar dipilih
                $('#file_gambar').change(function() {
                    var file_data = $('#file_gambar').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('image', file_data);
                    $.ajax({
                        url: '<?= $config['host']; ?>/api/v1/upload-images',
                        type: 'POST',
                        data: form_data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: false,
                        dataType: 'JSON',
                        success: function(hasil) {
                            // tampilkan nama file yang diunggah jika berhasil
                            if (hasil.status == 1) {
                                let file_path = hasil.data.file_path;
                                let file_name = hasil.data.file_name;
                                swal('Berhasil!', hasil.content, 'success');
                                $('#image_menu_thumb').attr('src', '<?= $config['host']; ?>/' + file_path);
                                $('#image_menu_box').show(1000);
                                $('#add_menu_images').val(file_path);
                            } else {
                                swal("Failed!", hasil.content, "error");
                            }
                        },
                        error: function(xhr, status, error) {
                            // tampilkan pesan error jika terjadi kesalahan
                            alert('Terjadi kesalahan: ' + error);
                        }
                    });
                });
            });


            // JS create data
            $("form#Add-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalAdd').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_2").html('<i class="fa fa-save"></i> Save');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS show data single -> show edit form
            $('#List-Data tbody').on('click', '.tblEdit', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/menu?method=READ_SINGLE",
                    data: {
                        id_menu: id_data
                    },
                    dataType: "JSON",
                    success: function(result) {
                        $('#modalEdit').modal('show');
                        $('#val_id_data').val(result.data.id_menu);
                        $('#val_menu_name').val(result.data.menu_name);
                        $('#val_menu_description').val(result.data.menu_description);
                        $('#val_menu_price').val(result.data.menu_price);
                        $('#val_menu_stock').val(result.data.menu_stock);
                        $('#val_menu_category').val(result.data.id_category);
                        $('#edit_menu_images').val(result.data.menu_images);
                        $('#image_menu_thumb_edit').attr('src', '<?= $config['host']; ?>/' + result.data.menu_images);
                    }
                });
            });

            // JS upload image (EDIT)
            $(document).ready(function() {
                // ketika gambar dipilih
                $('#file_gambar_2').change(function() {
                    var file_data = $('#file_gambar_2').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('image', file_data);
                    $.ajax({
                        url: '<?= $config['host']; ?>/api/v1/upload-images',
                        type: 'POST',
                        data: form_data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: false,
                        dataType: 'JSON',
                        success: function(hasil) {
                            // tampilkan nama file yang diunggah jika berhasil
                            if (hasil.status == 1) {
                                let file_path = hasil.data.file_path;
                                let file_name = hasil.data.file_name;
                                swal('Berhasil!', hasil.content, 'success');
                                $('#image_menu_thumb_edit').attr('src', '<?= $config['host']; ?>/' + file_path);
                                $('#edit_menu_images').val(file_path);
                            } else {
                                swal("Failed!", hasil.content, "error");
                            }
                        },
                        error: function(xhr, status, error) {
                            // tampilkan pesan error jika terjadi kesalahan
                            alert('Terjadi kesalahan: ' + error);
                        }
                    });
                });
            });

            // JS update data
            $("form#Edit-Data-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                            table.ajax.reload(null, false);
                            $('#modalEdit').modal('hide');
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id").html('<i class="fa fa-save"></i> Save');
                        $("#result_submit").html(c);
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id").html('Loading..');
                        $("#result_submit").html('');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS delete data
            $('#List-Data tbody').on('click', '.tblDelete', function() {
                var data = table.row($(this).parents('tr')).data();
                //var nama_tools = data[1];
                var id_data = data[0];
                swal({
                        title: "Anda yakin?",
                        text: "Anda tidak bisa mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, hapus data!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/admin/menu?method=DELETE",
                                data: {
                                    id_menu: id_data
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    if (hasil.status) {
                                        swal("Berhasil", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Dibatalkan", "Data anda aman :)", "error");
                        }
                    })
            });

            // JS (CHILD) create data
            $("form#Add-Data-Child-Form").submit(function() {
                var pdata = $(this).serialize();
                var purl = $(this).attr('action');
                var id_data = $("#val_id_menu").val();
                $.ajax({
                    url: purl,
                    data: pdata,
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_4").html('<i class="fa fa-save"></i> Simpan');
                        if (hasil.status) {
                            table.ajax.reload(null, false);
                            console.log(id_data);
                            loadChildData(id_data);
                            swal("Success!", "" + hasil.content + "", "success");
                        } else
                            swal("Failed!", "" + hasil.content + "", "error");
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        $("#button_id_4").html('<i class="fa fa-save"></i> Simpan');
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("#button_id_4").html('Loading..');
                        $("button").attr("disabled", "disabled");
                    }
                });
                return false
            });

            // JS (CHILD) update data
            $(document).on("click", "#editableTable button.edtUpdate", function() {
                let tr = $(this).closest('tr');
                let id_menu = tr.find('td:eq(1) input').val();
                let id_variant_group = tr.find('td:eq(3) select').val();
                console.log(id_variant_group);
                // alert('Table 1: ' + id_variant + '\n' + variant_name + '\n' + variant_description + '\n' + variant_price);
                $.ajax({
                    url: "<?= $config['host']; ?>/api/admin/menu?method=UPDATE_MENU_VARIAN",
                    data: {
                        id_menu: id_menu,
                        id_variant_group: id_variant_group
                    },
                    timeout: false,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(hasil) {
                        loadChildData(id_menu);
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                        if (hasil.status) {
                            swal("Success!", "" + hasil.content + "", "success");
                        } else {
                            swal("Failed!", "" + hasil.content + "", "error");
                        }
                    },
                    error: function(a, b, c) {
                        $("input").removeAttr("disabled", "disabled");
                        $("button").removeAttr("disabled", "disabled");
                    },
                    beforeSend: function() {
                        $("input").attr("disabled", "disabled");
                        $("button").attr("disabled", "disabled");
                    }
                });
            });

            // JS (CHILD) delete data
            $(document).on("click", "#editableTable button.edtDelete", function() {
                let tr = $(this).closest('tr');
                let id_menu = tr.find('td:eq(1) input').val();
                let id_variant_group = tr.find('td:eq(3) select').val();

                swal({
                        title: "Anda yakin?",
                        text: "Anda tidak bisa mengembalikan data ini!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, hapus data!",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "<?= $config['host']; ?>/api/admin/menu?method=DELETE_MENU_VARIAN",
                                data: {
                                    id_menu,
                                    id_variant_group
                                },
                                dataType: "JSON",
                                success: function(hasil) {
                                    if (hasil.status) {
                                        swal("Berhasil", "" + hasil.content + "", "success");
                                        table.ajax.reload(null, false);
                                        loadChildData(id_menu);
                                    } else
                                        swal("Failed", "" + hasil.content + "", "error");
                                }
                            });
                        } else {
                            swal("Dibatalkan", "Data anda aman :)", "error");
                        }
                    })
            });

            function loadChildData(id_data) {
                $.ajax({
                    type: "POST",
                    url: "<?= $config['host']; ?>/api/admin/menu?method=READ_CHILD",
                    data: {
                        id_menu: id_data
                    },
                    dataType: "JSON",
                    success: function(response) {
                        $('#modalView').modal('show');
                        $('#val_id_menu').val(id_data);
                        if (response.data.length > 0) {
                            $.each(response.data, function(i, item) {
                                $('#table-data').append(
                                    '<tr>' +
                                    '<td class="text-nowrap"><span class="text-nowrap"><button class="btn btn-primary btn-sm edtUpdate"><i class="fa fa-save"></i></button> <button class="btn btn-danger btn-sm edtDelete"><i class="fa fa-trash"></i></button></span></td>' +
                                    '<td class="text-nowrap d-none"><input type="text" class="form-control w-auto" value="' + item.id_menu + '" disabled></td>' +
                                    '<td class="text-nowrap"><input type="text" class="form-control w-auto" name="val_id_variant" value="' + item.menu_name + '" disabled></td>' +
                                    '<td class="text-nowrap"><select name="id_variant_group" class="form-control" required>' + item.html_data_edit + '</select></td>' +
                                    '</tr>'
                                );
                            });
                        } else {
                            $('#table-data').append(
                                '<tr>' +
                                '<td class="text-nowrap text-center" colspan="5">Belum ada data varian</td>' +
                                '</tr>'
                            );
                        }
                    },
                    error: function(a, b, c) {
                        console.log('NOT OK!')
                        swal("Error", "" + hasil.content + "", "error");
                    },
                    beforeSend(a, b) {
                        $('#table-data').html('');
                    }
                });
            }

            // JS (CHILD) show data list
            $('#List-Data tbody').on('click', '.tblView', function() {
                var data = table.row($(this).parents('tr')).data();
                var id_data = data[0];
                loadChildData(id_data);
            });

        });
    </script>
</body>

</html>